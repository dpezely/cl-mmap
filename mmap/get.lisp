;;;; get.lisp - read within an opened mmap

(in-package #:mmap)

(defun get-byte (mmap-addr offset)
  "Reads and returns one byte from a memory-mapped file pointed to by
  the MMAP-ADDR pointer, offset by OFFSET bytes."
  (declare (type (integer 0) offset))
  (ignore-mmap-errors
   (cffi:mem-aref (cffi:inc-pointer mmap-addr offset) :uint8)))

(defun get-char (mmap-addr offset)
  "Reads and returns a character from a memory-mapped file pointed to by
  the MMAP-ADDR pointer, offset by OFFSET bytes."
  (declare (type (integer 0) offset))
  (ignore-mmap-errors
   (code-char (cffi:mem-aref (cffi:inc-pointer mmap-addr offset) :char))))

(defun get-big-endian (mmap-addr offset n-bytes)
  "Reads and returns N-BYTES in big-endian byte order
  from a memory-mapped file pointed to by the MMAP-ADDR pointer,
  offset by OFFSET bytes.
  Returns: value on success or NIL."
  (declare (type (integer 0) offset n-bytes))
  (let ((value 0)
        (n-bits (* n-bytes 8)))
    (loop
       for i downfrom (- n-bits 8) downto 0 by 8
       for cursor upfrom offset
       for byte = (ignore-mmap-errors
		   (cffi:mem-aref (cffi:inc-pointer
				   mmap-addr cursor)
				  :uint8))
       when byte
       do (setf value (dpb byte (byte 8 i) value))
       else
       do (return-from get-big-endian nil))
    value))

(defun get-little-endian (mmap-addr offset n-bytes)
  "Reads and returns N-BYTES in little-endian byte order
  from a memory-mapped file pointed to by the MMAP-ADDR pointer,
  offset by OFFSET bytes.
  Returns: value on success or NIL."
  (declare (type (integer 0) offset n-bytes))
  (let ((value 0)
        (n-bits (* n-bytes 8)))
    (loop
       for i below n-bits by 8
       for cursor upfrom offset
       for byte = (ignore-mmap-errors
		   (cffi:mem-aref (cffi:inc-pointer
				   mmap-addr cursor)
				  :uint8))
       when byte
       do (setf value (dpb byte (byte 8 i) value))
       else
       do (return-from get-little-endian nil))
    value))

(defun get-byte-vector (mmap-addr offset vector-length)
  "Reads and returns VECTOR with elements of one byte from a memory-mapped file
  pointed to by the MMAP-ADDR pointer, offset by OFFSET bytes.
  Returns: populated VECTOR on success or NIL."
  (declare (type (integer 0) offset vector-length))
  (let ((vector (make-array vector-length :element-type '(unsigned-byte 8))))
    (loop
       for i below vector-length
       for cursor upfrom offset
       for byte = (ignore-mmap-errors
		   (cffi:mem-aref (cffi:inc-pointer mmap-addr cursor)
				  :uint8))
       when byte
       do (setf (elt vector i) byte)
       else
       do (return-from get-byte-vector nil))
    vector))

(defun get-big-endian-vector (mmap-addr offset vector-length
				n-bytes-per-element)
  "Reads and returns VECTOR with elements of N-BYTES in big-endian order
  from a memory-mapped file pointed to by the MMAP-ADDR pointer,
  offset by OFFSET bytes.
  Returns: populated VECTOR on success or NIL."
  (declare (type (integer 0) offset vector-length n-bytes-per-element))
  (let* ((n-bits (* n-bytes-per-element 8))
	 (vector (make-array vector-length
			     :element-type `(unsigned-byte ,n-bits))))
    (loop
       for i below vector-length
       for value = 0
       do (loop
	     for j downfrom (- n-bits 8) downto 0 by 8
	     for cursor upfrom offset
	     for byte = (ignore-mmap-errors
			 (cffi:mem-aref (cffi:inc-pointer
					 mmap-addr cursor)
					:uint8))
	     when byte
	     do (setf value (dpb byte (byte 8 j) value))
	     else
	     do (return-from get-big-endian-vector nil))
	 (setf (elt vector i) value)
	 (incf offset n-bytes-per-element))
    vector))

(defun get-little-endian-vector (mmap-addr offset vector-length
				   n-bytes-per-element)
  "Reads and returns VECTOR with elements of N-BYTES in little-endian order
  from a memory-mapped file pointed to by the MMAP-ADDR
  pointer, offset by OFFSET bytes.
  Returns: populated VECTOR on success or NIL."
  (declare (type (integer 0) offset vector-length n-bytes-per-element))
  (let* ((n-bits (* n-bytes-per-element 8))
	 (vector (make-array vector-length
			     :element-type `(unsigned-byte ,n-bits))))
    (loop
       for i below vector-length
       for value = 0
       do (loop
	     for j below n-bits by 8
	     for cursor upfrom offset
	     for byte = (ignore-mmap-errors
			 (cffi:mem-aref (cffi:inc-pointer
					 mmap-addr cursor)
					:uint8))
	     when byte
	     do (setf value (dpb byte (byte 8 j) value))
	     else
	     do (return-from get-little-endian-vector nil))
	 (setf (elt vector i) value)
	 (incf offset n-bytes-per-element))
    vector))
