;;;; posix-madvise.lisp - for CFFI grovel via ASDF

;; For values below:
;; See Unix/POSIX manual pages: posix_madvise(2), posix_fadvise(2)
;; also on FreeBSD and Linux: madvise(2), fadvise(2)

;; For more about the nature of this file, see:
;; https://common-lisp.net/project/cffi/manual/cffi-manual.html#Groveller-Syntax

(in-package #:mmap)

(include "sys/mman.h")                  ; madvise
(include "fcntl.h")                     ; fadvise

(constant (+posix-madv-random+ "POSIX_MADV_RANDOM"))
(constant (+posix-fadv-random+ "POSIX_FADV_RANDOM"))
