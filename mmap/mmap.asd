;;;; mmap.asd

(asdf:defsystem #:mmap
  :description
  "Low-level Library for working with memory-mapped files on POSIX-compliant OS"
  :author "Daniel Pezely <first name at last name dot com>"
  :version "1.0"
  :license "MIT"

  :long-description
  "On a POSIX-compliant OS including BSD Unix, Linux and macOS operate
  upon memory-mapped files with behaviour similar to Common Lisp functions
  such as OPEN and READ or WRITE.

  File operations supported include opening with direction of :input,
  :output or :io.  Manipulate single byte or multi-byte values including
  as vector elements within the mapped file.

  Big endian and little endian operations are supported.

  Modifications may be persisted at granularity of an entire file or
  just one region.  Both modes can optionally notify other concurrent
  mappings of same file to be refreshed."

  :defsystem-depends-on ("cffi-grovel")
  :depends-on (#:osicat
	       #:cffi)
  :serial t
  :components ((:file "package")
	       (:file "globals")
	       (:file "conditions")
	       (:file "utils")
	       (:file "open")
               (:cffi-grovel-file "posix-madvise")
               (:file "mmap")
	       (:file "get")
	       (:file "put"))
  :in-order-to ((asdf:test-op (asdf:test-op "mmap/test"))))

(asdf:defsystem #:mmap/test
  :description "Tests for mmap"
  :depends-on (#:mmap
	       #:prove)
  :serial t
  :components
  ((:module "test"
	    :components ((:file "setup-prove")
			 (:test-file "test-open-flags")
			 (:test-file "test-mapping-type")
			 (:test-file "test-get")
			 (:test-file "test-put")
			 (:test-file "test-sync"))))
  :defsystem-depends-on ("prove-asdf")
  :perform (asdf:test-op (op s)
			 (uiop:symbol-call :prove-asdf :run-test-system s)))
