;;;; conditions.lisp

(in-package #:mmap)

;;(push :allow-low-level-mmap-write-errors *features*)
;;
;; Writing beyond mmap extent signals implementation-dependent errors.
;; Rather than creating a maintenance nightmare for maintainers of this
;; package, suppress all errors for the relevant CFFI expression.
;;
;; However, while developing, it may be useful to see these errors to
;; ensure correctness.

(defmacro ignore-mmap-errors (&body body)
  "Like IGNORE-ERRORS but intended only for mmap.
  This macro traps actual mmap errors for signaling package level condition
  instead, controlled at the level of our API."
  #+allow-low-level-mmap-errors `(progn ,@body)
  #-allow-low-level-mmap-errors
  `(handler-case
       (progn ,@body)
     #+sbcl (sb-kernel::undefined-alien-variable-error () nil)
     #+sbcl (sb-sys:memory-fault-error () nil)

     ;; FIXME: add specific condition for various Lisps here...

     #-handle-only-known-mmap-errors
     (error () nil)))

;; FIXME: Not the best macro name but intended to be terse because of
;; code density in calling scope which in turn should lead to
;; refactoring that code into smaller functions.
(defmacro trap (system-call &optional (error-value -1))
  "Macro to wrap a POSIX system call, test its return value,
  and when necessary, invoke a Common Lisp condition or error."
  (alexandria:with-gensyms (status)
    (let* ((function-name (symbol-name (car `(,@system-call))))
	   (condition-name (or (find-symbol (concatenate
					     'string
					     (string-upcase function-name)
					     "-POSIX-ERROR"))
			       'posix-error)))
      `(handler-case
	   (let ((,status ,system-call))
	     (if (eql ,status ,error-value)
		 (error ',condition-name
			:system-call ',system-call
			:errno (osicat-posix:get-errno)
			:strerror (osicat-posix::strerror))
		 ,status))
	 #+allow-low-level-mmap-errors (dummy () nil)
	 #-allow-low-level-mmap-errors
	 (osicat-posix:posix-error ()
	   (error 'posix-error
		  :system-call ',system-call
		  :errno (osicat-posix:get-errno)
		  :strerror (osicat-posix::strerror)))))))

(define-condition mmap-error (error)
  ((description :initform (format nil "Generic error using CL-MMAP package")
		:reader mmap-error.description)
   (filepath :initarg :filepath :reader mmap-error.filepath)
   (file-size :initarg :file-size :reader mmap-error.file-size)
   (offset :initarg :offset :reader mmap-error.offset))
  (:report (lambda (condition stream)
	     (format stream "~A~%~@[filepath=~A ~]file-size=~A offset=~A"
		     (mmap-error.description condition)
		     (mmap-error.filepath condition)
		     (mmap-error.file-size condition)
		     (mmap-error.offset condition)))))

(define-condition end-of-mmap-error (mmap-error)
  ((description :initform "Attempted access beyond length of mmap file")))

(define-condition invalid-mmap-access-error (mmap-error)
  ((description :initform "Error while attempting access of mmap file")))

;; Most POSIX errors relevant to MMAP would be inappropriate for Lisp
;; condition restarts, except maybe for EAGAIN.
;;
;; Other POSIX errors likely encountered fit roughly into two categories:
;; 1) misconfigured due to this or an earlier call such as contradictory
;; flags to open() versus mmap(), or 2) resource constraint of the OS.
;;
;; Both categories are beyond scope of this package. None are ignorable.

(define-condition posix-error (error)
  ((description :initform "Error from POSIX system-call"
		:reader posix-error.description)
   (system-call :initform nil
		:initarg :system-call :reader posix-error.system-call)
   (errno :initform nil
	  :initarg :errno :reader posix-error.errno)
   (strerror :initform nil
	     :initarg :strerror :reader posix-error.strerror)
   (see-also :initform nil
	     :initarg :see-also :reader posix-error.see-also))
  (:report (lambda (condition stream)
	     (format stream
		     "~A~%Lisp-code=~A~@[~&errno=~A~]~@[ error=~S~]~
			~@[~%See also: Unix manual page for ~A~]"
		     (posix-error.description condition)
		     (posix-error.system-call condition)
		     (posix-error.errno condition)
		     (posix-error.strerror condition)
		     (posix-error.see-also condition)))))

(define-condition open-posix-error (posix-error)
  ((description :initform "Error from POSIX system-call: open()")
   (see-also :initform "open(2)")))

(define-condition fstat-posix-error (posix-error)
  ((description :initform "Error from POSIX system-call: fstat()")
   (see-also :initform "fstat(2)")))

(define-condition lseek-posix-error (posix-error)
  ((description :initform "Error from POSIX system-call: lseek()")
   (see-also :initform "lseek(2)")))

(define-condition write-posix-error (posix-error)
  ((description :initform "Error from POSIX system-call: write()")
   (see-also :initform "write(2)")))

(define-condition mmap-posix-error (posix-error)
  ((description :initform "Error from POSIX system-call: mmap()")
   (see-also :initform "mmap(2)")))

(define-condition msync-posix-error (posix-error)
  ((description
    :initform
    "Error from POSIX system-call: msync() which persists a memory-mapped file")
   (see-also :initform "msync(2)")))

(define-condition close-posix-error (posix-error)
  ((description :initform "Error from POSIX system-call: close()")
   (see-also :initform "close(2)")))

(define-condition dummy (error)
  ((description :initform "placeholder as no-op within HANDLER-CASE")))
