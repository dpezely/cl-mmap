;;;; mmap.lisp - Common Lisp semantics for POSIX open(2), mmap(2) and msync(2)

(in-package #:mmap)

;; See also `open.lisp' file for functions called by `mmap'
;; and a table illustrating semantics of `OPEN' behaviour.

(defmacro with-mmap ((mmap-addr filename offset length-bytes &rest options
                      &key direction if-exists if-does-not-exist random-access
                      mode posix-open-flags posix-mmap-flags) &body body)
  "Opens FILENAME and uses mmap() to memory-map region starting with OFFSET
  and spanning LENGTH-BYTES, each of which should be a multiple page size.
  Keyword options are similar to `OPEN' in CL package.
  First parameter, MMAP-ADDR, becomes a captured/anaphoric variable
  for use within BODY.
  Maps whole file when DIRECTION is `:input' with both OFFSET and
  LENGTH-BYTES are both NIL.
  When IF-DOES-NOT-EXIST is `:create' or `:supersede', specify LENGTH-BYTES > 0
  and ideally as a multiple of page size.  See `+posix-page-size+'.
  File permissions may be set with MODE; e.g., #o0644 for world-readable.
  For advanced use cases, POSIX-OPEN-FLAGS and POSIX-MMAP-FLAGS specify
  optional bit-flags as documented in Unix manual open(2) and mmap(2),
  respectively.  When supplied, these bit-flags are applied via LOGIOR.
  When RANDOM-ACCESS is non-NIL, advise OS that prefetch would be suboptimal.
  File will be unmapped and closed upon completion of BODY.
  This macro uses variable capture for MMAP-ADDR for use within BODY.
  May signal condition within `posix-error' or `mmap-error' hierarchies.
  Returns: resulting value from BODY." 
  (declare (ignorable direction if-exists if-does-not-exist
                      mode posix-open-flags posix-mmap-flags))
  (let (#+DELETE-ME (fd (gensym "FD"))
        #+DELETE-ME (actual-size (gensym "ACTUAL-SIZE"))
        (mmap-options (remove-keywords '(:random-access) options)))
    ;; Anaphoric (intentionally un-hygenic) use of MMAP-ADDR in
    ;; multiple instances:
    `(multiple-value-bind (,mmap-addr %fd% %actual-size%)
         (mmap ,filename ,offset ,length-bytes ,@mmap-options)
       (unwind-protect
            (progn
              ,(when random-access
                 `(random-access ,mmap-addr %fd% ,offset %actual-size%))
              ,@body)
         (munmap ,mmap-addr %fd% %actual-size%)))))

(defun mmap (pathname offset length-bytes &rest options
             &key (direction :input) (if-exists nil if-e-given)
               (if-does-not-exist nil if-ne-given) (mode *default-file-mode*)
               (posix-open-flags *default-posix-open-flags*)
               (posix-mmap-flags *default-posix-mmap-flags*))
  "Opens FILENAME and uses mmap() to memory-map region starting with OFFSET
  and spanning LENGTH-BYTES.  Keyword options similar to `OPEN' in CL.
  See `with-mmap' macro description for details.
  Returns: VALUES of address, file-descriptor and length in bytes."
  (declare (type (or pathname string) pathname)
	   (type (or null symbol) direction if-exists if-does-not-exist)
	   (type (or null (integer 0)) offset mode posix-open-flags posix-mmap-flags)
	   (type (or null (integer 1)) length-bytes)) ;error on Linux>2.6.12 if 0
  #+open-mmap-without-safety (declare (ignore options))
  #-open-mmap-without-safety
  (apply 'validate-open-args (list* pathname offset length-bytes options))
  (multiple-value-bind (open-flag memory-protection)
      (process-open-flags pathname direction
                          if-exists if-e-given if-does-not-exist if-ne-given)
    (when open-flag
      (when posix-open-flags
        (setf open-flag (logior open-flag posix-open-flags)))
      #+verbose
      (format t "open filename=~A open-flag=#x~4,'0X/~[R~;W~;RW~] mode=#o~4,'0O~%"
	      pathname open-flag (logand open-flag #x03) mode)
      (let* ((filename (if (pathnamep pathname) (namestring pathname) pathname))
	     (fd (trap (osicat-posix:open filename open-flag (or mode #o0600))))
             (mmap-successful nil))
        (if posix-mmap-flags
            (setf posix-mmap-flags (logior posix-mmap-flags (set-mapping-flag direction)))
            (setf posix-mmap-flags (set-mapping-flag direction)))
	(when (find direction '(:output :io))
	  (allocate-writable-region fd length-bytes))
	#+verbose
	(let ((stats (osicat-posix:fstat fd)))
	  (format t "opened fstat fd=~D mode=~5,'0O size=~A~%"
		  fd (osicat-posix:stat-mode stats) (osicat-posix:stat-size stats))
	  (format t "mmap length=~A protect=#x~X/~[none~;read~;write~;rw~] ~
		     mapping=~[none~;shared~;private~] offset=#x~X~%"
		  length-bytes memory-protection (logand memory-protection #x03)
		  (logand posix-mmap-flags #x03) offset))
	(unwind-protect
	     (let* ((actual-size (or length-bytes
                                     (trap (osicat-posix:stat-size
                                            (osicat-posix:fstat fd)))))
                    (mmap-addr (trap
                                (osicat-posix:mmap (cffi:null-pointer) actual-size
                                                   memory-protection posix-mmap-flags
                                                   fd (or offset 0))
                                osicat-posix:map-failed)))
               (setf mmap-successful t)
	       (values mmap-addr fd actual-size))
	  (unless mmap-successful
            (trap (osicat-posix:close fd))))))))

(defun munmap (mmap-addr fd length-bytes)
  "Removes file mapping at the address, MMAP-ADDR, with range FILE-SIZE
  and closes file descriptor, FD.  These were likely opened via `mmap'.
  May signal condition within `posix-error' or `mmap-error' hierarchies.
  Returns: T if action taken, otherwise NIL."
  (declare (type (integer -1) fd)
           (type (integer 1) length-bytes))
  #+verbose (format t "~&munmap addr=~X length-byte=~X fd=~A~%"
		    mmap-addr length-bytes fd)
  (when (and fd (> fd -1))
    (trap (osicat-posix:munmap mmap-addr length-bytes)
          osicat-posix:map-failed)
    #+verbose (format t "close fd=~D~%" fd)
    (trap (osicat-posix:close fd))
    t))

(defun sync (mmap-addr offset length-bytes &key notify async)
  "Finish writes immediately so they become persisted.
  A POSIX-compliant OS will sync the mapped region eventually _without_
  calling this function.
  Params: MMAP-ADDR is base address from return of `mmap' function.
  Values for OFFSET and LENGTH-BYTES should be idential as those
  supplied to `mmap'.
  When keyword param NOTIFY is non-NIL, instructs OS kernel to
  invalidate other mappings of this file to ensure getting fresh
  values just written.  This function should only be called when `mmap'
  was opened with direction :output or :io.
  When keyword param ASYNC is non-NIL, indicates asynchronous mode so
  calling scope continues without waiting.
  Returns: T on success or signals condition of `posix-error' 
  message upon failure.  See Unix manual page `msync(2)` for details."
  (declare (type (integer 0) offset)
           (type (integer 1) length-bytes))
  (let ((flags (if async
                   osicat-posix:ms-async
                   osicat-posix:ms-sync)))
    (trap (osicat-posix:msync (cffi:inc-pointer mmap-addr offset)
                              length-bytes
                              (if notify
                                  (logior flags osicat-posix:ms-invalidate)
                                  flags))))
  t)

(defun random-access (mmap-addr offset length-bytes fd)
  "Advise OS that the mmap is to be used with random-access pattern,
  so prefetching would be suboptimal.
  Because this is only _advising_ the OS, there are no guarantees of
  performance characteristics changing.
  Params match positional arguments and returned values of `mmap' with
  same names.
  Usage hint: only when reading _less_ than 4 KB or `+posix-page-size+'
  at a time _and_ stepping through the memory-map by orders of
  magnitude more, would this setting be useful.  Otherwise, it may
  degrade performance significantly."
  ;; FIXME: osicat yet to support posix_madvise(2) or posix_fadvise(2)
  ;; as of 2018-05-01.  Should this be migrated to osicat-posix, also
  ;; remove corresponding cffi-grovel references from .asd file.
  (progn
    (cffi:foreign-funcall "posix_madvise"
                          :pointer mmap-addr
                          :ulong length-bytes
                          :int +posix-madv-random+)
    (cffi:foreign-funcall "posix_fadvise"
                          :int fd
                          :ulong offset
                          :ulong 0        ; implies to end of file
                          :int +posix-fadv-random+)))

(defun mremap (mmap-addr old-length-bytes new-length-bytes fd)
  "Grow/shrink an existing memory-mapped region and corresponding file.
  MMAP-ADDR is address as returned by `mmap'.
  OLD-LENGTH-BYTES corresponds to second VALUE returned by `mmap'.
  NEW-LENGTH-BYTES specifies new target size and may be larger or smaller.
  FD is file-descriptor from third VALUE returned by `mmap'.

  **This operation _may change address_ of the resized memory-map,
  so value returned here _must_ become the new MMAP-ADDR for all
  subsequent operations using this library.**

  Returns: new value for MMAP-ADDR on all subsequent operations."
  (declare (type (integer 0) old-length-bytes new-length-bytes))
  (let ((new-addr (trap
                   (osicat-posix:mremap mmap-addr old-length-bytes
                                        new-length-bytes
                                        osicat-posix:mremap-maymove))))
    (when (< old-length-bytes new-length-bytes)
      (let ((delta (- new-length-bytes old-length-bytes 1)))
        (trap (osicat-posix:lseek fd delta osicat-posix:seek-end))
        (cffi:with-foreign-string (buffer (princ-to-string #\Null))
          (trap (osicat-posix:write fd buffer 1)))))
    new-addr))
