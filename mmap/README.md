MMAP
====

MMAP is a low-level Common Lisp library for working with memory-mapped files
on POSIX-compliant operating systems.

(For higher-level API offering function names similar to Common Lisp
standard library, see the related [CL-MMAP](../cl-mmap/) package which
offers an abstraction of linear file access.)

Open a memory-mapped file using options including `:direction` and
`:if-does-not-exist`.  Based upon those values, reasonable defaults are
applied for low-level POSIX `open()` and `mmap()` flags.

## Overview

First and foremost-- mmap is **not** a magic bullet and may introduce
*performance penalties* when operating on ordinary data files, with all else
being equal.

This library is intended to offer a generic platform and some example code
from which you may build your library, framework or application.

POSIX-compliant mmap is well suited for data being pinned into memory of a
long-running process, but beware of OS paging overhead.

It may also be used for *sharing* memory between concurrent processes on the
same host.

There **may or may not** be advantages keeping volatile data away from the
garbage collector.
(See [Inside Orbitz, tidbit #4](http://www.paulgraham.com/carl.html), but
that was written in January 2001 from their prior experiences, and OS
kernels have seen much tuning since then.)

See [test/README](./test/) for comparative performance measurements.

## Getting Started

The lower-level package from which CL-MMAP is built is MMAP, which uses CFFI
and osicat-posix Lisp libraries.

**Low-Level Reading**

Using MMAP's `with-mmap` macro and `get-big-endian` function to read one value
comprised as multiple bytes from beginning of file:

```lisp
CL-USER> (require 'mmap)
CL-USER> (mmap:with-mmap (mmap-addr #P"/tmp/foo.dat" nil nil)
            (mmap:get-big-endian mmap-addr 0 8))
```

**Low-Level Writing**

An example write operation putting characters of alphabet plus Newline into
a text file:

```lisp
CL-USER> (mmap:with-mmap (addr #P"/tmp/foo.txt" 0 27
                                  :direction :output)
           (loop for x upfrom (char-code #\A) to (char-code #\Z)
                 for offset upfrom 0
                 do (mmap:put-byte addr offset x))
           (mmap:put-byte addr 26 (char-code #\Newline)))
10
CL-USER> 
```

There is also a `sync` function for persisting, which is closer to the name
of the actual POSIX system-call, msync.

There are other tunables at this level, such as advising the OS of random
access usage pattern, but POSIX allows your OS to ignore these requests.

## Exported Symbols

The **lower-level MMAP package** uses names with `get-` and `put-`
(instead of `read-` and `write-`, respectively).

For every **get-** operation, there is an equivalent **put-** function:

* `with-mmap` - macro ensuring that the specificed memory-mapped file opened
  is safely unmapped afterwards
	+ In the case of having opened the file as read/write or write-only,
	  modifications are automatically persisted upon a clean file close;
	  i.e., "clean" meaning *before* UNWIND-PROTECT's cleanup form
	+ However, your OS may perform an intermittent save at any time
* `mmap` - opens and maps a file using simiarl options as
  [`OPEN`](http://www.lispworks.com/documentation/HyperSpec/Body/f_open.htm),
   calls POSIX `open()` and `mmap()`
* `munmap` - remove memory-map and close file
* `get-byte`, `get-char` - read one byte or character from memory-mapped file
* `get-byte-vector` - read vector of single byte elements such as
  ISO-8859-1 or ASCII characters
	+ Further processing would be required for handling UTF-8 properly
	+ See also: UTF8-OCTETS-TO-STRING
	  in [com.google.base](http://quickdocs.org/com.google.base/api)
* `get-big-endian` and `get-little-endian` - operate on multiple bytes as
  a single value
	+ Bytes for unsigned integer #x01020304 stored using Big Endian would
	  appear as octets equivalent to vector #(01 02 03 04): from low
	  address to high
	+ Conversely, same vector stored using Little Endian would appear
	  reversed in mmap file: #(04 03 02 01)
	+ Remember: Internet Protocol's "network byte order" is Big Endian
*  `get-big-endian-vector` and `get-little-endian-vector` - for
  multi-byte elements within a vector
* `sync` - commit pending writes of entire mapped file or a subset within
  mapped file, respectively
	+ Note: the OS may perform an intermittent save, so these commands
      simply give the means to persist on demand

## Dependencies

Only these packages are required to build and use this library:

* [Alexandria](https://common-lisp.net/project/alexandria/)
* [Osicat](https://common-lisp.net/project/osicat/)
* [CFFI](https://common-lisp.net/project/cffi/)

For automated testing:

* [Prove](http://quickdocs.org/prove/)

For generating documentation:

* [cl-gendoc](http://quickdocs.org/cl-gendoc/)

## Running Tests

Tests are triggered using ASDF.  See `cl-mmap.asd` file.

```lisp
CL-USER> (require 'mmap)
CL-USER> (asdf:test-system cl-mmap :force t)
```

The `:force t` flag causes a fresh compilation.

For automated testing via Makefiles or continuous build systems, consider
setting:

```lisp
CL-USER> (setf prove:*default-reporter* :tap)
```

## Automation

Tests may be run using `make clean test` but assumes SBCL for code coverage
reports.

Similarly for documentation: `make clean doc`

## License

MIT License

Essentially, do as you wish and without warranty from authors or maintainers.
