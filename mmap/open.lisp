;;;; open.lisp

;;; The function to actually open a memory-mapped file is `mmap'
;;; within `mmap.lisp' file.

(in-package #:mmap)

;;; Semantics for CL:OPEN on POSIX-compliant systems:
;;;
;;;                      |       if-exists         |   if-does-not-exist     |
;;;                      |       and exists        |   and doesn't exist     |
;;; |--------------------+-------+--------+--------+-------+--------+--------|
;;; | DIRECTION          | INPUT | OUTPUT | I/O    | INPUT | OUTPUT | I/O    |
;;; |--------------------+-------+--------+--------+-------+--------+--------|
;;; | :append            |       | append | append |       | create | create |
;;; | :create (*)        |       |        |        |       | create | create |
;;; | :error             | n/a   | error  | error  | error | error  | error  |
;;; | :new-version       |       | rename | rename |       | create | create |
;;; | :rename            |       | rename | rename |       | create | create |
;;; | :rename-and-delete |       | rename | rename |       | create | create |
;;; | :overwrite         |       | trunc  |        |       | create | create |
;;; | :supersede         |       | trunc  | trunc  |       | create | create |
;;; | nil                | nil   | nil    | nil    | nil   | nil    | nil    |
;;; | (unspecified)      |       | rename |        | error | create | create |
;;; 
;;; Empty cells imply nothing reasonable to do there beyond opening the file.
;;; 
;;; The only truly non-applicable position is whether or not to generate an
;;; error for `direction :input` and `if-exists :error`, but the programmer can
;;; simply use PROBE-FILE for that use case.  However, for `direction :io` use
;;; of `if-exists :error` would apply to prevent overwriting existing data.
;;;
;;; (*) From HyperSpec regarding `if-does-not-exist :create`: after
;;; creating file, "Processing continues as if the file had already
;;; existed but no processing as directed by if-exists is performed."

(defun validate-open-args (filename offset length-bytes
                           &key direction if-exists (if-does-not-exist nil ne?)
                             mode posix-open-flags posix-mmap-flags)
  "Validate arguments similar to OPEN yet constrained by POSIX mmap()
  semantics because underlying operating system facilities are finicky.
  If and only if preparing your application for **production** builds:
  either replace this function binding or compile this entire library
  with `:open-mmap-without-safety' in `*FEATURES*' to prevent this
  function from being called."
  (declare (ignore if-exists	; Protected with ECASE in calling scope
		   posix-open-flags posix-mmap-flags))
  (when offset
    (check-type offset (integer 0) "Positive Integer")
    (assert (= (mod offset +posix-page-size+) 0)
            (offset)
            "OFFSET=#x~X must be a multiple of +POSIX-PAGE-SIZE+=~D"
	    offset +posix-page-size+))
  (if length-bytes
      (check-type length-bytes (integer 0))
      (assert (null offset)
              (offset length-bytes)
              "OFFSET=~A and LENGTH-BYTES=~A must be either both NIL or both >0"
              offset length-bytes))
  (when direction
    (assert (or (eq direction :input)
		(and (eq direction :output) length-bytes (> length-bytes 0))
		(and (eq direction :io) (or (and ne? (null if-does-not-exist))
					    (and (not ne?) (probe-file filename))
					    t)))
	    (length-bytes)
	    "~&When creating new mmap files, specify LENGTH-BYTES > 0"))
  (when mode
    (check-type mode (integer 0 #o0777)
		"#o0600 for privacy; #o0644, readable by all; see: `man chmod`")))

(defun rename (pathname &optional rename-and-delete)
  "Renames a file according to backup convention using POSIX mechanism."
  (let* ((posix-filename (namestring pathname))
	 (posix-new-name (concatenate 'string
				      posix-filename *file-rename-suffix*)))
    #+verbose (format *error-output* "Renaming pathname=~S new=~S~%"
		      posix-filename posix-new-name)
    (trap (osicat-posix:rename posix-filename posix-new-name))
    (when rename-and-delete
      #+verbose (format *error-output* "Deleting pathname=~S~%" posix-new-name)
      (trap (osicat-posix:unlink posix-new-name)))))

(defun process-open-flags (filename direction if-exists if-exists_given?
                           if-does-not-exist if-does-not-exist_given?)
  "Set POSIX bitflags suitable for open() while processing params
  similarly as for OPEN, thus nonsense combinations are ignored."
  #+verbose
  (format t "open ~A ~A if-exists=~@[~A~] if-does-not-exist=~@[~A~]~%"
	  filename direction (and if-exists_given? if-exists)
	  (and if-does-not-exist_given? if-does-not-exist))
  (multiple-value-bind (input output open-flags protection-flags)
      (ecase direction
	(:input  (values t nil osicat-posix:o-rdonly osicat-posix:prot-read))
	(:output (values nil t osicat-posix:o-rdwr (logior
						    osicat-posix:prot-write
						    osicat-posix:prot-read)))
	(:io     (values t   t osicat-posix:o-rdwr (logior
						    osicat-posix:prot-write
						    osicat-posix:prot-read))))
    (if (probe-file filename)
	(cond
	  (if-exists_given?
	   (ecase if-exists
	     (:error (error 'file-error :pathname filename))
	     ((nil) (return-from process-open-flags nil))
	     ((:overwrite :append :supersede) nil)
	     ((:new-version :rename :rename-and-delete)
	      (rename filename (eq if-exists :rename-and-delete)))))
	  ((and (not input) output)
	   (setf open-flags (logior open-flags osicat-posix:o-creat))
	   (rename filename)))
	(cond
	  (if-does-not-exist_given?
	   (ecase if-does-not-exist
	     (:error (error 'file-error :pathname filename))
	     ((nil) (return-from process-open-flags nil))
	     (:create
	      (setf open-flags (logior open-flags osicat-posix:o-creat)))))
	  (output
	   (setf open-flags (logior open-flags osicat-posix:o-creat)))))
    (when output
      (when (and if-exists_given? (= (logior open-flags #x03) #x03))
	(ecase if-exists
	  ((:new-version :rename :rename-and-delete :error)
	   (setf open-flags (logior open-flags osicat-posix:o-creat)))
	  (:supersede
	   (setf open-flags (logior open-flags osicat-posix:o-trunc)))
	  (:overwrite
	   nil)
	  (:append
	   (setf open-flags (logior open-flags osicat-posix:o-append)
		 protection-flags (logior osicat-posix:prot-write
					  osicat-posix:prot-read))))))
    #+verbose
    (format t " open-flags=~A protection-flags=~A~%" open-flags protection-flags)
    (values open-flags protection-flags)))

(defun set-mapping-flag (direction)
  "Use reasonable defaults for mapping-type based upon DIRECTION.
  For DIRECTION of :input (read only), it's resonable keeping data
  restricted to this runtime process, thus defaults to `MAP-PRIVATE'.
  For DIRECTION of :output or :io, it's reasonable persisting writes
  upon file close, so these use `MAP-SHARED' as their defaults."
  (ecase direction
    (:input        osicat-posix:map-private)
    ((:output :io) osicat-posix:map-shared)))

(defun allocate-writable-region (fd length-bytes)
  "Add space within an open file.
  FD is file-descriptor from POSIX system-call, open(2).
  LENGTH-BYTES is number of bytes to be allocated.
  A new mmap file-- or extending one-- requires the mapped region to exist
  within the file prior to reading or writing, else get `bus error' or
  similarly confusing error message.
  Space may be efficiently allocated by creating a sparse file: one
  which needs only a single byte written as the upper bound of the
  allocated region, and value may be #\Null.  Attempting to read the
  file in this state will fetch only #\Null for the intermediate span
  plus the single value we have written, which is also #\Null for
  consistency.  Creating such a sparse file is fast and efficient in
  time with minimal calls into OS kernel."
  (trap (osicat-posix:lseek fd (1- length-bytes) osicat-posix:seek-set))
  (cffi:with-foreign-string (buffer (princ-to-string #\Null))
    (trap (osicat-posix:write fd buffer 1))))
