;;;; globals.lisp

(in-package #:mmap)

(defconstant +posix-page-size+ (osicat-posix:getpagesize)
  "Some systems like Linux and BSD Unix require mmapped file OFFSET
  being aligned to page boundaries when opening the map.

  Mapped files with length less than some multiple of page size may be
  padded with nulls, and any writes to this region might not be
  persisted.
  <https://www.freebsd.org/cgi/man.cgi?query=mmap&sektion=2&manpath>
  <http://man7.org/linux/man-pages/man2/mmap.2.html>")

(defvar *file-rename-suffix* "~"
  "When opening a file that exists and calling scope specifies
  IF-EXISTS :rename or :new-version, this is the suffix applied to
  the existing file's name.")

(defvar *default-file-mode* nil
  "For POSIX open(2), use this value for file ownership mode
  when creating new files, which includes rename and new-version.

  When NIL, defaults to read+write and private to this user.

  See Unix Manual for `chmod` for possible numeric values.
  e.g., #o0644 is user readable+writable and group, other readable;
  and #o0600 is private to just this user.")

(defvar *default-posix-open-flags* nil
  "For advanced use cases, POSIX-OPEN-FLAGS and POSIX-MMAP-FLAGS specify
  optional bit-flags as documented in Unix manual open(2) and mmap(2),
  respectively.

  Value should be either NIL or an unsigned integer.
  When non-NIL, these bit-flags are applied via LOGIOR.")

(defvar *default-posix-mmap-flags* nil
  "For advanced use cases, POSIX-OPEN-FLAGS and POSIX-MMAP-FLAGS specify
  optional bit-flags as documented in Unix manual open(2) and mmap(2),
  respectively.

  Value should be either NIL or an unsigned integer.
  When non-NIL, these bit-flags are applied via LOGIOR.")

(defvar %fd%)
(defvar %actual-size%)
(setf (documentation '%fd% 'variable)
      "Binding only valid within BODY of `with-mmap' macro"
      (documentation '%actual-size% 'variable)
      "Binding only valid within BODY of `with-mmap' macro")
