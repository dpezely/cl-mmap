;;;; put.lisp - write within an opened mmap

(in-package #:mmap)

(defun put-byte (mmap-addr offset value)
  "Writes single byte VALUE to a memory-mapped file pointed to by
  MMAP-ADDR pointer, offset by OFFSET bytes.
  Returns: VALUE on success or NIL."
  (declare (type (integer 0 255) value))
  (ignore-mmap-errors
   (setf (cffi:mem-aref mmap-addr :uint8 offset)
	 value)))

(defun put-char (mmap-addr offset single-byte-character)
  "Writes SINGLE-BYTE-CHARACTER to a memory-mapped file
  pointed to by MMAP-ADDR pointer, offset by OFFSET bytes.

  Beware this function is only suitable for writing single byte
  ISO-8859-1 or ASCII characters or similar.  It's ill-suited for
  UTF-8 values of more than one byte.

  Returns: VALUE on success or NIL."
  (declare (type character single-byte-character))
  (let ((byte (char-code single-byte-character)))
    (ignore-mmap-errors
     (setf (cffi:mem-aref mmap-addr :char offset) byte))))

(defun put-big-endian (mmap-addr offset value n-bytes)
  "Write VALUE within N-BYTES in big-endian byte order
  to memory-mapped file pointed to by the MMAP-ADDR pointer,
  offset by OFFSET bytes.
  Returns: VALUE on success or NIL."
  (declare (type (integer 0) offset value n-bytes))
  (let ((n-bits (* n-bytes 8)))
    (loop
       for i downfrom (- n-bits 8) downto 0 by 8
       for cursor upfrom offset
       for byte = (ldb (byte 8 i) value)
       unless (ignore-mmap-errors
	       (setf (cffi:mem-aref mmap-addr :uint8 cursor) byte))
       do (return-from put-big-endian nil)))
  value)

(defun put-little-endian (mmap-addr offset value n-bytes)
  "Writes VALUE within N-BYTES in little-endian order to memory-mapped file
  pointed to by the MMAP-ADDR pointer, offset by OFFSET bytes.
  Returns: VALUE on success or NIL."
  (declare (type (integer 0) offset value n-bytes))
  (let ((n-bits (* n-bytes 8)))
    (loop
       for i below n-bits by 8
       for cursor upfrom offset
       for byte = (ldb (byte 8 i) value)
       unless (ignore-mmap-errors
	       (setf (cffi:mem-aref mmap-addr :uint8 cursor) byte))
       do (return-from put-little-endian nil)))
  value)

(defun put-byte-vector (mmap-addr offset vector)
  "Writes single byte VALUE to a memory-mapped file pointed to by
  MMAP-ADDR pointer, offset by OFFSET bytes.
  Returns: VECTOR on success of NIL."
  (declare (type (integer 0) offset) (type vector vector))
  (let ((vector-length (length vector)))
    (loop
       for i below vector-length
       for cursor upfrom offset
       for byte = (elt vector i)
       unless (ignore-mmap-errors
	       (setf (cffi:mem-aref mmap-addr :uint8 cursor) byte))
       do (return-from put-byte-vector nil)))
  vector)

(defun put-big-endian-vector (mmap-addr offset vector
				 n-bytes-per-element)
  "Writes VECTOR within N-BYTES-PER-ELEMENT in big-endian order
  to memory-mapped file pointed to by the MMAP-ADDR pointer,
  offset by OFFSET bytes.
  Returns: VECTOR on success or NIL."
  (declare (type (integer 0) offset n-bytes-per-element)
	   (type vector vector))
  (let ((vector-length (length vector))
	(n-bits (* n-bytes-per-element 8)))
    (loop
       for base-cursor upfrom offset by n-bytes-per-element
       for i below vector-length
       for value  = (elt vector i)
       do (loop
	     for value-cursor upfrom 0
	     for bit-offset downfrom (- n-bits 8) downto 0 by 8
	     for byte = (ldb (byte 8 bit-offset) value)
	     unless (ignore-mmap-errors
		     (setf (cffi:mem-aref mmap-addr :uint8
					  (+ base-cursor value-cursor))
			   byte))
	     do (return-from put-big-endian-vector nil))))
  vector)

(defun put-little-endian-vector (mmap-addr offset vector
				    n-bytes-per-element)
  "Writes VECTOR within N-BYTES-PER-ELEMENT in little-endian order
  to memory-mapped file pointed to by the MMAP-ADDR pointer,
  offset by OFFSET bytes.
  Returns: VECTOR on success or NIL."
  (declare (type (integer 0) offset n-bytes-per-element)
	   (type vector vector))
  (let ((vector-length (length vector))
	(n-bits (* n-bytes-per-element 8)))
    (loop
       for base-cursor upfrom offset by n-bytes-per-element
       for i below vector-length
       for value  = (elt vector i)
       do (loop
	     for value-cursor upfrom 0
	     for bit-offset below n-bits by 8
	     for byte = (ldb (byte 8 bit-offset) (elt vector i))
	     unless (ignore-mmap-errors
		     (setf (cffi:mem-aref mmap-addr :uint8
					  (+ base-cursor value-cursor))
			   byte))
	     do (return-from put-little-endian-vector nil))))
  vector)
