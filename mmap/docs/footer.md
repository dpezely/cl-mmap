## Links

Clone [repo](https://gitlab.com/dpezely/cl-mmap), which provides:

- Higher level [cl-mmap](https://gitlab.com/dpezely/cl-mmap/cl-mmap) package
- Lower level [mmap](https://gitlab.com/dpezely/cl-mmap/mmap) package
