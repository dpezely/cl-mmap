;;;; run-gendoc.lisp

;; Usage: run this from .. (top-level directory for this package)
;; so that REQUIRE can find dev version rather than system package
;;
;; sbcl --load "docs/render-emacs-lisp-mode-markup.lisp" \
;;      --load "docs/gendoc-script.lisp"

(in-package :cl-user)

(defvar *manual-page* (merge-pathnames "docs/mmap.html")
  "Where to write the generated file, from perspective of `make`
  which runs from parent directory")

(ql:quickload 'cl-gendoc)
(ql:quickload '3bmd-ext-code-blocks)

;; See ../Makefile which sets this shell environment variable:
(let ((hs-dir (pathname (or (uiop:getenv "HYPERSPEC_DIR")
			    "/usr/local/lisp/HyperSpec/"))))
  (setf clhs-lookup::*hyperspec-map-file* (merge-pathnames "Data/Map_Sym.txt" hs-dir)
	3bmd-code-blocks:*code-blocks* t))

;; Avoid using #'ql:quickload here-- to ensure getting our local version
(require 'mmap)

;; This _rewrites_ the doc-string of each function for Markdown & HTML:
(let ((package (find-package 'mmap)))
  (do-external-symbols (sym package)
    (cond
      ((documentation sym 'variable)
       (let* ((doc-string (documentation sym 'variable))
	      (new-doc-string (subst-emacs-lisp-mode-markup package doc-string)))
	 (when (not (eq doc-string new-doc-string))
	   (setf (documentation sym 'variable) new-doc-string))))
      ((documentation sym 'function)
       (let* ((doc-string (documentation sym 'function))
	      (new-doc-string (subst-emacs-lisp-mode-markup package doc-string)))
	 (when (not (eq doc-string new-doc-string))
	   (setf (documentation sym 'function) new-doc-string)))))))

(with-open-file (out #P"docs/generated-timestamps.md"
		     :direction :output
		     :if-exists :append)
  (format out "* Quicklisp Distribution: ~A~%"
	  (ql-dist:version (ql-dist:dist "quicklisp"))))

(gendoc:gendoc
 (:title "MMAP Reference - Low-Level API"
         :output-filename *manual-page*
         :css "style.css")
 (:mdf "docs/header.md")
 (:apiref :mmap)
 (:mdf "docs/generated-conditions.md")
 (:mdf "docs/contributing.md")
 (:mdf "docs/footer.md")
 (:mdf "docs/generated-timestamps.md"))

(format t "~&file://~A~%" (pathname *manual-page*))
