## Contributing Or Building Upon This Library

During development or deeper exploration of this package, it may be useful
pushing certain keywords onto `*features*` list.

(Remember, of course, that a fresh compile will be necessary after updating
`*features*`.)

* `(push :verbose *features*)`
	+ Display logging messages to standard output.
	+ Output will be mostly from *opening* memory-mapped files.
* `(push :allow-low-level-mmap-errors *features*)`
	+ Certain mmap related operations such as writing beyond file extent
	  signals implementation-dependent errors.  Rather than tracking the
	  multitude of specific errors (see `man mmap(2)`), they are coalesced
	  into a package-specific condition.
	+ This compilation flag disables the feature and may be useful while
      developing for this package or building upon it.
	+ Be sure to visit code where this flag is used, and confirm granularity
      of the behaviour.
* `(push :handle-only-known-mmap-errors *features*)`
    + Similar to `allow-low-level-mmap-errors` above, setting this one
      disables the final match of generic `error` in HANDLER-CASE that gets
      wrapped around various POSIX system-calls.
	+ This compilation flag may also be useful while developing for this
      package or building upon it.

When updating tests:

* `(setf prove:*debug-on-error* t)`
	+ Use this within your development environment, so errors in test case
      code or package code may signal an error without it being masked by
      the test framework.
	+ If using a test library or framework other than Prove, this variable
      of course would have no effect.
