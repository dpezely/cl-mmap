<!doctype html>

<html lang='en'>
  <head>
    <meta charset='UTF-8' />
<title>MMAP Reference - Low-Level API
</title>
<link rel='stylesheet' href='style.css' />
  </head>
  <body>
<article class='markdown-article' id='file-header-md'><h1>MMAP</h1>

<p><em>Common Lisp library for operating on memory-mapped files</em></p>

<p>See also: CL-MMAP package which offers a higher level API for using an MMAP
similarly to operating upon other types of files within Common Lisp.</p>

<h2>Overview</h2>

<p>On a POSIX-compliant OS including BSD Unix, Linux and macOS operate
upon memory-mapped files with behaviour similar to Common Lisp functions
such as OPEN and READ.</p>

<p>There are two inter-related Common Lisp packages here: CL-MMAP and MMAP,
offering a high-level and low-level API, respectively.</p>

<p>The higher level API offers functions with names and options familiar to
Common Lisp programmers.  For instance, apply knowledge of OPEN from CL
package to the function of same name in this one.  Several CL symbols are
shadowed.</p>

<p>Use the lower level routines for constructing higher level APIs such as
building your own database where more fine-grained control is desired.
Nothing gets shadowed by this package, as I/O functions have names beginning
with <code>get-</code> and <code>put-</code> prefix instead, as for hash-tables.</p>

<p>File operations supported include opening with DIRECTION of <code>:input</code>,
<code>:output</code> or <code>:io</code>.  Manipulate single byte or multi-byte values including
as vector elements within the mapped file.</p>

<p>Big endian and little endian operations are supported.</p>

<p>Modifications may be persisted at granularity of an entire file or
just one region.  Both modes can optionally notify other concurrent
mappings of same file to be refreshed.</p>

<p>See README for example code, list of dependencies, known limitations, etc.</p>

</article>
<article id='reference-mmap' class='apiref-article'>
  <h1>Reference: mmap
  </h1>
<section id='mmap-specials' class='section-specials'>
  <h2>Special Variables
  </h2>
<section id='apiref-*default-file-mode*' class='section-apiref-item'>
  <div class='apiref-spec'>*DEFAULT-FILE-MODE*
  </div>
  <div class='apiref-lambda'>
  </div>
  <div class='apiref-doc'><p>For POSIX open(2), use this value for file ownership mode
  when creating new files, which includes rename and new-version.</p>

<p>When NIL, defaults to read+write and private to this user.</p>

<p>See Unix Manual for <code>chmod</code> for possible numeric values.</p>

<p>e.g., #o0644 is user readable+writable and group, other readable;
  and #o0600 is private to just this user.</p>

  </div>
</section>
<section id='apiref-*default-posix-mmap-flags*' class='section-apiref-item'>
  <div class='apiref-spec'>*DEFAULT-POSIX-MMAP-FLAGS*
  </div>
  <div class='apiref-lambda'>
  </div>
  <div class='apiref-doc'><p>For advanced use cases, POSIX-OPEN-FLAGS and POSIX-MMAP-FLAGS specify
  optional bit-flags as documented in Unix manual open(2) and mmap(2),
  respectively.</p>

<p>Value should be either NIL or an unsigned integer.</p>

<p>When non-NIL, these bit-flags are applied via LOGIOR.</p>

  </div>
</section>
<section id='apiref-*default-posix-open-flags*' class='section-apiref-item'>
  <div class='apiref-spec'>*DEFAULT-POSIX-OPEN-FLAGS*
  </div>
  <div class='apiref-lambda'>
  </div>
  <div class='apiref-doc'><p>For advanced use cases, POSIX-OPEN-FLAGS and POSIX-MMAP-FLAGS specify
  optional bit-flags as documented in Unix manual open(2) and mmap(2),
  respectively.</p>

<p>Value should be either NIL or an unsigned integer.</p>

<p>When non-NIL, these bit-flags are applied via LOGIOR.</p>

  </div>
</section>
<section id='apiref-*file-rename-suffix*' class='section-apiref-item'>
  <div class='apiref-spec'>*FILE-RENAME-SUFFIX*
  </div>
  <div class='apiref-lambda'>
  </div>
  <div class='apiref-doc'><p>When opening a file that exists and calling scope specifies
  IF-EXISTS :rename or :new-version, this is the suffix applied to
  the existing file's name.</p>

  </div>
</section>
<section id='apiref-+posix-page-size+' class='section-apiref-item'>
  <div class='apiref-spec'>+POSIX-PAGE-SIZE+
  </div>
  <div class='apiref-lambda'>
  </div>
  <div class='apiref-doc'><p>Some systems like Linux and BSD Unix require mmapped file OFFSET
  being aligned to page boundaries when opening the map.</p>

<p>Mapped files with length less than some multiple of page size may be
  padded with nulls, and any writes to this region might not be
  persisted.</p>

<p><a href="https://www.freebsd.org/cgi/man.cgi?query=mmap&sektion=2&manpath">https://www.freebsd.org/cgi/man.cgi?query=mmap&sektion=2&manpath</a>
  <a href="http://man7.org/linux/man-pages/man2/mmap.2.html">http://man7.org/linux/man-pages/man2/mmap.2.html</a></p>

  </div>
</section>
</section>
<section id='mmap-functions' class='section-functions'>
  <h2>Functions
  </h2>
<section id='apiref-get-big-endian' class='section-apiref-item'>
  <div class='apiref-spec'>GET-BIG-ENDIAN
  </div>
  <div class='apiref-lambda'>(mmap-addr offset n-bytes)
  </div>
  <div class='apiref-doc'><p>Reads and returns N-BYTES in big-endian byte order
  from a memory-mapped file pointed to by the MMAP-ADDR pointer,
  offset by OFFSET bytes.</p>

<p>Returns: value on success or NIL.</p>

  </div>
</section>
<section id='apiref-get-big-endian-vector' class='section-apiref-item'>
  <div class='apiref-spec'>GET-BIG-ENDIAN-VECTOR
  </div>
  <div class='apiref-lambda'>(mmap-addr offset vector-length n-bytes-per-element)
  </div>
  <div class='apiref-doc'><p>Reads and returns VECTOR with elements of N-BYTES in big-endian order
  from a memory-mapped file pointed to by the MMAP-ADDR pointer,
  offset by OFFSET bytes.</p>

<p>Returns: populated VECTOR on success or NIL.</p>

  </div>
</section>
<section id='apiref-get-byte' class='section-apiref-item'>
  <div class='apiref-spec'>GET-BYTE
  </div>
  <div class='apiref-lambda'>(mmap-addr offset)
  </div>
  <div class='apiref-doc'><p>Reads and returns one byte from a memory-mapped file pointed to by
  the MMAP-ADDR pointer, offset by OFFSET bytes.</p>

  </div>
</section>
<section id='apiref-get-byte-vector' class='section-apiref-item'>
  <div class='apiref-spec'>GET-BYTE-VECTOR
  </div>
  <div class='apiref-lambda'>(mmap-addr offset vector-length)
  </div>
  <div class='apiref-doc'><p>Reads and returns VECTOR with elements of one byte from a memory-mapped file
  pointed to by the MMAP-ADDR pointer, offset by OFFSET bytes.</p>

<p>Returns: populated VECTOR on success or NIL.</p>

  </div>
</section>
<section id='apiref-get-char' class='section-apiref-item'>
  <div class='apiref-spec'>GET-CHAR
  </div>
  <div class='apiref-lambda'>(mmap-addr offset)
  </div>
  <div class='apiref-doc'><p>Reads and returns a character from a memory-mapped file pointed to by
  the MMAP-ADDR pointer, offset by OFFSET bytes.</p>

  </div>
</section>
<section id='apiref-get-little-endian' class='section-apiref-item'>
  <div class='apiref-spec'>GET-LITTLE-ENDIAN
  </div>
  <div class='apiref-lambda'>(mmap-addr offset n-bytes)
  </div>
  <div class='apiref-doc'><p>Reads and returns N-BYTES in little-endian byte order
  from a memory-mapped file pointed to by the MMAP-ADDR pointer,
  offset by OFFSET bytes.</p>

<p>Returns: value on success or NIL.</p>

  </div>
</section>
<section id='apiref-get-little-endian-vector' class='section-apiref-item'>
  <div class='apiref-spec'>GET-LITTLE-ENDIAN-VECTOR
  </div>
  <div class='apiref-lambda'>(mmap-addr offset vector-length n-bytes-per-element)
  </div>
  <div class='apiref-doc'><p>Reads and returns VECTOR with elements of N-BYTES in little-endian order
  from a memory-mapped file pointed to by the MMAP-ADDR
  pointer, offset by OFFSET bytes.</p>

<p>Returns: populated VECTOR on success or NIL.</p>

  </div>
</section>
<section id='apiref-mmap' class='section-apiref-item'>
  <div class='apiref-spec'>MMAP
  </div>
  <div class='apiref-lambda'>(pathname offset length-bytes &amp;rest options &amp;key (direction :input)
          (if-exists nil if-e-given) (if-does-not-exist nil if-ne-given)
          (mode *default-file-mode*)
          (posix-open-flags *default-posix-open-flags*)
          (posix-mmap-flags *default-posix-mmap-flags*))
  </div>
  <div class='apiref-doc'><p>Opens FILENAME and uses mmap() to memory-map region starting with OFFSET
  and spanning LENGTH-BYTES.  Keyword options similar to <code>OPEN</code> in CL.</p>

<p>See <a href="#apiref-with-mmap">with-mmap</a> macro description for details.</p>

<p>Returns: VALUES of address, file-descriptor and length in bytes.</p>

  </div>
</section>
<section id='apiref-mremap' class='section-apiref-item'>
  <div class='apiref-spec'>MREMAP
  </div>
  <div class='apiref-lambda'>(mmap-addr old-length-bytes new-length-bytes fd)
  </div>
  <div class='apiref-doc'><p>Grow/shrink an existing memory-mapped region and corresponding file.</p>

<p>MMAP-ADDR is address as returned by <a href="#apiref-mmap">mmap</a>.</p>

<p>OLD-LENGTH-BYTES corresponds to second VALUE returned by <a href="#apiref-mmap">mmap</a>.</p>

<p>NEW-LENGTH-BYTES specifies new target size and may be larger or smaller.</p>

<p>FD is file-descriptor from third VALUE returned by <a href="#apiref-mmap">mmap</a>.</p>

<p><strong>This operation <em>may change address</em> of the resized memory-map,
  so value returned here <em>must</em> become the new MMAP-ADDR for all
  subsequent operations using this library.</strong></p>

<p>Returns: new value for MMAP-ADDR on all subsequent operations.</p>

  </div>
</section>
<section id='apiref-munmap' class='section-apiref-item'>
  <div class='apiref-spec'>MUNMAP
  </div>
  <div class='apiref-lambda'>(mmap-addr fd length-bytes)
  </div>
  <div class='apiref-doc'><p>Removes file mapping at the address, MMAP-ADDR, with range FILE-SIZE
  and closes file descriptor, FD.  These were likely opened via <a href="#apiref-mmap">mmap</a>.</p>

<p>May signal condition within <a href="#apiref-posix-error">posix-error</a> or <a href="#apiref-mmap-error">mmap-error</a> hierarchies.</p>

<p>Returns: T if action taken, otherwise NIL.</p>

  </div>
</section>
<section id='apiref-put-big-endian' class='section-apiref-item'>
  <div class='apiref-spec'>PUT-BIG-ENDIAN
  </div>
  <div class='apiref-lambda'>(mmap-addr offset value n-bytes)
  </div>
  <div class='apiref-doc'><p>Write VALUE within N-BYTES in big-endian byte order
  to memory-mapped file pointed to by the MMAP-ADDR pointer,
  offset by OFFSET bytes.</p>

<p>Returns: VALUE on success or NIL.</p>

  </div>
</section>
<section id='apiref-put-big-endian-vector' class='section-apiref-item'>
  <div class='apiref-spec'>PUT-BIG-ENDIAN-VECTOR
  </div>
  <div class='apiref-lambda'>(mmap-addr offset vector n-bytes-per-element)
  </div>
  <div class='apiref-doc'><p>Writes VECTOR within N-BYTES-PER-ELEMENT in big-endian order
  to memory-mapped file pointed to by the MMAP-ADDR pointer,
  offset by OFFSET bytes.</p>

<p>Returns: VECTOR on success or NIL.</p>

  </div>
</section>
<section id='apiref-put-byte' class='section-apiref-item'>
  <div class='apiref-spec'>PUT-BYTE
  </div>
  <div class='apiref-lambda'>(mmap-addr offset value)
  </div>
  <div class='apiref-doc'><p>Writes single byte VALUE to a memory-mapped file pointed to by
  MMAP-ADDR pointer, offset by OFFSET bytes.</p>

<p>Returns: VALUE on success or NIL.</p>

  </div>
</section>
<section id='apiref-put-byte-vector' class='section-apiref-item'>
  <div class='apiref-spec'>PUT-BYTE-VECTOR
  </div>
  <div class='apiref-lambda'>(mmap-addr offset vector)
  </div>
  <div class='apiref-doc'><p>Writes single byte VALUE to a memory-mapped file pointed to by
  MMAP-ADDR pointer, offset by OFFSET bytes.</p>

<p>Returns: VECTOR on success of NIL.</p>

  </div>
</section>
<section id='apiref-put-char' class='section-apiref-item'>
  <div class='apiref-spec'>PUT-CHAR
  </div>
  <div class='apiref-lambda'>(mmap-addr offset single-byte-character)
  </div>
  <div class='apiref-doc'><p>Writes SINGLE-BYTE-CHARACTER to a memory-mapped file
  pointed to by MMAP-ADDR pointer, offset by OFFSET bytes.</p>

<p>Beware this function is only suitable for writing single byte
  ISO-8859-1 or ASCII characters or similar.  It's ill-suited for
  UTF-8 values of more than one byte.</p>

<p>Returns: VALUE on success or NIL.</p>

  </div>
</section>
<section id='apiref-put-little-endian' class='section-apiref-item'>
  <div class='apiref-spec'>PUT-LITTLE-ENDIAN
  </div>
  <div class='apiref-lambda'>(mmap-addr offset value n-bytes)
  </div>
  <div class='apiref-doc'><p>Writes VALUE within N-BYTES in little-endian order to memory-mapped file
  pointed to by the MMAP-ADDR pointer, offset by OFFSET bytes.</p>

<p>Returns: VALUE on success or NIL.</p>

  </div>
</section>
<section id='apiref-put-little-endian-vector' class='section-apiref-item'>
  <div class='apiref-spec'>PUT-LITTLE-ENDIAN-VECTOR
  </div>
  <div class='apiref-lambda'>(mmap-addr offset vector n-bytes-per-element)
  </div>
  <div class='apiref-doc'><p>Writes VECTOR within N-BYTES-PER-ELEMENT in little-endian order
  to memory-mapped file pointed to by the MMAP-ADDR pointer,
  offset by OFFSET bytes.</p>

<p>Returns: VECTOR on success or NIL.</p>

  </div>
</section>
<section id='apiref-random-access' class='section-apiref-item'>
  <div class='apiref-spec'>RANDOM-ACCESS
  </div>
  <div class='apiref-lambda'>(mmap-addr offset length-bytes fd)
  </div>
  <div class='apiref-doc'><p>Advise OS that the mmap is to be used with random-access pattern,
  so prefetching would be suboptimal.</p>

<p>Because this is only <em>advising</em> the OS, there are no guarantees of
  performance characteristics changing.</p>

<p>Params match positional arguments and returned values of <a href="#apiref-mmap">mmap</a> with
  same names.</p>

<p>Usage hint: only when reading <em>less</em> than 4 KB or <a href="#apiref-+posix-page-size+">+posix-page-size+</a>
  at a time <em>and</em> stepping through the memory-map by orders of
  magnitude more, would this setting be useful.  Otherwise, it may
  degrade performance significantly.</p>

  </div>
</section>
<section id='apiref-sync' class='section-apiref-item'>
  <div class='apiref-spec'>SYNC
  </div>
  <div class='apiref-lambda'>(mmap-addr offset length-bytes &amp;key notify async)
  </div>
  <div class='apiref-doc'><p>Finish writes immediately so they become persisted.</p>

<p>A POSIX-compliant OS will sync the mapped region eventually <em>without</em>
  calling this function.</p>

<p>Params: MMAP-ADDR is base address from return of <a href="#apiref-mmap">mmap</a> function.</p>

<p>Values for OFFSET and LENGTH-BYTES should be idential as those
  supplied to <a href="#apiref-mmap">mmap</a>.</p>

<p>When keyword param NOTIFY is non-NIL, instructs OS kernel to
  invalidate other mappings of this file to ensure getting fresh
  values just written.  This function should only be called when <a href="#apiref-mmap">mmap</a>
  was opened with direction :output or :io.</p>

<p>When keyword param ASYNC is non-NIL, indicates asynchronous mode so
  calling scope continues without waiting.</p>

<p>Returns: T on success or signals condition of <a href="#apiref-posix-error">posix-error</a>for details.</p>

  </div>
</section>
<section id='apiref-validate-open-args' class='section-apiref-item'>
  <div class='apiref-spec'>VALIDATE-OPEN-ARGS
  </div>
  <div class='apiref-lambda'>(filename offset length-bytes &amp;key direction if-exists
 (if-does-not-exist nil ne?) mode posix-open-flags posix-mmap-flags)
  </div>
  <div class='apiref-doc'><p>Validate arguments similar to OPEN yet constrained by POSIX mmap()
  semantics because underlying operating system facilities are finicky.</p>

<p>If and only if preparing your application for <strong>production</strong> builds:
  either replace this function binding or compile this entire library
  with <code>:open-mmap-without-safety</code> in <code>*FEATURES*</code> to prevent this
  function from being called.</p>

  </div>
</section>
</section>
<section id='mmap-macros' class='section-macros'>
  <h2>Macros
  </h2>
<section id='apiref-with-mmap' class='section-apiref-item'>
  <div class='apiref-spec'>WITH-MMAP
  </div>
  <div class='apiref-lambda'>((mmap-addr filename offset length-bytes &amp;rest options &amp;key direction if-exists
  if-does-not-exist random-access mode posix-open-flags posix-mmap-flags)
 &amp;body body)
  </div>
  <div class='apiref-doc'><p>Opens FILENAME and uses mmap() to memory-map region starting with OFFSET
  and spanning LENGTH-BYTES, each of which should be a multiple page size.</p>

<p>Keyword options are similar to <code>OPEN</code> in CL package.</p>

<p>First parameter, MMAP-ADDR, becomes a captured/anaphoric variable
  for use within BODY.</p>

<p>Maps whole file when DIRECTION is <code>:input</code> with both OFFSET and
  LENGTH-BYTES are both NIL.</p>

<p>When IF-DOES-NOT-EXIST is <code>:create</code> or <code>:supersede</code>, specify LENGTH-BYTES &gt; 0
  and ideally as a multiple of page size.  See <a href="#apiref-+posix-page-size+">+posix-page-size+</a>.</p>

<p>File permissions may be set with MODE; e.g., #o0644 for world-readable.</p>

<p>For advanced use cases, POSIX-OPEN-FLAGS and POSIX-MMAP-FLAGS specify
  optional bit-flags as documented in Unix manual open(2) and mmap(2),
  respectively.  When supplied, these bit-flags are applied via LOGIOR.</p>

<p>When RANDOM-ACCESS is non-NIL, advise OS that prefetch would be suboptimal.</p>

<p>File will be unmapped and closed upon completion of BODY.</p>

<p>This macro uses variable capture for MMAP-ADDR for use within BODY.</p>

<p>May signal condition within <a href="#apiref-posix-error">posix-error</a> or <a href="#apiref-mmap-error">mmap-error</a> hierarchies.</p>

<p>Returns: resulting value from BODY.</p>

  </div>
</section>
</section>
</article>
<article class='markdown-article' id='file-generated-conditions-md'><!-- GENERATED: see Makefile -->

<h2>Conditions</h2>

<p>Custom errors may be signaled:</p>

<ul>
<li>mmap-error</li>
<li>end-of-mmap-error</li>
<li>invalid-mmap-access-error</li>
<li>posix-error</li>
<li>open-posix-error</li>
<li>fstat-posix-error</li>
<li>lseek-posix-error</li>
<li>write-posix-error</li>
<li>mmap-posix-error</li>
<li>msync-posix-error</li>
<li>close-posix-error</li>
<li>dummy</li>
</ul>

</article>
<article class='markdown-article' id='file-contributing-md'><h2>Contributing Or Building Upon This Library</h2>

<p>During development or deeper exploration of this package, it may be useful
pushing certain keywords onto <code>*features*</code> list.</p>

<p>(Remember, of course, that a fresh compile will be necessary after updating
<code>*features*</code>.)</p>

<ul>
<li><code>(push :verbose *features*)</code>

<ul>
<li>Display logging messages to standard output.</li>
<li>Output will be mostly from <em>opening</em> memory-mapped files.</li>
</ul></li>
<li><code>(push :allow-low-level-mmap-errors *features*)</code>

<ul>
<li>Certain mmap related operations such as writing beyond file extent
  signals implementation-dependent errors.  Rather than tracking the
  multitude of specific errors (see <code>man mmap(2)</code>), they are coalesced
  into a package-specific condition.</li>
<li>This compilation flag disables the feature and may be useful while
  developing for this package or building upon it.</li>
<li>Be sure to visit code where this flag is used, and confirm granularity
  of the behaviour.</li>
</ul></li>
<li><code>(push :handle-only-known-mmap-errors *features*)</code>

<ul>
<li>Similar to <code>allow-low-level-mmap-errors</code> above, setting this one
  disables the final match of generic <code>error</code> in HANDLER-CASE that gets
  wrapped around various POSIX system-calls.</li>
<li>This compilation flag may also be useful while developing for this
  package or building upon it.</li>
</ul></li>
</ul>

<p>When updating tests:</p>

<ul>
<li><code>(setf prove:*debug-on-error* t)</code>

<ul>
<li>Use this within your development environment, so errors in test case
  code or package code may signal an error without it being masked by
  the test framework.</li>
<li>If using a test library or framework other than Prove, this variable
  of course would have no effect.</li>
</ul></li>
</ul>

</article>
<article class='markdown-article' id='file-footer-md'><h2>Links</h2>

<p>Clone <a href="https://gitlab.com/dpezely/cl-mmap" >repo</a>, which provides:</p>

<ul>
<li>Higher level <a href="https://gitlab.com/dpezely/cl-mmap/cl-mmap" >cl-mmap</a> package</li>
<li>Lower level <a href="https://gitlab.com/dpezely/cl-mmap/mmap" >mmap</a> package</li>
</ul>

</article>
<article class='markdown-article' id='file-generated-timestamps-md'><!-- GENERATED: see Makefile -->

<h2>Colophon</h2>

<ul>
<li>Documentation Generated: 2020-01-02</li>
<li>Lisp Compiler: SBCL 2.0.0</li>
<li>Quicklisp Distribution: 2019-12-27</li>
</ul>

</article>
  </body>
</html>