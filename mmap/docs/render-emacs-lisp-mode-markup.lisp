;;;; render-emacs-lisp-mode-markup.lisp

;; FIXME: should become a proper parser for 3bmd & cl-gendoc

(defun subst-emacs-lisp-mode-paragraph-breaks (doc-string)
  "Matches pattern of #\.#\Newline and adds an additional Newline
  so that compact Emacs lisp-mode CL doc-strings may be friendlier
  for Markdown"
  (let ((cursor 0)
	(new-doc '()))
    (do ((eoln (position #\Newline doc-string)
	       (position #\Newline doc-string :start (1+ eoln))))
	((null eoln))
      (let ((prev (when (> eoln 0)
		    (elt doc-string (1- eoln)))))
	(when (find prev '(#\. #\! #\?))
	  (push (format nil "~A~%" (subseq doc-string cursor eoln)) new-doc)
	  (setf cursor eoln))))
    (cond
      (new-doc
       (push (subseq doc-string cursor) new-doc)
       (let ((new-doc-string (apply #'concatenate
				    (cons 'string (reverse new-doc)))))
	 #+verbose
	 (format t "~&~%Add Newlines to doc-string:~% OLD=~%  ~S~% NEW=~%  ~S~%"
		 doc-string new-doc-string)
	 new-doc-string))
      (t
	doc-string))))

;; You might also be interested in new Emacs 25.1 variable,
;; ‘text-quoting-style’

(defun subst-emacs-lisp-mode-markup (package doc-string)
  "Convert from Emacs lisp-mode markup use of `foo' or ‘foo’ to HTML HREF
  when symbol-name can be found in same package; otherwise, convert to
  Markdown so that CL-GENDOC can render it consistently.
  (Use of UTF-8 quotes added in Emacs 25.1; try C-x 8 [ and C-x 8 ].)
  Note: CL-GENDOC uses HTML `id=apiref-foo' for function named `foo'."
  (let ((doc (subst-emacs-lisp-mode-paragraph-breaks doc-string))
	(cursor 0)
	(new-doc '()))
    (do ((open (position-if #'(lambda (x) (find x '(#\‘ #\`))) doc)
	       (position-if #'(lambda (x) (find x '(#\‘ #\`))) doc :start cursor))
	 (limit (length doc-string)))
	((null open))
      (let* ((i (or (position-if #'(lambda (x)
				     (find x '(#\’ #\' #\Space)))
				 doc :start open)
		    limit)))
	(when (find (elt doc i) '(#\’ #\'))
	  (let ((symbol-name (subseq doc (1+ open) i)))
	    (push (subseq doc cursor open) new-doc)
	    (multiple-value-bind (symbol status)
		(find-symbol (string-upcase symbol-name) package)
	      (if (and symbol (or (eq status :external)
				  (macro-function symbol)))
		  (if (eq (elt symbol-name 0) #\*)
		      (push (concatenate 'string ; Avoid *var* getting italics
					 "<a href=\"#apiref-"
					 (string-downcase symbol-name) "\">`"
					 symbol-name "`</a>")
			    new-doc)
		      (push (concatenate 'string
					 "<a href=\"#apiref-"
					 (string-downcase symbol-name) "\">"
					 symbol-name "</a>")
			    new-doc))
		  (push (concatenate 'string "`" symbol-name "`") new-doc)))))
	(setf cursor (1+ i))))
    (cond
      (new-doc
       (push (subseq doc cursor) new-doc)
       (let ((new-doc-string (apply #'concatenate
				    (cons 'string (nreverse new-doc)))))
	 #+verbose
	 (format t "~&~%Substituting doc-string:~% OLD=~%  ~S~% NEW=~%  ~S~%"
		 doc-string new-doc-string)
	 new-doc-string))
      (t
       doc))))
