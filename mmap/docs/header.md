MMAP
====

*Common Lisp library for operating on memory-mapped files*

See also: CL-MMAP package which offers a higher level API for using an MMAP
similarly to operating upon other types of files within Common Lisp.

## Overview

On a POSIX-compliant OS including BSD Unix, Linux and macOS operate
upon memory-mapped files with behaviour similar to Common Lisp functions
such as OPEN and READ.

There are two inter-related Common Lisp packages here: CL-MMAP and MMAP,
offering a high-level and low-level API, respectively.

The higher level API offers functions with names and options familiar to
Common Lisp programmers.  For instance, apply knowledge of OPEN from CL
package to the function of same name in this one.  Several CL symbols are
shadowed.

Use the lower level routines for constructing higher level APIs such as
building your own database where more fine-grained control is desired.
Nothing gets shadowed by this package, as I/O functions have names beginning
with `get-` and `put-` prefix instead, as for hash-tables.

File operations supported include opening with DIRECTION of `:input`,
`:output` or `:io`.  Manipulate single byte or multi-byte values including
as vector elements within the mapped file.

Big endian and little endian operations are supported.

Modifications may be persisted at granularity of an entire file or
just one region.  Both modes can optionally notify other concurrent
mappings of same file to be refreshed.

See README for example code, list of dependencies, known limitations, etc.
