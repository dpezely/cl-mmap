<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<title>README.html<test></title>
<link href="../docs/style.css" type="text/css" rel="stylesheet">
</head>

<body>

<h1>Test &amp; Measure</h1>

<p>Results from comparative testing yielded measurements that may surprise
people unfamiliar with broader consequences of using POSIX mmap().</p>

<p>Such surprises prompted writing these notes on &ldquo;performance&rdquo;, because more
than a few people misunderstood mmap() and were misusing it.</p>

<p>An explanation of test procedures is followed by description of test
functions.  Automation for these are available via <code>make measure</code> from the
parent directory/folder.</p>

<p>(See also: automated unit and function testing, via <code>make test</code>.)</p>

<h2>Running</h2>

<p>Everything is scripted, so you may repeat these measurements on other
hardware and OS configurations easily.</p>

<p>Important: <strong>run these scripts from the parent directory/folder, ..</strong></p>

<p>That ensures you are running from the local development version (rather than
system-wide instance, likely installed by Quicklisp).</p>

<p>Command for quick tests: (1 iteration of each)</p>

<pre><code>make measure
</code></pre>

<p>Command for full tests: (each 3x sequentially)</p>

<pre><code>make full-measure
</code></pre>

<p>Results are reported within a JSON file saved to <code>/tmp/measure-$$.json</code>
where <code>$$</code> is process id (PID) of shell script invoked from Makefile.</p>

<p>Scripts assume SBCL for testing and measuring.  Pull-requests welcomed!</p>

<h2>Approach</h2>

<p>Per conventional load &amp; capacity test guidelines (sometimes imprecisely
called &ldquo;stress testing&rdquo;), run each multiple times.  Only accept measurements
when three consecutive runs have low variance.</p>

<p>When differences are more than a small fraction of standard deviation,
consider a host running fewer background processes.</p>

<p>For sufficiently large files (compared to your OS file cache), you may skip
a preliminary dry-run to &ldquo;warm the cache&rdquo; as it won&rsquo;t fit the entire file
anyway.</p>

<p>For real-world value, however, runs should include those which launched from
a cold start versus long-running Lisp session after some number of periodic
gc sweeps.</p>

<p>General variations:</p>

<ol>
<li>Baseline: read one byte at a time using standard READ-BYTE</li>
<li>Read one byte at a time using cl-mmap: with-open-mmap + read-byte</li>
<li>Read one byte at a time using mmap: with-mmap + get-byte</li>
<li>Read native uint64 using mmap: with-mmap + get-native</li>
</ol>


<p>Suites of tests:</p>

<ol type="a">
<li>Random access, after initial load</li>
<li>Linear scan, known to be suboptimal for mmap technique</li>
</ol>


<p>Always repeat experiements, and attempt to disprove earlier results.</p>

<h2>Pre- &amp; Post-Conditions</h2>

<p>Beware of false positives.</p>

<p>If running the &ldquo;conventional&rdquo; approach (open/read/write) with a file small
enough to fit within your OS file cache, the subsequent mmap version may see
a false performance advantage due to that cache.</p>

<p>An OS cache on modern hardware may easily fit several hundred megabytes and
go unnoticed in a trivial test.</p>

<p>Conversely, beware of false negatives.</p>

<p>Ensure that your test system has <strong>not</strong> been swapping.  Before and after
each run, check using <code>vmstat</code> or <code>free</code> commands.</p>

<pre><code>vmstat -s | grep swapped
        0 pages swapped in
        0 pages swapped out
</code></pre>

<p>If it has been swapping, those tests have included an additional variable
within experimental conditions than necessary.  Try again without it.</p>

<p>Other tunables to ensure a stable starting state but beyond scope here:</p>

<ul>
<li>Maybe finish/flush file cache, see: <code>sync</code></li>
<li>Disable swap using <code>swapoff</code>, with caveats when a page has already been
written to swap</li>
<li>On Linux:

<ul>
<li>Adjust tendency for swapping: <code>sysctl</code> and <code>vm.swappiness</code></li>
<li>Adjust file cache behaviour: <code>sysctl</code> and <code>vm.vfs_cache_pressure</code></li>
<li>Duration of file cache: <code>sysctl</code> and <code>vm.drop_caches</code></li>
<li>Control resources on per-app basis via <code>cgroups</code></li>
</ul>
</li>
</ul>


<h2>Data File</h2>

<p>A download of 5+ million English language articles from
<a href="https://dumps.wikimedia.org/enwiki/20180420/">Wikipedia dumps</a>
&ldquo;Recombine articles, templates, media/file descriptions, and primary meta-pages&rdquo;
gives a bz2 compressed file of 14 GiB.</p>

<h2>Hardware</h2>

<p>The machine is an old Intel NUC, essentially laptop-grade hardware in a
small form factor chassis:</p>

<ul>
<li>Running Ubuntu 18.04 LTS server x86_64</li>
<li>File system is ext4 (<em>without</em> volume manager)</li>
<li>4 CPU cores: Intel&reg; Core&trade; i5-4250U CPU @ 1.30GHz (Haswell)</li>
<li>Total RAM: 16 GiB</li>
<li>Storage: Samsung SSD 840 EVO 120GiB mSATA, EXT41B6Q, max UDMA/133</li>
</ul>


<p>FIXME: that this machine has enough RAM to contain the test file, so we
should either intentionally constrain Linux OS file cache, physically
remove some RAM, or perhaps use a much larger file.</p>

<h2>Continuous Reading of Random Tiles</h2>

<p>Mimicking a tiling file structure&ndash; one which has a header tile followed by
additional non-overlapping tiles as app-layer logical blocks of 4 MiB&ndash;
tests processed the Wikipedia compressed dump.</p>

<p>Randomness was simulated by selecting the n-th tile based upon a reversed
sorted (high to low) sequence
of <a href="https://en.wikipedia.org/wiki/List_of_prime_numbers">prime numbers</a>
where each indicated a tile number (not file offset), which should
sufficiently defeat the OS file cache.</p>

<p>First, mmap() loses here!</p>

<p>Its performance is worse than using standard library functions such as
READ-BYTE within the CL package.</p>

<p>Values were consistent over multiple non-consecutive runs.  That is, from
cold start of hardware and iterating for number given below, these numbers
remained consistent across different invocations.</p>

<p>MMAP performance on Linux:</p>

<pre><code>| RANDOM ACCESS     | Count |   Context | Blocked |    Real |    User |     Sys | Bytes     |
|                   |       | Switching |  on I/O | Seconds | Seconds | Seconds | CONS'd    |
|-------------------+-------+-----------+---------+---------+---------+---------+-----------|
| CL:READ-BYTE      |     1 |        57 |       1 |      48 |     100 |    0.44 |   256,448 |
| cl-mmap:read-byte |     1 |       126 |       1 |     100 |     100 |    0.15 | 1,565,824 |
| mmap:get-byte     |     1 |        90 |       1 |      74 |      74 |    0.09 | 1,565,712 |
|                   |       |           |         |         |         |         |           |
| CL:READ-BYTE      |    10 |       193 |       1 |     467 |     464 |    3.71 | 2,497,568 |
| cl-mmap:read-byte |    10 |      1254 |       1 |     981 |     980 |    0.98 | 3,806,480 |
| mmap:get-byte     |    10 |       935 |       1 |     747 |     747 |    0.09 | 3,805,328 |
</code></pre>

<p>&ldquo;Count&rdquo; is the number of iterations of the loop internal to the test app
(not shell script).</p>

<p>&ldquo;Real&rdquo; and &ldquo;User&rdquo; seconds are rounded, because order of magnitude is most
relevant there.  &ldquo;Sys&rdquo; seconds are reported as-is.  Those numbers come from
<code>time</code> program (not Bash built-in), and final column is from <code>ROOM</code> in
Common Lisp.</p>

<p>Repeating the test but adding POSIX versions of <code>madvise(2)</code> and
<code>fadvise(2)</code> for suggesting random access of both the file and memory-map
offered negligible differences on Linux.</p>

<p>Linux kernel is known to ignore some &ldquo;but not all&rdquo; suggestions from these.</p>

<p>Because the test routines were reading 4 megabytes at a time (far more than
page size) before jumping elsewhere in the file, &ldquo;advise&rdquo; for random access
should translate to <em>worse</em> performance for this use case.</p>

<h2>Sequential Reading Of Bytes</h2>

<p>This use case is <em>known</em> to be <strong>suboptimal for mmap()</strong> based techniques,
as most of the OS overhead associated with it are up-front costs.</p>

<p>The Wikipedia compressed dump was opened, reading one byte at a time for the
entire 14 gigabytes.</p>

<p>This table merely illustrates the fact, offered as counter-point for those
falsely believing that mmap() might be a magic bullet.</p>

<p>MMAP performance on Linux:</p>

<pre><code>| SEQUENTIAL        | Count |   Context | Blocked |    Real |    User |     Sys |
|                   |       | Switching |  on I/O | Seconds | Seconds | Seconds |
|-------------------+-------+-----------+---------+---------+---------+---------|
| CL:READ-BYTE      |     1 |       322 |       1 |     426 |     423 |    3.00 |
| cl-mmap:read-byte |     1 |       992 |       1 |     767 |     766 |    1.16 |
| mmap:get-byte     |     1 |    114376 |      11 |     602 |     598 |    3.88 |
</code></pre>

<p>Again, values are from <code>time</code> program (not Bash built-in).</p>

<h2>Conclusion</h2>

<p>Memory-mapped files (mmap) may seem to be just what you want, but ensure
it&rsquo;s what you actually need.</p>

<p>Complexity and overhead should be disincentive for many use cases as
illustrated by the tables above.</p>

<p>Begin with conventional read/write operations, and only when absolutely
necessary for some feature&ndash; <strong>not</strong> an anticipation of performance&ndash; should
you even think about using mmap.</p>

</body>
</html>
