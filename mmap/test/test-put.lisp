;;;; test-put.lisp

(in-package #:mmap/test)

(prove:plan nil)

(prove:diag "Put data to MMAP region")

;; Be quiet while setting initial conditions-- clean slate:
(remove-all-test-files :quiet)

(let ((length 8)
      (offset 0))
  (multiple-value-bind (mmap-addr fd)
      (mmap:mmap %test-write-filepath% 0 length
            :direction :io)
    (prove:is (mmap:put-byte mmap-addr offset #x01)
              #x01
              "Opened :io, put-byte: 1")
    (mmap:sync mmap-addr 0 length)
    (prove:is (mmap:get-byte mmap-addr offset)
              #x01
              "get-byte: zero")

    (incf offset)
    (prove:is (mmap:put-byte mmap-addr offset #x02)
              #x02
              "Opened :io, put-byte: 2")
    (prove:is (mmap:get-byte mmap-addr offset)
              #x02
              "get-byte: 2")

    (incf offset)
    (prove:is (mmap:put-big-endian mmap-addr offset #x03 1)
              #x03
              "Opened :io, put-byte: 3")
    (prove:is (mmap:get-big-endian mmap-addr offset 1)
              #x03
              "get-byte: 3")

    (incf offset)
    (prove:is (mmap:put-little-endian mmap-addr offset #x04 1)
              #x04
              "Opened :io, put-byte: 4")
    (prove:is (mmap:get-little-endian mmap-addr offset 1)
              #x04
              "get-byte: 4")

    (setf offset 0)
    (prove:is (mmap:get-byte-vector mmap-addr offset 4)
              #(1 2 3 4)
              "re-reading, get-byte-vector: 4 bytes"
              :test #'equalp)

    (setf offset 4)
    (prove:is (mmap:put-byte-vector mmap-addr offset #(5 6 7 8))
              #(5 6 7 8)
              "rewriting, put-byte-vector: 4 bytes"
              :test #'equalp)
    (prove:is (mmap:get-byte-vector mmap-addr offset 4)
              #(5 6 7 8)
              "get-byte-vector: 4 bytes"
              :test #'equalp)

    (setf offset 0)
    (prove:is (mmap:get-byte-vector mmap-addr 0 8)
              #(1 2 3 4 5 6 7 8)
              "re-reading, get-byte-vector: 8 bytes"
              :test #'equalp)

    (prove:ok (mmap:munmap mmap-addr fd length)
              "Successful close")
    (with-open-file (stream %test-write-filepath%
                            :element-type '(unsigned-byte 8))
      (prove:is (let ((buffer (make-array 4 :element-type '(unsigned-byte 8))))
                  (read-sequence buffer stream)
                  buffer)
                #(1 2 3 4)
                "Re-opened via OPEN, then READ-SEQUENCE"
                :test #'equalp))

    (mmap:with-mmap (mmap-addr %test-write-filepath% nil nil
                               :direction :input)
      (prove:is (mmap:get-byte-vector mmap-addr 0 8)
                #(1 2 3 4 5 6 7 8)
                "Re-opened read-only, get-byte-vector"
                :test #'equalp)

      ;; Running with `sbcl --lose-on-corruption`, these tests crash
      ;; before HANDLER-CASE within `ignore-mmap-errors' macro sees it:
      (progn
        #+(or allow-low-level-mmap-errors handle-only-known-mmap-errors)
        (prove:is-error (mmap:put-byte mmap-addr 0 #x99)
                        ;; May be implementation-specific error
                        ;; e.g., sb-sys:memory-fault-error
                        'error 
                        "Disallow writing to read-only mmap")
        #-(or allow-low-level-mmap-errors handle-only-known-mmap-errors)
        (prove:is (mmap:put-byte mmap-addr 0 #x99)
                  nil
                  "Disallow writing to read-only mmap")))))

(prove:ok (probe-file %test-write-filepath%)
	  "Test file already exists")
(prove:is (probe-file %test-renamed-filepath%)
	  nil
	  "Renamed file does NOT exist yet")

(let ((length 4))
  (mmap:with-mmap (mmap-addr %test-write-filepath% 0 length
                             :direction :output)
    (mmap:put-byte mmap-addr 0 #x00))

  (with-open-file (stream %test-write-filepath%
			  :element-type '(unsigned-byte 8))
    (prove:is (read-byte stream nil)
	      #x00
	      "Open :output IF-EXISTS unspecified, write one byte"
	      :test #'equalp)))

(prove:ok (probe-file %test-renamed-filepath%)
	  "Renamed file exists here")

(let ((length 1))
  (mmap:with-mmap (mmap-addr %test-write-filepath% 0 length
                             :direction :output)
    (mmap:put-byte mmap-addr 0 #xFF))

  (with-open-file (stream %test-write-filepath%
			  :element-type '(unsigned-byte 8))
    (prove:is (read-byte stream nil)
	      #xFF
	      "Open :ouput, Write one byte: #xFF")))

(prove:is-error (mmap:with-mmap (mmap-addr %test-write-filepath% 0 3
				     :direction :output)
		  (mmap:put-byte mmap-addr 0 #x100))
		type-error
		"Attempt writing more than one by as one byte: TYPE-ERROR")

(let ((length (length %alphabet%)))
  (mmap:with-mmap (mmap-addr %test-write-filepath% 0 length
                             :direction :io)
    (loop
       for char upfrom 65 to 90
       for i upfrom 0
       do (mmap:put-byte mmap-addr i char))
    (mmap:put-byte mmap-addr 26 (char-code #\Newline)))

  (with-open-file (stream %test-write-filepath%)
    (prove:is (let ((buffer (make-string length)))
		(read-sequence buffer stream)
		buffer)
	      %alphabet%
	      "Persisted alphabet via mmap"
	      :test #'equal)))

(let ((*print-base* 16)
      (offset 0)
      (data #(#x000102030405060708090a0b0c0d0e0f
	      #x101112131415161718191a1b1c1d1e1f
	      #x202122232425262728292a2b2c2d2e2f
	      #x303132333435363738393a3b3c3d3e3f)))
  (mmap:with-mmap (mmap-addr %test-write-filepath% 0 128
                             :direction :output
                             :if-exists :new-version)
    (mmap:put-big-endian mmap-addr offset (elt data 0) 16)
    (incf offset 16)
    (mmap:put-big-endian mmap-addr offset (elt data 1) 16)
    (incf offset 16)
    (mmap:put-big-endian mmap-addr offset (elt data 2) 16)
    (incf offset 16)
    (mmap:put-big-endian mmap-addr offset (elt data 3) 16)
    (incf offset 16)
    (mmap:put-little-endian mmap-addr offset (elt data 0) 16)
    (incf offset 16)
    (mmap:put-little-endian mmap-addr offset (elt data 1) 16)
    (incf offset 16)
    (mmap:put-little-endian mmap-addr offset (elt data 2) 16)
    (incf offset 16)
    (mmap:put-little-endian mmap-addr offset (elt data 3) 16))
  (with-open-file (stream %test-write-filepath%
			  :element-type 'unsigned-byte)
    (let ((buffer (make-array 16 :element-type '(unsigned-byte 8))))
      (read-sequence buffer stream)
      (prove:is buffer (elt data 0) "Big Endian, first 16 bytes"
		:test #'compare-bytes-to-value)
      (read-sequence buffer stream)
      (prove:is buffer (elt data 1) "Big Endian, second 16 bytes"
		:test #'compare-bytes-to-value)
      (read-sequence buffer stream)
      (prove:is buffer (elt data 2) "Big Endian, third 16 bytes"
		:test #'compare-bytes-to-value)
      (read-sequence buffer stream)
      (prove:is buffer (elt data 3) "Big Endian, fourth 16 bytes"
		:test #'compare-bytes-to-value)

      (read-sequence buffer stream)
      (prove:is (reverse buffer) (elt data 0) "Little Endian, first 16 bytes"
		:test #'compare-bytes-to-value)
      (read-sequence buffer stream)
      (prove:is (reverse buffer) (elt data 1) "Little Endian, second 16 bytes"
		:test #'compare-bytes-to-value)
      (read-sequence buffer stream)
      (prove:is (reverse buffer) (elt data 2) "Little Endian, third 16 bytes"
		:test #'compare-bytes-to-value)
      (read-sequence buffer stream)
      (prove:is (reverse buffer) (elt data 3) "Little Endian, fourth 16 bytes"
		:test #'compare-bytes-to-value))))

(let ((*print-base* 16)
      (data #(#x000102030405060708090a0b0c0d0e0f
	      #x101112131415161718191a1b1c1d1e1f
	      #x202122232425262728292a2b2c2d2e2f
	      #x303132333435363738393a3b3c3d3e3f)))
  (mmap:with-mmap (mmap-addr %test-write-filepath% 0 128
                             :direction :output
                             :if-exists :supersede)
    (mmap:put-big-endian-vector    mmap-addr  0 data 16)
    (mmap:put-little-endian-vector mmap-addr 64 data 16))
  (with-open-file (stream %test-write-filepath%
			  :element-type 'unsigned-byte)
    (let ((buffer (make-array 16 :element-type '(unsigned-byte 8))))
      (read-sequence buffer stream)
      (prove:is buffer (elt data 0) "Big Endian, first 16 bytes"
		:test #'compare-bytes-to-value)
      (read-sequence buffer stream)
      (prove:is buffer (elt data 1) "Big Endian, second 16 bytes"
		:test #'compare-bytes-to-value)
      (read-sequence buffer stream)
      (prove:is buffer (elt data 2) "Big Endian, third 16 bytes"
		:test #'compare-bytes-to-value)
      (read-sequence buffer stream)
      (prove:is buffer (elt data 3) "Big Endian, fourth 16 bytes"
		:test #'compare-bytes-to-value)

      (read-sequence buffer stream)
      (prove:is (reverse buffer) (elt data 0) "Little Endian, first 16 bytes"
		:test #'compare-bytes-to-value)
      (read-sequence buffer stream)
      (prove:is (reverse buffer) (elt data 1) "Little Endian, second 16 bytes"
		:test #'compare-bytes-to-value)
      (read-sequence buffer stream)
      (prove:is (reverse buffer) (elt data 2) "Little Endian, third 16 bytes"
		:test #'compare-bytes-to-value)
      (read-sequence buffer stream)
      (prove:is (reverse buffer) (elt data 3) "Little Endian, fourth 16 bytes"
		:test #'compare-bytes-to-value))))

(with-open-file (out %test-write-filepath%
		     :direction :output
		     :if-exists :supersede
		     :element-type '(unsigned-byte 8))
  (write-sequence (map 'vector #'char-code %header%) out))

(prove:subtest "Supersede"
  (let ((file-length 3)) ; some size other than %test-write-filepath%
    (prove:isnt (test-write-file-size)
		file-length
		"File exists (but not of our next target size)")
    (prove:is (mmap:with-mmap (mmap-addr %test-write-filepath% 0 file-length
                                         :direction :io
                                         :if-exists :supersede)
		(when mmap-addr
		  :ok))
	      :ok
	      "Open both-directions but file already exists, so supersede")
    (prove:is (test-write-file-size)
	      file-length
	      "File has been truncated")))


;; Running with `sbcl --lose-on-corruption`, these tests crash
;; before HANDLER-CASE within `ignore-mmap-errors' macro sees it:
;;#+(or)
(progn
  (multiple-value-bind (mmap-addr fd size) (mmap:mmap %test-write-filepath% 0 9
                                                      :direction :output
                                                      :if-exists :supersede)
    (prove:ok (mmap:munmap mmap-addr fd size)
              "closing mmap")
    #-(or allow-low-level-mmap-errors handle-only-known-mmap-errors)
    (prove:is (mmap:put-byte mmap-addr 0 #xFF)
              nil
              "Disallow write after file close")
    #+(or allow-low-level-mmap-errors handle-only-known-mmap-errors)
    (prove:is-error (mmap:put-byte mmap-addr 0 #xFF)
                    'error
                    "Attempted write after file close signals error"))

  #-allow-low-level-mmap-errors
  (prove:is-error (mmap:mmap %test-write-filepath%
                             (1- mmap:+posix-page-size+)
                             mmap:+posix-page-size+
                             :direction :input)
                  'simple-error           ;signaled by ASSERT
                  "Attempted open :input with OFFEST other than page size hits ASSERT")

  #-allow-low-level-mmap-errors
  (prove:is-error (mmap:mmap %test-write-filepath%                              
                             (1+ mmap:+posix-page-size+)
                             mmap:+posix-page-size+
                             :direction :input)
                  'simple-error           ;signaled by ASSERT
                  "Attempted open :input with OFFEST other than page size hits ASSERT")

  #-allow-low-level-mmap-errors
  (prove:is-error (mmap:mmap %test-write-filepath%
                             (1+ mmap:+posix-page-size+)
                             mmap:+posix-page-size+
                             :direction :output)
                  'simple-error           ;signaled by ASSERT
                  "Attempted open :output with OFFEST other than page size hits ASSERT"))

;; This next test passes intermittently due to OS behaviour.  For our
;; low-level functions to trigger an error condition seems to depend
;; upon whether the page has been place into memory with an empty page
;; following it or not.  If not, only then does this trigger EOF behaviour!
#+(or)
(mmap:with-mmap (mmap-addr %test-write-filepath% 0 mmap:+posix-page-size+
                           :direction :io
                           :if-exists :supersede)
  ;; Fill to maximum file-length:
  (mmap:put-little-endian mmap-addr 0 #x01020304 mmap:+posix-page-size+)
  ;; Try writing beyond extents:
  (mmap:put-big-endian mmap-addr mmap:+posix-page-size+ #x61626364 4)
  ;; Try reading it:
  (prove:is (mmap:get-big-endian mmap-addr mmap:+posix-page-size+ 4)
	    nil	;maybe, maybe not
	    "Low-level writing beyond page size loses data"))

(remove-all-test-files :quiet)

(unless (prove:finalize)
  #+swank
  (break "Test suite failures")
  #-swank
  ;; Non-interactive such as running via make or CI, so avoid stack trace
  (uiop/image:shell-boolean-exit nil))
