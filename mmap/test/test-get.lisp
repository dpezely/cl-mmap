;;;; test-get.lisp

(in-package #:mmap/test)

(prove:plan nil)

(prove:diag "Get data from MMAP region")

;; Be quiet while setting initial conditions-- clean slate:
(remove-all-test-files :quiet)

(create-read-file)

(let ((*print-base* 16)
      (offset 0))
  (multiple-value-bind (mmap-addr fd actual-size)
      (mmap:mmap %test-read-filepath% nil nil)
    (declare (ignore fd))

    (prove:is (mmap:get-char mmap-addr offset)
              #\T
              "First character")

    (prove:is (mmap:get-char mmap-addr (1+ offset))
              #\h
              "Second character")

    (prove:is (mmap:get-char mmap-addr (+ offset 2))
              #\i
              "Second character")

    ;; OFFSET hasn't been incremented yet, so we can read whole header:
    (prove:is (mmap:get-byte-vector mmap-addr offset (length %header%))
              (map 'vector #'char-code %header%)
              "File header as string of bytes"
              :test #'equalp)
    (incf offset (length %header%))

    (prove:is (mmap:get-big-endian-vector mmap-addr offset (length %alphabet%) 1)
              (map 'vector #'char-code %alphabet%)
              "Alphabet as byte vector of char-codes"
              :test #'equalp)
    (incf offset (length %alphabet%))

    (prove:is (mmap:get-little-endian-vector mmap-addr offset (length %hex-digits%) 1)
              (map 'vector #'char-code %hex-digits%)
              "Hex digits as vector of char-codes"
              :test #'equalp)
    (incf offset (length %hex-digits%))

    (prove:is (mmap:get-char mmap-addr offset)
              #\Newline
              "#\\Newline as character")
    (incf offset)

    (prove:is (mmap:get-char mmap-addr offset)
              #\Newline
              "#\\Newline as character")
    (incf offset)

    (prove:is (mmap:get-big-endian mmap-addr offset 2)
              #x0A0A
              "Two #\\Newline but as non-characters")
    (incf offset 2)

    (prove:is (mmap:get-little-endian mmap-addr offset 16)
              #x46454443424139383736353433323130
              "Little Endian, 16 bytes")
    (incf offset 16)
    
    (prove:is (mmap:get-big-endian mmap-addr offset 16)
              #x30313233343536373839414243444546
              "Big Endian, 16 bytes")
    (incf offset 16)

    (prove:is (mmap:get-big-endian mmap-addr offset 8)
              #x0001020304050607
              "Hex digits from #x00 through #x07 consecutively as single integer")
    (incf offset 8)
    
    (prove:is (mmap:get-big-endian mmap-addr offset 8)
              #x08090A0B0C0D0E0F
              "Hex digits from #x08 through #x0F consecutively as single integer")
    (incf offset 8)
    
    (prove:is (mmap:get-big-endian mmap-addr offset 8)
              #x1011121314151617
              "Hex digits from #x10 through #x17 consecutively as single integer")
    (incf offset 8)
    
    (prove:is (mmap:get-byte mmap-addr offset)
              #x18
              "Read byte")
    (incf offset)

    (prove:is (mmap:get-byte mmap-addr offset)
              #x19
              "Read byte")
    (incf offset)

    (prove:is-error (mmap:get-big-endian-vector mmap-addr 0 3 -1)
                    'type-error
                    "Negative byte size not allowed")

    (prove:is-error (mmap:get-big-endian-vector mmap-addr 0 3 0)
                    'simple-error
                    "zero byte size not allowed")

    (prove:is (mmap:get-big-endian-vector mmap-addr offset 3 2)
              #(#x1A1B #x1C1D #x1E1F)
              "Three element vector, 2 bytes each Big Endian"
              :test #'equalp)
    (incf offset (* 3 2))

    (prove:is (mmap:get-big-endian-vector mmap-addr offset 2 16)
              #(#x202122232425262728292A2B2C2D2E2F
                #x303132333435363738393A3B3C3D3E3F)
              "Two element vector, 16 bytes each Big Endian"
              :test #'equalp)
    (incf offset (* 2 16))

    (prove:is (mmap:get-little-endian-vector mmap-addr offset 2 16)
              #(#x4f4e4d4c4b4a49484746454443424140
                #x5f5e5d5c5b5a59585756555453525150)
              "Two element vector, 16 bytes each Little Endian"
              :test #'equalp)
    (incf offset (* 2 16))
    
    (prove:is (mmap:get-little-endian-vector mmap-addr offset 2 32)
              #(#x7f7e7d7c7b7a797877767574737271706f6e6d6c6b6a69686766656463626160
                #x9f9e9d9c9b9a999897969594939291908f8e8d8c8b8a89888786858483828180)
              "Two element vector, 32 bytes each Little Endian"
              :test #'equalp)
    (incf offset (* 2 32))

    (prove:is (mmap:get-little-endian-vector mmap-addr offset 1 80)
              #(#xEfEeEdEcEbEaE9E8E7E6E5E4E3E2E1E0DfDeDdDcDbDaD9D8D7D6D5D4D3D2D1D0CfCeCdCcCbCaC9C8C7C6C5C4C3C2C1C0BfBeBdBcBbBaB9B8B7B6B5B4B3B2B1B0AfAeAdAcAbAaA9A8A7A6A5A4A3A2A1A0)
              "One element vector, 80 byte value as Little Endian"
              :test #'equalp)
    (incf offset 80)

    (prove:is (mmap:get-big-endian-vector mmap-addr offset 16 1)
              #(#xF0 #xF1 #xF2 #xF3 #xF4 #xF5 #xF6 #xF7 #xF8 #xF9
                #xFa #xFb #xFc #xFd #xFe #xFf)
              "16 element vector, 1 byte values as unnecessary Big Endian"
              :test #'equalp)
    (incf offset 16)

    (prove:is (mmap:get-byte mmap-addr actual-size)
              0
              "read-byte at end of small file returns Null")

    (prove:is (mmap:get-char mmap-addr actual-size)
              #\Null
              "read-byte at EOF of small file returns Null")

    (prove:is (mmap:get-byte-vector mmap-addr actual-size 1)
              #(0)
              "read-byte-vector at EOF of small file returns Null elements"
              :test #'equalp)

    (prove:is (mmap:get-little-endian mmap-addr actual-size 1)
              0
              "read-little-endian at EOF of small file returns Null")

    (prove:is (mmap:get-big-endian mmap-addr actual-size 1)
              0
              "read-big-endian at EOF of small file returns Null")

    (prove:is (mmap:get-little-endian-vector mmap-addr actual-size 1 1)
              #(0)
              "read-little-endian-vector at EOF of small file returns Null elements"
              :test #'equalp)

    (prove:is (mmap:get-big-endian-vector mmap-addr actual-size 1 1)
              #(0)
              "read-big-endian-vector at EOF of small file returns Null elements"
              :test #'equalp)))

(remove-all-test-files :quiet)

(unless (prove:finalize)
  #+swank
  (break "Test suite failures")
  #-swank
  ;; Non-interactive such as running via make or CI, so avoid stack trace
  (uiop/image:shell-boolean-exit nil))
