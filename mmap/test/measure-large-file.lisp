;;;; measure-large-file.lisp - Load test & measurements for basic comparison

(in-package #:cl-user)

(require 'uiop)
(require 'cl-mmap)
(require 'mmap)

(defvar *filepath* #P"enwiki-20180420-pages-articles.xml.bz2"
        "Downloaded from https://dumps.wikimedia.org/enwiki/20180420/
  Recombine articles, templates, media/file descriptions, and primary meta-pages
  14.0 GB bz2 compressed")

(defvar *functions* '(continuous-random-via-cl-read-byte
                      continuous-random-via-cl-mmap-read-byte
                      continuous-random-via-mmap-get-byte
                      #+FIXME_future
                      continuous-random-via-mmap-get-native

                      linear-scan-via-cl-read-byte
                      linear-scan-via-cl-mmap-read-byte
                      linear-scan-via-mmap-get-byte
                      #+FIXME_future
                      linear-scan-via-mmap-get-native)
  "Evaluate functions named in this sequence")

(defconstant +tile-size-bytes+
  #.(* 4 1024 1024)
  "Size for a fictitious tiling-oriented file structure
   including header tile")

(defconstant +primes+
  #(2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83
    89 97 101 103 107 109 113 127 131 137 139 149 151 157 163 167 173 179
    181 191 193 197 199 211 223 227 229 233 239 241 251 257 263 269 271
    277 281 283 293 307 311 313 317 331 337 347 349 353 359 367 373 379
    383 389 397 401 409 419 421 431 433 439 443 449 457 461 463 467 479
    487 491 499 503 509 521 523 541 547 557 563 569 571 577 587 593 599
    601 607 613 617 619 631 641 643 647 653 659 661 673 677 683 691 701
    709 719 727 733 739 743 751 757 761 769 773 787 797 809 811 821 823
    827 829 839 853 857 859 863 877 881 883 887 907 911 919 929 937 941
    947 953 967 971 977 983 991 997 1009 1013 1019 1021 1031 1033 1039
    1049 1051 1061 1063 1069 1087 1091 1093 1097 1103 1109 1117 1123 1129
    1151 1153 1163 1171 1181 1187 1193 1201 1213 1217 1223 1229 1231 1237
    1249 1259 1277 1279 1283 1289 1291 1297 1301 1303 1307 1319 1321 1327
    1361 1367 1373 1381 1399 1409 1423 1427 1429 1433 1439 1447 1451 1453
    1459 1471 1481 1483 1487 1489 1493 1499 1511 1523 1531 1543 1549 1553
    1559 1567 1571 1579 1583 1597 1601 1607 1609 1613 1619 1621 1627 1637
    1657 1663 1667 1669 1693 1697 1699 1709 1721 1723 1733 1741 1747 1753
    1759 1777 1783 1787 1789 1801 1811 1823 1831 1847 1861 1867 1871 1873
    1877 1879 1889 1901 1907 1913 1931 1933 1949 1951 1973 1979 1987 1993
    1997 1999 2003 2011 2017 2027 2029 2039 2053 2063 2069 2081 2083 2087
    2089 2099 2111 2113 2129 2131 2137 2141 2143 2153 2161 2179 2203 2207
    2213 2221 2237 2239 2243 2251 2267 2269 2273 2281 2287 2293 2297 2309
    2311 2333 2339 2341 2347 2351 2357 2371 2377 2381 2383 2389 2393 2399
    2411 2417 2423 2437 2441 2447 2459 2467 2473 2477 2503 2521 2531 2539
    2543 2549 2551 2557 2579 2591 2593 2609 2617 2621 2633 2647 2657 2659
    2663 2671 2677 2683 2687 2689 2693 2699 2707 2711 2713 2719 2729 2731
    2741 2749 2753 2767 2777 2789 2791 2797 2801 2803 2819 2833 2837 2843
    2851 2857 2861 2879 2887 2897 2903 2909 2917 2927 2939 2953 2957 2963
    2969 2971 2999 3001 3011 3019 3023 3037 3041 3049 3061 3067 3079 3083
    3089 3109 3119 3121 3137 3163 3167 3169 3181 3187 3191 3203 3209 3217
    3221 3229 3251 3253 3257 3259 3271 3299 3301 3307 3313 3319 3323 3329
    3331 3343 3347 3359 3361 3371 3373 3389 3391 3407 3413)
  "Copied from https://en.wikipedia.org/wiki/List_of_prime_numbers")

(defconstant +tile-index-sequence+
  (make-array (length +primes+) :element-type 'fixnum
              :initial-contents (reverse +primes+))
  "Approximate non-linear access of large, tiling data file.
  These values represent a tile sequence number-- NOT file offset.
  Sequence need only be sufficient to thwart OS read-ahead cache.")

;; Prohibit compiler from optimizing away our trivial "work"
(declaim (notinline arbitrary-work))

;; Do just enough work that it can't be optimized away:
(let ((%byte-frequencies% (make-array 256 :element-type '(integer 0)
                                      :initial-element 0)))
  (defun get-frequencies ()
    %byte-frequencies%)

  (defun arbitrary-work (byte)
    "Count distribution of byte values observed"
    (when byte
      (incf (elt %byte-frequencies% byte)))))


(defun continuous-random-via-cl-read-byte ()
  "With random offset, read one byte at a time using CL:READ-BYTE.
  Returns: bytes count."
  (with-open-file (stream *filepath* :element-type '(unsigned-byte 8))
    (let* ((length-bytes (file-length stream))
           (length-tiles (/ length-bytes +tile-size-bytes+)))
      ;; Simulate reading header tile:
      (loop
         for i upfrom 0 below +tile-size-bytes+
         while (arbitrary-work (read-byte stream nil)))
      (remove-duplicates
       ;; Simulate reading within pseudo-random tile
       (loop
          for tile-id across +tile-index-sequence+
          when (< tile-id length-tiles)
          collect
            (progn
              (file-position stream (* tile-id +tile-size-bytes+))
              (loop
                 for i upfrom 0 below +tile-size-bytes+
                 unless (arbitrary-work (read-byte stream nil))
                 return i)))))))

(defun continuous-random-via-cl-mmap-read-byte ()
  "With random offset, read one byte at a time using cl-mmap:read-byte.
  Returns: bytes count."
  (cl-mmap:with-open-mmap (mmap *filepath*)
    (let* ((length-bytes (cl-mmap::mmapped-file-length-bytes mmap))
           (length-tiles (/ length-bytes +tile-size-bytes+)))
      ;; Simulate reading header tile:
      (loop
         for i upfrom 0 below +tile-size-bytes+
         while (arbitrary-work (cl-mmap:read-byte mmap nil)))
      #+enable-posix-madvise-for-worse-performance
      (mmap:random-access (cl-mmap::mmapped-file-addr mmap) 0
                          (cl-mmap::mmapped-file-length-bytes mmap)
                          (cl-mmap::mmapped-file-fd mmap))
      (remove-duplicates
       ;; Simulate reading within pseudo-random tile
       (loop
          for tile-id across +tile-index-sequence+
          when (< tile-id length-tiles)
          collect
            (progn
              (cl-mmap:seek mmap (* tile-id +tile-size-bytes+))
              (loop
                 for i upfrom 0 below +tile-size-bytes+
                 unless (arbitrary-work (cl-mmap:read-byte mmap nil))
                 return i)))))))

(defun continuous-random-via-mmap-get-byte ()
  "With random offset, read one byte at a time using mmap:get-byte.
  Returns: bytes count."
  (mmap:with-mmap (mmap *filepath* nil nil)
    (let* ((length-bytes mmap::%actual-size%)
           (length-tiles (/ length-bytes +tile-size-bytes+)))
      ;; Simulate reading header tile:
      (loop
         for i upfrom 0 below +tile-size-bytes+
         while (arbitrary-work (mmap:get-byte mmap i)))
      #+enable-posix-madvise-for-worse-performance
      (mmap:random-access mmap 0 mmap::%actual-size% mmap::%fd%)
      (remove-duplicates
       ;; Simulate reading within pseudo-random tile
       (loop
          for tile-id across +tile-index-sequence+
          for offset = (* tile-id +tile-size-bytes+)
          when (< tile-id length-tiles)
          collect
            (loop
               for i upfrom 0 below +tile-size-bytes+
               unless (arbitrary-work (mmap:get-byte mmap offset))
               return i))))))


(defun linear-scan-via-cl-read-byte ()
  "Read one byte at a time using CL:READ-BYTE.
  Returns: bytes count."
  (with-open-file (stream *filepath* :element-type '(unsigned-byte 8))
    (loop
       for i upfrom 0
       unless (arbitrary-work (read-byte stream nil))
       return i)))

(defun linear-scan-via-cl-mmap-read-byte ()
  "Read one byte at a time using cl-mmap:read-byte.
  Returns: bytes count."
    (cl-mmap:with-open-mmap (mmap *filepath*)
      (loop
         for i upfrom 0
         unless (arbitrary-work (cl-mmap:read-byte mmap nil))
         return i)))

;; In anything other than this type of arbitrary load test, we would
;; likely read something from file header that indicates total file
;; size possibly in units of +posix-page-size+ or "tiles".
;; Without that, this code generates a warning at runtime on SBCL:
;; "CORRUPTION WARNING in SBCL pid 1876(tid 0x7ffff7fecb80):
;; Memory fault at 0x7ffff66ee000 (pc=0x2281bae3, sp=0x7ffff6cf72c0)
;; The integrity of this image is possibly compromised.
;; Continuing with fingers crossed."
;; Therefore, avoid running SBCL with --lose-on-corruption
(defun linear-scan-via-mmap-get-byte ()
  "Read one byte at a time using mmap: with-mmap + get-byte.
  Returns: bytes count."
  (mmap:with-mmap (mmap-addr *filepath* nil nil)
    (loop
       for i upfrom 0
       unless (arbitrary-work (mmap:get-byte mmap-addr i))
       return i)))

#+FIXME_future
(defun linear-scan-via-mmap-get-native ()
  "Read native uint64 or uint32 using mmap: with-mmap + get.
  Returns: bytes count."
  (mmap:with-mmap (mmap-addr *filepath* nil nil)
    (loop
       for i upfrom 0
       unless (arbitrary-work (mmap:get-native mmap-addr i))
       return i)))


(defun run-you-clever-toy-and-remember-measurements (nth-function
                                                     &optional (max-runs 4))
  "Run just the single function indicated by NTH-FUNCTION
  and repeat up to MAX-RUNS count.
  Tracks basic data such as output of ROOM and elapsed run time."
  (check-type nth-function (integer 0) "non-negative integer")
  (check-type max-runs (integer 0) "non-negative integer")
  (when (< nth-function (length *functions*))
    (let ((fn (elt *functions* nth-function)))
      (loop
         for i below max-runs
         for start-time = (get-internal-run-time)
         do (format *error-output* "~&Running ~A [~D]...~%" fn (- max-runs i))
         collect (list fn
                       (get-universal-time)
                       (funcall fn)
                       (- (get-internal-run-time) start-time)
                       (with-output-to-string (*standard-output*) (room)))))))

(defun measure ()
  "When running from OS shell, call only this function.
  Results: output printed to `*standard-output*' as excerpts of JSON
  format so output of multiple runs may be concatenated."
  (let ((args uiop:*command-line-arguments*))
    (cond
      ((= (length args) 3)
       (setf *filepath* (pathname (first args)))
       (format t "\"room-baseline-~D\":~S,~%~
                 ~{~{\"~(~A~)-~2,'0D\":{~% ~
                 \"resulting-byte-count\":\"~A\",~% ~
                 \"internal-run-time\":~D,~% ~
                 \"room-after\":~S}~^,~%~}~}~%" ; Omit matched #\}
               (get-internal-real-time)
               (with-output-to-string (*standard-output*) (room))
               (run-you-clever-toy-and-remember-measurements
                (parse-integer (second args))
                (parse-integer (third args))))
       (uiop:quit))
      (t
        (uiop:die "~&Usage: command-line params are ~
                   <pathname> <nth-function> <max-runs>~%")))))
