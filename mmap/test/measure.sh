#! /bin/bash

# Load test & measurements for basic comparison

# Script needs to be run FROM SUBDIRECTORY CONTAINING *.ASD files of
# working copy, as this code avoids using Quicklisp for dev purposes.

# Downloaded 14 GB from https://dumps.wikimedia.org/enwiki/20180420/
DATA="/home/mirror/enwiki-20180420-pages-articles.xml.bz2"

LOG="/tmp/measure-$$.json"

SBCL="sbcl --disable-ldb --noinform --end-runtime-options --disable-debugger --quit"

ITERATIONS=3
LAST_FN=5

measure() {
    # Backslash prefix avoids Bash built-in `time` here:
    \time -a -o $LOG -f \
          'command:"%C"\ncontext-switching:%c,waited:%w,real-seconds:%e,user-seconds:%U,sys-seconds:%S,exit-status:%x' $*
}

quick() {
    start=${1:-0}
    end=${2:-$LAST_FN}
    iterations=1
    echo '{' > $LOG
    for i in $(seq $start $end); do
        measure \
            $SBCL --load speed \
            --load "test/measure-large-file" --eval '(measure)' \
            $DATA $i $iterations >> $LOG
    done
    echo '}' >> ${LOG}
}

iterations() {
    start=${1:-0}
    end=${2:-$LAST_FN}
    iterations=${3:-$ITERATIONS}
    echo '{' > $LOG
    for i in $(seq $start $end); do
        measure \
            $SBCL --load speed \
            --load "test/measure-large-file" --eval '(measure)' \
            $DATA $i $iterations >> $LOG
    done
    echo '}' >> $LOG
}

$*
ls -lh $LOG
echo file://$LOG
