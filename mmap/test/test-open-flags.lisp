;;;; test-open-flags.lisp

(in-package #:mmap/test)

(prove:plan nil)

(prove:diag "process-open-flags")

;; Be quiet while setting initial conditions-- clean slate:
(remove-all-test-files :quiet)

(prove:is (mmap::process-open-flags %filepath-does-not-exist%
                                        :io
                                        nil nil
                                        nil t)
	  nil
	  "Open for i/o with `if-does-not-exist nil' when file doesn't exist")

(prove:is-error (mmap::process-open-flags %filepath-does-not-exist%
                                              :io
                                              nil nil
                                              :error t)
		'file-error
		"Open for i/o, but error if file doesn't exist")

(prove:is (mmap::process-open-flags %filepath-does-not-exist%
                                        :input
                                        nil nil
                                        nil t)
	  nil
	  "Open for input with `if-does-not-exist nil' when file doesn't exist")

(prove:is (mmap::process-open-flags %filepath-does-not-exist%
                                        :output
                                        nil nil
                                        nil t)
	  nil
	  "Open for output with `if-does-not-exist NIL' when file doesn't exist")

(create-write-file)

(prove:is-error (mmap::process-open-flags %test-write-filepath%
                                              :io
                                              :error t
                                              nil nil)
		'file-error
		"Open for i/o with `if-exists :error' when file exists")

(prove:is-error (mmap::process-open-flags %test-write-filepath%
                                              :output
                                              :error t
                                              nil nil)
		'file-error
		"Open for output with `if-exists error' when file exists")

(prove:is (mmap::process-open-flags %test-write-filepath%
                                        :io
                                        nil t
                                        nil nil)
	  nil
	  "Open for i/o with `if-exists nil' when file exists")

(prove:is (mmap::process-open-flags %test-write-filepath%
                                        :output
                                        nil t
                                        nil nil)
	  nil
	  "Open for output with `if-exists nil' when file exists")

(prove:ok (probe-file %test-write-filepath%)
	  "Test file exists")

(prove:is (probe-file %test-renamed-filepath%)
	  nil
	  "Renamed file does not exist")

(prove:is-values (mmap::process-open-flags %test-write-filepath%
                                               :io
                                               :rename t
                                               nil nil)
		 (list (logior osicat-posix:o-rdwr osicat-posix:o-creat)
		       (logior osicat-posix:prot-read osicat-posix:prot-write))
		 "Open for i/o with rename when file exists")

(prove:is (probe-file %test-write-filepath%)
	  nil
	  "Original filename no longer exists")

(prove:ok (probe-file %test-renamed-filepath%)
	  "Found renamed file")

(create-write-file)

(prove:is-values (mmap::process-open-flags %test-write-filepath%
                                               :io
                                               :rename-and-delete t
                                               nil nil)
		 (list (logior osicat-posix:o-rdwr osicat-posix:o-creat)
		       (logior osicat-posix:prot-read osicat-posix:prot-write))
		 "Open for i/o with rename and delete when file exists")

(prove:is (probe-file %test-write-filepath%)
	  nil
	  "Original file has been renamed")

(prove:is (probe-file %test-renamed-filepath%)
	  nil
	  "Renamed file has been deleted")

(create-write-file)

(prove:is-values (mmap::process-open-flags %test-write-filepath%
                                               :io
                                               :append t
                                               nil nil)
		 (list (logior osicat-posix:o-rdwr osicat-posix:o-append)
		       (logior osicat-posix:prot-read osicat-posix:prot-write))
		 "Open for i/o with append when file exists")

(prove:is-values (mmap::process-open-flags %test-write-filepath%
                                               :io
                                               :supersede t
                                               nil nil)
		 (list (logior osicat-posix:o-rdwr osicat-posix:o-trunc)
		       (logior osicat-posix:prot-read osicat-posix:prot-write))
		 "Open for i/o with supersede when file exists")

(remove-all-test-files)

(prove:is-values (mmap::process-open-flags %test-write-filepath%
                                               :io
                                               :append t
                                               nil nil)
		 (list (logior osicat-posix:o-rdwr osicat-posix:o-creat)
		       (logior osicat-posix:prot-read osicat-posix:prot-write))
		 "Open for i/o with append when file doesn't exist")

(prove:is-values (mmap::process-open-flags %test-write-filepath%
                                               :io
                                               :supersede t
                                               nil nil)
		 (list (logior osicat-posix:o-rdwr osicat-posix:o-creat)
		       (logior osicat-posix:prot-read osicat-posix:prot-write))
		 "Open for i/o with supersede when file doesn't exist")


;; FIXME: when adding further tests, be sure to clean-up afterwards:
(remove-all-test-files)


(unless (prove:finalize)
  #+swank
  (break "Test suite failures")
  #-swank
  ;; Non-interactive such as running via make or CI, so avoid stack trace
  (uiop/image:shell-boolean-exit nil))
