;;;; test-mapping-type.lisp

(in-package #:mmap/test)

(prove:plan nil)

(prove:diag "set-mapping-flag")

;; These simple tests exist as canary-in-the-coal-mine since any
;; change to this behaviour impacts API contract of top-level code.

(prove:is (mmap::set-mapping-flag :input)
	  osicat-posix:map-private
	  "Direction input (read-only) defaults to private")

(prove:is (mmap::set-mapping-flag :output)
	  osicat-posix:map-shared
	  "Direction output (write-only) defaults to shared")

(prove:is (mmap::set-mapping-flag :io)
	  osicat-posix:map-shared
	  "Direction io (includes write) defaults to shared")

(unless (prove:finalize)
  #+swank
  (break "Test suite failures")
  #-swank
  ;; Non-interactive such as running via make or CI, so avoid stack trace
  (uiop/image:shell-boolean-exit nil))
