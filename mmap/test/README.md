Test & Measure
==============

Results from comparative testing yielded measurements that may surprise
people unfamiliar with broader consequences of using POSIX mmap().

Such surprises prompted writing these notes on "performance", because more
than a few people misunderstood mmap() and were misusing it.

An explanation of test procedures is followed by description of test
functions.  Automation for these are available via `make measure` from the
parent directory/folder.

(See also: automated unit and function testing, via `make test`.)

## Running

Everything is scripted, so you may repeat these measurements on other
hardware and OS configurations easily.

Important: **run these scripts from the parent directory/folder, ..**

That ensures you are running from the local development version (rather than
system-wide instance, likely installed by Quicklisp).

Command for quick tests: (1 iteration of each)

	make measure

Command for full tests: (each 3x sequentially)

	make full-measure
    
Results are reported within a JSON file saved to `/tmp/measure-$$.json`
where `$$` is process id (PID) of shell script invoked from Makefile.

Scripts assume SBCL for testing and measuring.  Pull-requests welcomed!

## Approach

Per conventional load & capacity test guidelines (sometimes imprecisely
called "stress testing"), run each multiple times.  Only accept measurements
when three consecutive runs have low variance.

When differences are more than a small fraction of standard deviation,
consider a host running fewer background processes.

For sufficiently large files (compared to your OS file cache), you may skip
a preliminary dry-run to "warm the cache" as it won't fit the entire file
anyway.

For real-world value, however, runs should include those which launched from
a cold start versus long-running Lisp session after some number of periodic
gc sweeps.

General variations:

1. Baseline: read one byte at a time using standard READ-BYTE
2. Read one byte at a time using cl-mmap: with-open-mmap + read-byte
3. Read one byte at a time using mmap: with-mmap + get-byte
4. Read native uint64 using mmap: with-mmap + get-native

Suites of tests:

a. Random access, after initial load
b. Linear scan, known to be suboptimal for mmap technique

Always repeat experiements, and attempt to disprove earlier results.

## Pre- & Post-Conditions

Beware of false positives.

If running the "conventional" approach (open/read/write) with a file small
enough to fit within your OS file cache, the subsequent mmap version may see
a false performance advantage due to that cache.

An OS cache on modern hardware may easily fit several hundred megabytes and
go unnoticed in a trivial test.

Conversely, beware of false negatives.

Ensure that your test system has **not** been swapping.  Before and after
each run, check using `vmstat` or `free` commands.

    vmstat -s | grep swapped
            0 pages swapped in
            0 pages swapped out

If it has been swapping, those tests have included an additional variable
within experimental conditions than necessary.  Try again without it.

Other tunables to ensure a stable starting state but beyond scope here:

- Maybe finish/flush file cache, see: `sync`
- Disable swap using `swapoff`, with caveats when a page has already been
  written to swap
- On Linux:
  + Adjust tendency for swapping: `sysctl` and `vm.swappiness`
  + Adjust file cache behaviour: `sysctl` and `vm.vfs_cache_pressure`
  + Duration of file cache: `sysctl` and `vm.drop_caches`
  + Control resources on per-app basis via `cgroups`

## Data File

A download of 5+ million English language articles from
[Wikipedia dumps](https://dumps.wikimedia.org/enwiki/20180420/)
"Recombine articles, templates, media/file descriptions, and primary meta-pages"
gives a bz2 compressed file of 14 GiB.

## Hardware

The machine is an old Intel NUC, essentially laptop-grade hardware in a
small form factor chassis:

- Running Ubuntu 18.04 LTS server x86_64
- File system is ext4 (*without* volume manager)
- 4 CPU cores: Intel(R) Core(TM) i5-4250U CPU @ 1.30GHz (Haswell)
- Total RAM: 16 GiB
- Storage: Samsung SSD 840 EVO 120GiB mSATA, EXT41B6Q, max UDMA/133

FIXME: that this machine has enough RAM to contain the test file, so we
should either intentionally constrain Linux OS file cache, physically
remove some RAM, or perhaps use a much larger file.

## Continuous Reading of Random Tiles

Mimicking a tiling file structure-- one which has a header tile followed by
additional non-overlapping tiles as app-layer logical blocks of 4 MiB--
tests processed the Wikipedia compressed dump.

Randomness was simulated by selecting the n-th tile based upon a reversed
sorted (high to low) sequence
of [prime numbers](https://en.wikipedia.org/wiki/List_of_prime_numbers)
where each indicated a tile number (not file offset), which should
sufficiently defeat the OS file cache.

First, mmap() loses here!

Its performance is worse than using standard library functions such as
READ-BYTE within the CL package.

Values were consistent over multiple non-consecutive runs.  That is, from
cold start of hardware and iterating for number given below, these numbers
remained consistent across different invocations.

MMAP performance on Linux:

    | RANDOM ACCESS     | Count |   Context | Blocked |    Real |    User |     Sys | Bytes     |
    |                   |       | Switching |  on I/O | Seconds | Seconds | Seconds | CONS'd    |
    |-------------------+-------+-----------+---------+---------+---------+---------+-----------|
    | CL:READ-BYTE      |     1 |        57 |       1 |      48 |     100 |    0.44 |   256,448 |
    | cl-mmap:read-byte |     1 |       126 |       1 |     100 |     100 |    0.15 | 1,565,824 |
    | mmap:get-byte     |     1 |        90 |       1 |      74 |      74 |    0.09 | 1,565,712 |
    |                   |       |           |         |         |         |         |           |
    | CL:READ-BYTE      |    10 |       193 |       1 |     467 |     464 |    3.71 | 2,497,568 |
    | cl-mmap:read-byte |    10 |      1254 |       1 |     981 |     980 |    0.98 | 3,806,480 |
    | mmap:get-byte     |    10 |       935 |       1 |     747 |     747 |    0.09 | 3,805,328 |

"Count" is the number of iterations of the loop internal to the test app
(not shell script).

"Real" and "User" seconds are rounded, because order of magnitude is most
relevant there.  "Sys" seconds are reported as-is.  Those numbers come from
`time` program (not Bash built-in), and final column is from `ROOM` in
Common Lisp.

Repeating the test but adding POSIX versions of `madvise(2)` and
`fadvise(2)` for suggesting random access of both the file and memory-map
offered negligible differences on Linux.

Linux kernel is known to ignore some "but not all" suggestions from these.

Because the test routines were reading 4 megabytes at a time (far more than
page size) before jumping elsewhere in the file, "advise" for random access
should translate to *worse* performance for this use case.

## Sequential Reading Of Bytes

This use case is *known* to be **suboptimal for mmap()** based techniques,
as most of the OS overhead associated with it are up-front costs.

The Wikipedia compressed dump was opened, reading one byte at a time for the
entire 14 gigabytes.

This table merely illustrates the fact, offered as counter-point for those
falsely believing that mmap() might be a magic bullet.

MMAP performance on Linux:

    | SEQUENTIAL        | Count |   Context | Blocked |    Real |    User |     Sys |
    |                   |       | Switching |  on I/O | Seconds | Seconds | Seconds |
    |-------------------+-------+-----------+---------+---------+---------+---------|
    | CL:READ-BYTE      |     1 |       322 |       1 |     426 |     423 |    3.00 |
    | cl-mmap:read-byte |     1 |       992 |       1 |     767 |     766 |    1.16 |
    | mmap:get-byte     |     1 |    114376 |      11 |     602 |     598 |    3.88 |

Again, values are from `time` program (not Bash built-in).

## Conclusion

Memory-mapped files (mmap) may seem to be just what you want, but ensure
it's what you actually need.

Complexity and overhead should be disincentive for many use cases as
illustrated by the tables above.

Begin with conventional read/write operations, and only when absolutely
necessary for some feature-- **not** an anticipation of performance-- should
you even think about using mmap.
