;;;; test-sync.lisp

(in-package #:mmap/test)

(prove:plan nil)

(prove:diag "Sync MMAP data to file")

;; Be quiet while setting initial conditions-- clean slate:
(remove-all-test-files :quiet)

(let* ((not-page-size (floor mmap:+posix-page-size+ 3)))
  (mmap:with-mmap (mmap-addr %test-write-filepath% 0 not-page-size
                             :direction :io
                             :if-exists :supersede)
    (mmap:put-byte-vector mmap-addr 0 #(01 02 03 04))
    (mmap:put-byte-vector mmap-addr 4 #(05 06 07 08))
    #-allow-low-level-mmap-errors
    (prove:is-error (mmap:sync mmap-addr not-page-size not-page-size)
		    'mmap:posix-error
		    "sync requires multiples of page-size")
    #+allow-low-level-mmap-errors
    (prove:is-error (mmap:sync mmap-addr not-page-size not-page-size)
		    'error
		    "sync requires multiples of page-size")
    #-allow-low-level-mmap-errors
    (prove:is-error (mmap:sync mmap-addr
                               (* not-page-size 10)
                               (* not-page-size 11))
		    'mmap:posix-error
		    "sync requires region be within mapped region")
    #+allow-low-level-mmap-errors
    (prove:is-error (mmap:sync mmap-addr
                               (* not-page-size 10)
                               (* not-page-size 11))
		    'error
		    "sync requires region be within mapped region")
    (prove:ok (mmap:sync mmap-addr 0 not-page-size)
              "sync entire mapped file even when smaller than page-size")))

(prove:subtest "sync persists only subset of file"
  (let* ((file-size (* 4 mmap:+posix-page-size+))
	 (page-size mmap:+posix-page-size+)
         (offset 0))
    (mmap:with-mmap (mmap-addr %test-write-filepath% 0 file-size
                               :direction :io
                               :if-exists :supersede)
      (mmap:put-big-endian mmap-addr offset #x01020304 page-size)
      (incf offset page-size)
      (mmap:put-big-endian mmap-addr offset #x05060708 page-size)
      (incf offset page-size)
      (mmap:put-big-endian mmap-addr offset #x090A0B0C page-size)
      (incf offset page-size)
      (mmap:put-big-endian mmap-addr offset #x0D0E0F10 page-size)
      (incf offset page-size)

      (mmap:sync mmap-addr page-size page-size)

      (with-open-file (stream %test-write-filepath%
			      :element-type 'unsigned-byte)
	(let ((buffer (make-array page-size
				  :element-type '(unsigned-byte 8))))
	  ;; Because the OS may have performed an intermittent save,
	  ;; only test region specified to be saved:
	  (read-sequence buffer stream)	;intentionally not tested
	  (read-sequence buffer stream)
	  (prove:is (count 0 buffer)
		    (- page-size 4)
		    "Second quarter of file is mostly null")
	  (prove:is (subseq buffer (- page-size 4))
		    #(#x05 #x06 #x07 #x08)
		    "Last 4 bytes of second quarter of file has been persisted"
		    :test #'equalp))))))

(remove-all-test-files :quiet)

(unless (prove:finalize)
  #+swank
  (break "Test suite failures")
  #-swank
  ;; Non-interactive such as running via make or CI, so avoid stack trace
  (uiop/image:shell-boolean-exit nil))
