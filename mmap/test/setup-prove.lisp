;;;; setup-prove.lisp

#+testing
(setf prove:*debug-on-error* t)

#+tap-test-report
(setf prove:*default-reporter* :tap)

(when (string= (uiop:getenv "TERM") "dumb")
  (setf prove:*enable-colors* nil))

(in-package #:mmap/test)

(defparameter %filepath-does-not-exist% #P"/doEs/noT/eXiSt!"
	      "An intentionally bogus filepath")

;; Assume this is on a POSIX-compliant system.  Thus, /tmp exists:
(defparameter %test-read-filepath%
  #P"/tmp/cl-mmap-test-reading_DELETE-ME.dat")

(defparameter %test-write-filepath%
  #P"/tmp/cl-mmap-test-writing_DELETE-ME.dat")

(defparameter %test-renamed-filepath%
  #P"/tmp/cl-mmap-test-writing_DELETE-ME.dat~"
  "Semantics of this file name correspond to behaviour of `%rename'")

(defparameter %header% (format nil "This is a GENERATED test file~
				   and may be safely deleted.~%"))

(defparameter %alphabet% (format nil "ABCDEFGHIJKLMNOPQRSTUVWXYZ~%"))

(defparameter %hex-digits% (format nil "0123456789ABCDEF"))

(defun create-read-file ()
  (with-open-file (out %test-read-filepath%
		       :direction :output
		       :if-exists :supersede
		       :element-type '(unsigned-byte 8))
    (write-sequence (map 'vector #'char-code %header%) out)
    (write-sequence (map 'vector #'char-code %alphabet%) out)
    (write-sequence (map 'vector #'char-code %hex-digits%) out)
    (write-sequence #(10 10 10 10) out)
    (write-sequence (map 'vector #'char-code %hex-digits%) out)
    (write-sequence (map 'vector #'char-code %hex-digits%) out)
    (loop for i to #xFF
       do (write-sequence (make-array 1
				      :element-type '(unsigned-byte 8)
				      :initial-contents (list i))
			  out))))

(defun create-write-file ()
  (with-open-file (stream %test-write-filepath%
			  :direction :output
			  :if-exists :supersede)
    (format stream "~A~%Testing process-open-flags ~%" %header%)))

(defun test-write-file-size ()
  (with-open-file (stream %test-write-filepath%
			  :direction :input
			  :if-does-not-exist nil)
    (when stream
      (file-length stream))))

(defun remove-all-test-files (&optional quiet)
  (handler-case
      (osicat-posix:unlink %test-read-filepath%)
    (osicat-posix:enoent ()
      (unless quiet
	(format *error-output*
		"~&No such file or directory: ~A~%"
		%test-read-filepath%))))
  (handler-case
      (osicat-posix:unlink %test-write-filepath%)
    (osicat-posix:enoent ()
      (unless quiet
	(format *error-output*
		"~&No such file or directory: ~A~%"
		%test-write-filepath%))))
  (handler-case
      (osicat-posix:unlink %test-renamed-filepath%)
    (osicat-posix:enoent ()
      (unless quiet
	(format *error-output*
		"~&No such file or directory: ~A~%"
		%test-renamed-filepath%)))))

(defun compare-bytes-to-value (byte-vector value)
  "Test for equivalance of bytes that have been re-read from mmap.
  These may not be directly compared, as one will be a vector or
  bytes, the other a big-num integer.
  Returns: Boolean of success."
  (every #'identity
	 (loop
	    with length = (length byte-vector)
	    for byte across byte-vector
	    for bit-offset downfrom (* (1- length) 8) downto 0 by 8
	    collect (= byte (ldb (byte 8 bit-offset) value)))))
