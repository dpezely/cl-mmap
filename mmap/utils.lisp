;;;; utils.lisp

(in-package #:mmap)

(eval-when (:compile-toplevel :load-toplevel :execute)

  (defun remove-keywords (keywords options)
    "Intended for macros needing to prune keyword options before dispatching
  a wrapped function.
  Returns OPTIONS but without any key/value neighbours specified in
  KEYWORDS list."
    (loop
       while options
       for x = (pop options)
       when (find x keywords)
       do (pop options)
       else
       collect x)))
