;;;; package.lisp

(in-package #:cl-user)

(defpackage #:mmap
  (:use #:cl)
  (:export #:+posix-page-size+
           #:*file-rename-suffix*
	   #:*default-file-mode*

           #:with-mmap
           #:mmap
           #:munmap
           #:sync
           #:random-access
           #:mremap
           #:get-byte
           #:get-char
           #:get-big-endian
           #:get-little-endian
           #:get-byte-vector
           #:get-big-endian-vector
           #:get-little-endian-vector
           #:put-byte
           #:put-char
           #:put-big-endian
           #:put-little-endian
           #:put-byte-vector
           #:put-big-endian-vector
           #:put-little-endian-vector

           #:mmap-error
           #:end-of-mmap-error
           #:posix-error
           #:open-posix-error
           #:fstat-posix-error
           #:lseek-posix-error
           #:write-posix-error
           #:mmap-posix-error
           #:msync-posix-error
           #:close-posix-error

	   ;; Advanced use cases:
           #:validate-open-args
	   #:*default-posix-open-flags*
	   #:*default-posix-mmap-flags*))

(defpackage #:mmap/test
  (:use #:cl))
