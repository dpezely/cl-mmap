CL-MMAP
=======

This is a pair of Common Lisp libraries for working with memory-mapped files
on POSIX-compliant operating systems.

There are actually two packages here:

1. [CL-MMAP](./cl-mmap/) -- higher-level interface, read mmap file linearly
2. [MMAP](./mmap/) -- lower-level interface to POSIX `mmap()`

The higher-level API offers function names similar to Common Lisp standard
library and shadows several CL package symbols such as OPEN, READ, WRITE,
etc.  Paramters to these library functions should be familiar.

For instance, open a memory-mapped file using options including `:direction`
and `:if-does-not-exist`.  Based upon those values, reasonable defaults are
applied for low-level POSIX `open()` and `mmap()` flags.

Functions also exist for operating on Big Endian and Little Endian values
and sequences.

That library in turn uses the lower-level MMAP package, also included here.

However, incrementing file offsets becomes responsibility of the calling
scope.

## Overview

First and foremost-- mmap is **not** a magic bullet and may introduce
*performance penalties* when operating on ordinary data files, with all else
being equal.

This library is intended to offer a generic platform and some example code
from which you may build your library, framework or application.

POSIX-compliant mmap is well suited for data being pinned into memory of a
long-running process, but beware of OS paging overhead.

It may also be used for *sharing* memory between concurrent processes on the
same host.

There **may or may not** be advantages keeping volatile data away from the
garbage collector.
(See [Inside Orbitz, tidbit #4](http://www.paulgraham.com/carl.html), but
that was written in January 2001 from their prior experiences, and OS
kernels have seen much tuning since then.)

See [mmap/test/README](./mmap/test/) for comparative performance measurements.

## Getting Started

The Common Lisp package for higher-level abstraction is CL-MMAP.

**Reading**

Using CL-MMAP's `with-open-mmap` macro and `read-big-endian` function to
read one value of multiple bytes or return NIL:

```lisp
CL-USER> (require 'cl-mmap)
CL-USER> (cl-mmap:with-open-mmap (mmap-addr #P"/tmp/foo.dat")
            (cl-mmap:read-big-endian mmap-addr 8 nil))
```

**Writing**

An example write operation putting characters of alphabet plus Newline into
a text file:

```lisp
CL-USER> (cl-mmap:with-open-mmap (mmap-addr #P"/tmp/foo.txt" 0 27
                                  :direction :output)
           (loop for x upfrom (char-code #\A) to (char-code #\Z)
              do (cl-mmap:write-byte mmap-addr x))
           (cl-mmap:write-byte mmap-addr (char-code #\Newline)))
10
CL-USER> 
```

```bash
bash:~> cat /tmp/foo.txt
ABCDEFGHIJKLMNOPQRSTUVWXYZ
bash:~> 
```

Notice that an explicit save is omitted due to semantics of POSIX mmap()
doing this for you upon successful close.  Many implementations *may* also
synchronize modifications *at any time*, according to the POSIX spec.

However, you may persist the whole mapped file or any specific subset by
using `save` function, with options for OS notifying other mappings of this
same file or region, respectively.

## Lower-Level API

The lower-level package from which CL-MMAP is built is MMAP, which uses CFFI
and osicat-posix Lisp libraries.

**Low-Level Reading**

Using MMAP's `with-mmap` macro and `get-big-endian` function to read one value
comprised as multiple bytes from beginning of file:

```lisp
CL-USER> (require 'mmap)
CL-USER> (mmap:with-mmap (mmap-addr #P"/tmp/foo.dat" nil nil)
            (mmap:get-big-endian mmap-addr 0 8))
```

**Low-Level Writing**

An example write operation putting characters of alphabet plus Newline into
a text file:

```lisp
CL-USER> (mmap:with-mmap (addr #P"/tmp/foo.txt" 0 27
                                  :direction :output)
           (loop for x upfrom (char-code #\A) to (char-code #\Z)
                 for offset upfrom 0
                 do (mmap:put-byte addr offset x))
           (mmap:put-byte addr 26 (char-code #\Newline)))
10
CL-USER> 
```

There is also a `sync` function for persisting, which is closer to the name
of the actual POSIX system-call, msync.

There are other tunables at this level, such as advising the OS of random
access usage pattern, but POSIX allows your OS to ignore these requests.

	*	*	*

(TO DO: add example exercising multi-process shared map with sync, which of
course is one of the primary use cases for POSIX `mmap()`.)


## Exported Symbols

The **higher level CL-MMAP package** exports the following symbols.

For every **read-** operation, there is an equivalent **write-** function
within the top-level API within CL-MMAP package:

* `with-open-mmap` - macro that ensures that a memory-mapped
  file is safely unmapped (using `munmap-file` function) after we are
  done with it
	+ In the case of having opened the file as read/write or write-only,
	  modifications are automatically persisted upon a clean file close;
	  i.e., "clean" meaning *before* UNWIND-PROTECT's cleanup form
	+ However, your OS may perform an intermittent save at any time
	+ In the case of errors, file is *not closed* and *not explicitly saved*
	  to avoid potential corruption, so you can handle the error and retry
* `open` - with many options same as
  [`OPEN`](http://www.lispworks.com/documentation/HyperSpec/Body/f_open.htm),
   calls POSIX `open()` and `mmap()`
* `close` - remove memory-map and close file
* `read-byte`, `read-char` - read one byte or character from memory-mapped file
* `read-byte-vector` - read vector of single byte elements such as
  ISO-8859-1 or ASCII characters
	+ Further processing would be required for handling UTF-8 properly
	+ See also: UTF8-OCTETS-TO-STRING
	  in [com.google.base](http://quickdocs.org/com.google.base/api)
* `read-big-endian` and `read-little-endian` - operate on multiple bytes as
  a single value
	+ Bytes for unsigned integer #x01020304 stored using Big Endian would
	  appear as octets equivalent to vector #(#x01 #x02 #x03 #x04): from low
	  address to high
	+ Conversely, same vector stored using Little Endian would appear
	  reversed in mmap file: #(#x04 #x03 #x02 #x01)
	+ Remember: Internet Protocol's "network byte order" is Big Endian
*  `read-big-endian-vector` and `read-little-endian-vector` - for
  multi-byte elements within a vector
* `save` and `save-region` - commit pending writes of entire mapped file or
  a subset within mapped file, respectively
	+ Note: the OS may perform an intermittent save, so these commands
      simply give the means to persist on demand
* `seek` and `tell` - sets and gets current position within mapped file,
  respectively

The **lower-level MMAP package** uses similar names but with `get-` and `put-`
(instead of `read-` and `write-`, respectively).

For every **get-** operation, there is an equivalent **put-** function:

* `with-mmap` - macro ensuring that the specificed memory-mapped file opened
  is safely unmapped afterwards
	+ In the case of having opened the file as read/write or write-only,
	  modifications are automatically persisted upon a clean file close;
	  i.e., "clean" meaning *before* UNWIND-PROTECT's cleanup form
	+ However, your OS may perform an intermittent save at any time
* `mmap` - opens and maps a file using simiarl options as
  [`OPEN`](http://www.lispworks.com/documentation/HyperSpec/Body/f_open.htm),
   calls POSIX `open()` and `mmap()`
* `munmap` - remove memory-map and close file
* `get-byte`, `get-char` - read one byte or character from memory-mapped file
* `get-byte-vector` - read vector of single byte elements such as
  ISO-8859-1 or ASCII characters
	+ Further processing would be required for handling UTF-8 properly
	+ See also: UTF8-OCTETS-TO-STRING
	  in [com.google.base](http://quickdocs.org/com.google.base/api)
* `get-big-endian` and `get-little-endian` - operate on multiple bytes as
  a single value
	+ Bytes for unsigned integer #x01020304 stored using Big Endian would
	  appear as octets equivalent to vector #(01 02 03 04): from low
	  address to high
	+ Conversely, same vector stored using Little Endian would appear
	  reversed in mmap file: #(04 03 02 01)
	+ Remember: Internet Protocol's "network byte order" is Big Endian
*  `get-big-endian-vector` and `get-little-endian-vector` - for
  multi-byte elements within a vector
* `sync` - commit pending writes of entire mapped file or a subset within
  mapped file, respectively
	+ Note: the OS may perform an intermittent save, so these commands
      simply give the means to persist on demand

## Dependencies

Only these packages are required to build and use this library:

* [Alexandria](https://common-lisp.net/project/alexandria/)
* [Osicat](https://common-lisp.net/project/osicat/)
* [CFFI](https://common-lisp.net/project/cffi/)

For automated testing:

* [Prove](http://quickdocs.org/prove/)

For generating documentation:

* [cl-gendoc](http://quickdocs.org/cl-gendoc/)

## Strengths

* Easy-to-use API for working with memory-mapped files on POSIX-compliant systems
* Believed to be portable across Lisps supported by libraries named above
* Use of options and behaviour similar to corresponding Common Lisp functions:
	+ Shares CL:OPEN's options such as `:direction` and `:if-exists`
	+ All variations of read-* functions allow control over EOF or
      signalling an error when reading beyond end of memory-mapped file
	+ Various write-* functions return the same principal object like
      CL:WRITE does, and similar to our `read` allows control over EOF
* Sufficiently idiomatic Lisp in terms of CONDITION handling for POSIX system-calls
* Comprehensive unit tests and feature tests with respectable code coverage
* Advanced use cases may remove "training wheels"
	+ For production builds, elide some safety checks when opening mmap
	+ Push `:open-mmap-without-safety` onto `*features*` and recompile
	  (see `open.lisp`)
* Advanced techniques involving open() or mmap() *not rigorously tested*
  but supported by allowing custom bit-flags be supplied to `mmap-file`
  function and `with-mmap-file` macro:
	+ open() flags including:
	  `O_DSYNC`/`O_SYNC`, `O_LARGEFILE`, `O_NOATIME`, `O_NONBLOCK`/`O_NDELAY`
	+ mmap() flags including:
	  `MAP_HUGETLB`, `MAP_NORESERVE`
	  (but `MAP_ANONYMOUS` not supported where fd required to be -1)

## Limitations

* This library supports reading and writing data using memory-mapped files
  on **only POSIX** compliant systems:
	+ Supported systems include: BSD Unix, Linux and macOS
	+ See [Windows Subsystem for Linux](https://blogs.msdn.microsoft.com/wsl)
          for W10 or [andLinux](http://andlinux.org/) for W2k through W7
	+ Cygwin -- while great for many things -- would be ill-advised since it
	  is a userland translation library for portability, not performance
    + Pull requests welcomed: a compiler directive that substitutes the
      low-level MMAP-OPS package with a Windows equivalant could be
      transparent from perspective of the top-level API
* Multi-threading or multi-processing **partially** supported:
	+ See `save` function with keyword args, `:async` and `:notify`;
	  however, anticipate that the OS may *also* persist it at any time
	+ Use of CL-MMAP or MMAP for *concurrent* shared memory across OS
	  threads or processes was **not** rigorously tested as a general use
	  case, but file-backed shared memories can be built on top
* Only 8-bit byte transfer operations are available in this version:
	+ That is, multi-byte values are read/written as sequence of bytes
	+ However, native 32-bit and 64-bit operations are forthcoming
	+ Compatibility and translation modes should be straight-forward to add
* Hints on usage via `madvise(2)` not rigorously tested:
    + This POSIX equivalent system-call is used, `posix_madvise(2)`
    + Flagging mmap usage as `MADV_RANDOM` is experimental in v1.0 of this
      package
    + When reading more than a single page (typically 4096 bytes) in a
      single operation or as sequence, you probably *don't want this*
      enabled
* Versioning of files beyond simple renaming **not** supported:
	+ Nothing special done for ZFS nor Btrfs, etc.
	+ Regardless of whether your Lisp offers comprehensive versioning via
	  `OPEN` or not, renaming and versioning of mmap files here is performed
	  via POSIX calls to ensure sanity for mmap()

## Running Tests

Tests are triggered using ASDF.  See `cl-mmap.asd` file.

```lisp
CL-USER> (require 'mmap)
CL-USER> (asdf:test-system mmap :force t)

CL-USER> (require 'cl-mmap)
CL-USER> (asdf:test-system cl-mmap :force t)
```

The `:force t` flag causes a fresh compilation.

For automated testing via Makefiles or continuous build systems, consider
setting:

```lisp
CL-USER> (setf prove:*default-reporter* :tap)
```

## Automation

Within each package's subdirectory, [./mmap/](./mmap/) and
[./cl-mmap/](./cl-mmap/), respectively:

Tests may be run using `make clean test` but assumes SBCL for code coverage
reports.

Similarly for documentation: `make clean doc`

## License

MIT License

Essentially, do as you wish and without warranty from authors or maintainers.
