# Top-level makefile for CL-MMAP, which includes MMAP package dependency

all: deps test lib doc

.PHONY: deps
deps:
	(cd mmap && make deps)
	(cd cl-mmap && make deps)

.PHONY: test
test:
	(cd mmap && make test)
	(cd cl-mmap && make test)

.PHONY: lib
lib:
	(cd mmap && make lib)
	(cd cl-mmap && make lib)

.PHONY: doc
doc:
	(cd mmap && make doc)
	(cd cl-mmap && make doc)

.PHONY: clean
clean:
	(cd mmap && make clean)
	(cd cl-mmap && make clean)
