;;;; package.lisp

;; For idomatic naming, some functions shadow those in CL package.
;;
;; From HyperSpec page for MAKE-PACKAGE:
;; http://www.lispworks.com/documentation/HyperSpec/Body/f_mk_pkg.htm
;;
;;  "In situations where the packages to be used contain symbols
;;  which would conflict, it is necessary to first create the
;;  package with :use '(), then to use shadow or shadowing-import to
;;  address the conflicts, and then after that to use use-package
;;  once the conflicts have been addressed."
;;
;; However, from HyperSpec page for DEFPACKAGE:
;; http://www.lispworks.com/documentation/HyperSpec/Body/m_defpkg.htm
;;
;;  "The order in which the options appear in a defpackage form is
;;  irrelevant. The order in which they are executed is as follows:
;;  1. :shadow and :shadowing-import-from.
;;  2. :use.
;;  3. :import-from and :intern.
;;  4. :export."
;;
;; Therefore, see USE-PACKAGE immediately following DEFPACKAGE below:

(in-package #:cl-user)

(defpackage #:cl-mmap
  (:shadow #:open
           #:close
           #:read
           #:read-byte
	   #:read-char
           #:write
	   #:write-byte
	   #:write-char)
  (:export #:with-open-mmap
	   #:open
           #:close
           #:read
           #:read-char
           #:read-byte
           #:read-byte-vector
           #:read-little-endian
           #:read-big-endian
           #:read-little-endian-vector
           #:read-big-endian-vector
           #:save
           #:save-region
           #:seek
           #:tell
           #:write
           #:write-char
           #:write-byte
           #:write-byte-vector
           #:write-little-endian
           #:write-big-endian
           #:write-little-endian-vector
           #:write-big-endian-vector))

;; For facilitating shadowing of CL functions:
(use-package 'cl 'cl-mmap)

(defpackage #:cl-mmap/test
  (:use #:cl)
  (:import-from #:cl-mmap #:with-open-mmap))
