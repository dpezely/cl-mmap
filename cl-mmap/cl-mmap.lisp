;;;; cl-mmap.lisp - top-level API which shadows functions from CL package

(in-package #:cl-mmap)

(defstruct mmapped-file
  (filepath "" :type (or string pathname))
  (status :unopened :type symbol)
  (fd -1 :type fixnum)
  (addr (cffi:null-pointer))
  (offset 0 :type fixnum)
  (length-bytes 0 :type fixnum))

(defmacro with-open-mmap ((mmapped-file filename &rest options
                                        &key direction if-exists if-does-not-exist
                                        offset length-bytes mode
                                        posix-open-flags posix-mmap-flags)
			  &body body)
  "This macro ensures that a mapped file opened gets closed properly.
  First parameter, MMAPPED-FILE, becomes a captured/anaphoric variable
  for use within BODY.
  DIRECTION defaults to :input, and then OFFSET and LENGTH-BYTES may
  be omitted for mapping whole file; otherwise, LENGTH-BYTES is required.
  See description of `open' function for other options and behaviour.
  Returns: results from final expression of BODY."
  (declare (ignorable direction if-exists if-does-not-exist
                      mode posix-open-flags posix-mmap-flags))
  (let ((open-options (mmap::remove-keywords '(:offset :length-bytes) options)))
    ;; Anaphoric (intentionally un-hygenic) use of MMAPPED-FILE in
    ;; multiple instances:
    `(let ((,mmapped-file (cl-mmap:open ,filename ,offset ,length-bytes
                                        ,@open-options)))
       (unwind-protect
            (progn ,@body)
         (cl-mmap:close ,mmapped-file)))))

(defun open (filename offset length-bytes &rest options
             &key direction if-exists if-does-not-exist
               mode posix-open-flags posix-mmap-flags)
  "Open a file with mmap() semantics using parameters similar to CL:OPEN.
  DIRECTION defaults to :input, and then OFFSET and LENGTH-BYTES may both
  be NIL for mapping whole file; otherwise, LENGTH-BYTES must be specified.
  Unless LENGTH-BYTES is NIL, it must be a positive integer and
  ideally as a multiple of page size.  See `+posix-page-size+'.
  When IF-DOES-NOT-EXIST is :create or :supersede, specify LENGTH-BYTES.
  File permissions may be set with MODE; e.g., #o0644 for world-readable.
  For advanced use cases, POSIX-OPEN-FLAGS and POSIX-MMAP-FLAGS specify
  optional bit-flags as documented in Unix manual open(2) and mmap(2),
  respectively.  When supplied, these bit-flags are applied via LOGIOR.
  Returns: populated STRUCT of `mmapped-file' suitable as first parameter
  to functions such as `read-big-endian' and `write-big-endian'.
  See also: `with-open-mmap' macro, and consider using that instead."
  (declare (ignorable length-bytes offset mode
		      direction if-exists if-does-not-exist
		      posix-open-flags posix-mmap-flags))
  (multiple-value-bind (mmap-addr fd actual-size)
      (apply 'mmap:mmap (list* filename offset length-bytes options))
    (when mmap-addr
      #+verbose
      (format t "open direction=~A addr=~A fd=~A actual-size=~D~%"
              direction mmap-addr fd actual-size)
      (make-mmapped-file :filepath filename :status direction :fd fd
                         :addr mmap-addr :length-bytes actual-size))))

(defun close (mmapped-file)
  "An mmap file that was opened using `mmap-file' must be closed,
  especially if it was opened with direction :output or :io to avoid
  leaking file descriptors.
  Modifications made to mapped files that were opened for writing will
  be persisted by OS upon unmap.
  If calling this function directly, also consider UNWIND-PROTECT."
  (with-accessors ((addr mmapped-file-addr)
		   (length-bytes mmapped-file-length-bytes)
                   (status mmapped-file-status)
                   (fd mmapped-file-fd)) mmapped-file
    (when (> fd -1)
      (prog1
	  (mmap:munmap addr fd length-bytes)
	(setf status :closed
              addr (cffi:null-pointer)
	      length-bytes -1
	      fd -1)))))

(defun read-byte (mmapped-file &optional (eof-error-p t) eof-value)
  "Reads and returns one byte from a memory-mapped file, MMAPPED-FILE.
  Remaining arguments are similar to CL:READ.  Setting EOF-ERROR-P nil
  suppresses `end-of-mmap-error'.  May signal `posix-error'.
  Returns: value from file or EOF-VALUE."
  (with-accessors ((addr mmapped-file-addr)
		   (offset mmapped-file-offset)
                   (length-bytes mmapped-file-length-bytes)
                   (filepath mmapped-file-filepath)) mmapped-file
    (cond
      ((< offset length-bytes)
       (prog1
	   (mmap:get-byte addr offset)
	 (incf offset)))
      (eof-error-p
       (error 'mmap:end-of-mmap-error :filepath filepath
	      :file-size length-bytes :offset offset))
      (t
       eof-value))))

(defun read-char (mmapped-file &optional (eof-error-p t) eof-value)
  "Reads and returns a character from a memory-mapped file, MMAPPED-FILE.
  Remaining arguments are similar to CL:READ.  Setting EOF-ERROR-P nil
  suppresses `end-of-mmap-error'.  May signal `posix-error'.
  Returns: value from file or EOF-VALUE."
  (with-accessors ((addr mmapped-file-addr)
		   (offset mmapped-file-offset)
                   (length-bytes mmapped-file-length-bytes)
                   (filepath mmapped-file-filepath)) mmapped-file
    (cond
      ((< offset length-bytes)
       (prog1
	   (mmap:get-char addr offset)
	 (incf offset)))
      (eof-error-p
       (error 'mmap:end-of-mmap-error :filepath filepath
	      :file-size length-bytes :offset offset))
      (t
       eof-value))))

(defun read-big-endian (mmapped-file n-bytes &optional (eof-error-p t) eof-value)
  "Reads N-BYTES sized value assuming Big Endian byte sequence
  from a memory-mapped file, MMAPPED-FILE.
  Remaining arguments are similar to CL:READ.  Setting EOF-ERROR-P nil
  suppresses `end-of-mmap-error'.  May signal `posix-error'.
  Big Endian is also known as Network Byte Order for IP.
  Returns: value from file or EOF-VALUE."
  (declare (type (integer 1) n-bytes))
  (with-accessors ((addr mmapped-file-addr)
		   (offset mmapped-file-offset)
                   (length-bytes mmapped-file-length-bytes)
                   (filepath mmapped-file-filepath)) mmapped-file
    (cond
      ((<= (+ offset n-bytes) length-bytes)
       (prog1
	   (mmap:get-big-endian addr offset n-bytes)
	 (incf offset n-bytes)))
      (eof-error-p
       (error 'mmap:end-of-mmap-error :filepath filepath
	      :file-size length-bytes :offset offset))
      (t
       eof-value))))

(defun read-little-endian (mmapped-file n-bytes &optional (eof-error-p t) eof-value)
  "Reads N-BYTES sized value assuming Little Endian byte sequence
  from a memory-mapped file, MMAPPED-FILE.
  Remaining arguments are similar to CL:READ.  Setting EOF-ERROR-P nil
  suppresses `end-of-mmap-error'.  May signal `posix-error'.
  Returns: value from file or EOF-VALUE."
  (declare (type (integer 1) n-bytes))
  (with-accessors ((addr mmapped-file-addr)
		   (offset mmapped-file-offset)
                   (length-bytes mmapped-file-length-bytes)
                   (filepath mmapped-file-filepath)) mmapped-file
    (cond
      ((<= (+ offset n-bytes) length-bytes)
       (prog1
	   (mmap:get-little-endian addr offset n-bytes)
	 (incf offset n-bytes)))
      (eof-error-p
       (error 'mmap:end-of-mmap-error :filepath filepath
	      :file-size length-bytes :offset offset))
      (t
       eof-value))))

(defun read-byte-vector (mmapped-file length &optional (eof-error-p t) eof-value)
  "Reads vector of bytes from memory-mapped file, MMAPPED-FILE.
  Remaining arguments are similar to CL:READ.  Setting EOF-ERROR-P nil
  suppresses `end-of-mmap-error'.  May signal `posix-error'.
  Returns: vector from file or EOF-VALUE."
  (declare (type (integer 0) length))
  (with-accessors ((addr mmapped-file-addr)
		   (offset mmapped-file-offset)
		   (length-bytes mmapped-file-length-bytes)
                   (filepath mmapped-file-filepath)) mmapped-file
    (cond
      ((<= (+ offset length) length-bytes)
       (prog1
	   (mmap:get-byte-vector addr offset length)
	 (incf offset length)))
      (eof-error-p
       (error 'mmap:end-of-mmap-error :filepath filepath
	      :file-size length-bytes :offset offset))
      (t
       eof-value))))

(defun read-big-endian-vector (mmapped-file length n-bytes-per-element
                               &optional (eof-error-p t) eof-value)
  "Reads and returns vector of N-BYTES sized elements from
  memory-mapped file, MMAPPED-FILE assuming Big Endian byte sequence.
  Remaining arguments are similar to CL:READ.  Setting EOF-ERROR-P nil
  suppresses `end-of-mmap-error'.  May signal `posix-error'.
  Big Endian is also known as Network Byte Order for IP.
  Returns: vector from file or EOF-VALUE."
  (declare (type (integer 0) length) (type (integer 1) n-bytes-per-element))
  (with-accessors ((addr mmapped-file-addr)
		   (offset mmapped-file-offset)
		   (length-bytes mmapped-file-length-bytes)
                   (filepath mmapped-file-filepath)) mmapped-file
    (let ((offset-delta (* length n-bytes-per-element)))
      (cond
	((<= (+ offset offset-delta) length-bytes)
	 (prog1
	     (mmap:get-big-endian-vector addr offset length
                                         n-bytes-per-element)
	   (incf offset offset-delta)))
	(eof-error-p
	 (error 'mmap:end-of-mmap-error :filepath filepath
		:file-size length-bytes :offset offset))
	(t
	 eof-value)))))

(defun read-little-endian-vector (mmapped-file length n-bytes-per-element
				  &optional (eof-error-p t) eof-value)
  "Reads and returns vector of N-BYTES sized elements from
  memory-mapped file, MMAPPED-FILE assuming Little Endian byte sequence.
  Remaining arguments are similar to CL:READ.  Setting EOF-ERROR-P nil
  suppresses `end-of-mmap-error'.  May signal `posix-error'.
  Returns: vector from file or EOF-VALUE."
  (declare (type (integer 0) length) (type (integer 1) n-bytes-per-element))
  (with-accessors ((addr mmapped-file-addr)
		   (offset mmapped-file-offset)
		   (length-bytes mmapped-file-length-bytes)
                   (filepath mmapped-file-filepath)) mmapped-file
    (let ((offset-delta (* length n-bytes-per-element)))
      (cond
	((<= (+ offset offset-delta) length-bytes)
	 (prog1
	     (mmap:get-little-endian-vector addr offset length
                                            n-bytes-per-element)
	   (incf offset offset-delta)))
	(eof-error-p
	 (error 'mmap:end-of-mmap-error :filepath filepath
		:file-size length-bytes :offset offset))
	(t
	 eof-value)))))

(defun save (mmapped-file &key start n-bytes async notify)
  "Commit pending writes of region within mapped file
  so modifications persist for only that region.
  Optional region specified by START spanning N-BYTES must be on
  boundary of OS page, so use multiples of `+posix-page-size+'.
  Specifying ASYNC, return before OS has completed the request.
  When keyword param NOTIFY is non-NIL, instructs OS kernel to
  invalidate other mappings of this file to ensure getting fresh
  values just written.
  Attempts to save a read-only map are ignored.
  May signal `posix-error'.
  Returns: boolean indicating whether request was performed or not."
  (when start
    (check-type start (integer 0) "non-negative integer"))
  (when n-bytes
    (check-type n-bytes (integer 1) "positive integer"))
  (when (find (mmapped-file-status mmapped-file) '(:output :io))
    (with-accessors ((addr mmapped-file-addr)
                     (length-bytes mmapped-file-length-bytes)) mmapped-file
      (assert (<= 0 start length-bytes)
              (n-bytes length-bytes)
              "START=#x~X plus N-BYTES=~D must be less than LENGTH-BYTES=~D"
              start n-bytes length-bytes)
      (assert (= (mod start mmap:+posix-page-size+) 0)
              (start)
              "START=#x~X must be a multiple of +POSIX-PAGE-SIZE+=#x~X"
              start mmap:+posix-page-size+)
      (assert (= (mod n-bytes mmap:+posix-page-size+) 0)
              (n-bytes)
              "N-BYTES=~D must be a multiple of +POSIX-PAGE-SIZE+=~D"
              n-bytes mmap:+posix-page-size+)
      (mmap:sync addr start n-bytes :async async :notify notify))
    t))

(defun seek (mmapped-file new-offset)
  "Set NEW-OFFSET as FIXNUM to be new current position within MMAPPED-FILE.
  Returns: NEW-OFFSET."
  (declare (type fixnum new-offset))
  (setf (mmapped-file-offset mmapped-file) new-offset))

(defun tell (mmapped-file)
  "Return FIXNUM offset of current position within MMAPPED-FILE."
  (mmapped-file-offset mmapped-file))

(defun write-byte (mmapped-file byte &optional (eof-error-p t) eof-value)
  "Write a single BYTE to mmap.
  To persist this write, either use `with-mmap-file' macro or
  explicitly call `save' or `save-region' functions.  Otherwise,
  changes are ephemeral and last only until file is closed.
  Setting EOF-ERROR-P nil suppresses `end-of-mmap-error'.
  May signal `posix-error'.
  Returns: BYTE upon success or EOF-VALUE."
  (with-accessors ((addr mmapped-file-addr)
		   (offset mmapped-file-offset)
                   (length-bytes mmapped-file-length-bytes)
                   (filepath mmapped-file-filepath)) mmapped-file
    (cond
      ((and (< offset length-bytes)
	    (mmap:put-byte addr offset byte))
       (incf offset)
       byte)
      (eof-error-p
       (error 'mmap:end-of-mmap-error :filepath filepath
	      :file-size length-bytes :offset offset :value byte))
      (t
       eof-value))))

(defun write-char (mmapped-file character &optional (eof-error-p t) eof-value)
  "Writes a single CHARACTER to memory-mapped file, MMAPPED-FILE.
  Beware this function is only suitable for writing single byte
  ISO-8859-1 or ASCII characters or similar.  It's ill-suited for
  UTF-8 values of more than one byte.
  To persist this write, either use `with-mmap-file' macro or
  explicitly call `save' or `save-region' functions.  Otherwise,
  changes are ephemeral and last only until file is closed.
  Setting EOF-ERROR-P nil suppresses `end-of-mmap-error'.
  May signal `posix-error'.
  Returns: CHARACTER written or EOF-VALUE."
  (declare (type character character))
  (with-accessors ((addr mmapped-file-addr)
		   (offset mmapped-file-offset)
                   (length-bytes mmapped-file-length-bytes)
                   (filepath mmapped-file-filepath)) mmapped-file
    (cond
      ((and (< offset length-bytes)
	    (mmap:put-char addr offset character))
       (incf offset)
       character)
      (eof-error-p
       (error 'mmap:end-of-mmap-error :filepath filepath
	      :file-size length-bytes :offset offset :value character))
      (t
       eof-value))))

(defun write-big-endian (mmapped-file value n-bytes
			 &optional (eof-error-p t) eof-value)
  "Writes VALUE of N-BYTES using Big Endian byte sequence
  to memory-mapped file, MMAPPED-FILE.
  To persist this write, either use `with-mmap-file' macro or
  explicitly call `save' or `save-region' functions.  Otherwise,
  changes are ephemeral and last only until file is closed.
  Setting EOF-ERROR-P nil suppresses `end-of-mmap-error'.
  Big Endian is also known as Network Byte Order for IP.
  May signal `posix-error'.
  Returns: VALUE on success or EOF-VALUE."
  (declare (type integer value) (type (integer 1) n-bytes))
  (with-accessors ((addr mmapped-file-addr)
		   (offset mmapped-file-offset)
                   (length-bytes mmapped-file-length-bytes)
                   (filepath mmapped-file-filepath)) mmapped-file
    (cond
      ((and (<= (+ offset n-bytes) length-bytes)
	    (mmap:put-big-endian addr offset value n-bytes))
       (incf offset n-bytes)
       value)
      (eof-error-p
       (error 'mmap:end-of-mmap-error :filepath filepath
	      :file-size length-bytes :offset offset :value value))
      (t
       eof-value))))

(defun write-little-endian (mmapped-file value n-bytes
			    &optional (eof-error-p t) eof-value)
  "Writes VALUE of N-BYTES using Little Endian byte sequence
  to memory-mapped file, MMAPPED-FILE.
  To persist this write, either use `with-mmap-file' macro or
  explicitly call `save' or `save-region' functions.  Otherwise,
  changes are ephemeral and last only until file is closed.
  Setting EOF-ERROR-P nil suppresses `end-of-mmap-error'.
  May signal `posix-error'.
  Returns: VALUE on success or EOF-VALUE."
  (declare (type integer value) (type (integer 1) n-bytes))
  (with-accessors ((addr mmapped-file-addr)
		   (offset mmapped-file-offset)
                   (length-bytes mmapped-file-length-bytes)
                   (filepath mmapped-file-filepath)) mmapped-file
    (cond
      ((and (<= (+ offset n-bytes) length-bytes)
	    (mmap:put-little-endian addr offset value n-bytes))
       (incf offset n-bytes)
       value)
      (eof-error-p
       (error 'mmap:end-of-mmap-error :filepath filepath
	      :file-size length-bytes :offset offset :value value))
      (t
       eof-value))))

(defun write-byte-vector (mmapped-file vector &optional (eof-error-p t) eof-value)
  "Writes VECTOR of byte values to memory-mapped file, MMAPPED-FILE.
  To persist this write, either use `with-mmap-file' macro or
  explicitly call `save' or `save-region' functions.  Otherwise,
  changes are ephemeral and last only until file is closed.
  Setting EOF-ERROR-P nil suppresses `end-of-mmap-error'.
  May signal `end-of-mmap-error'.
  Returns: VECTOR on success or EOF-VALUE."
  (declare (type vector vector))
  (with-accessors ((addr mmapped-file-addr)
		   (offset mmapped-file-offset)
                   (length-bytes mmapped-file-length-bytes)
                   (filepath mmapped-file-filepath)) mmapped-file
    (let ((vector-length (length vector)))
      (cond
	((and (<= (+ offset vector-length) length-bytes)
	      (mmap:put-byte-vector addr offset vector))
	 (incf offset vector-length)
	 vector)
	(eof-error-p
	 (error 'mmap:end-of-mmap-error :filepath filepath
		:file-size length-bytes :offset offset :value vector))
	(t
	 eof-value)))))

(defun write-big-endian-vector (mmapped-file vector n-bytes-per-element
				&optional (eof-error-p t) eof-value)
  "Writes VECTOR of values in Big Endian byte order to memory-mapped file,
  MMAPPED-FILE.  Each vector element spans N-BYTES-PER-ELEMENT.
  To persist this write, either use `with-mmap-file' macro or
  explicitly call `save' or `save-region' functions.  Otherwise,
  changes are ephemeral and last only until file is closed.
  Setting EOF-ERROR-P nil suppresses `end-of-mmap-error'.
  Big Endian is also known as Network Byte Order for IP.
  Returns: VECTOR on success or EOF-VALUE."
  (declare (type vector vector) (type (integer 1) n-bytes-per-element))
  (with-accessors ((addr mmapped-file-addr)
		   (offset mmapped-file-offset)
                   (length-bytes mmapped-file-length-bytes)
                   (filepath mmapped-file-filepath)) mmapped-file
    (let* ((vector-length (length vector))
	   (offset-delta (* vector-length n-bytes-per-element)))
      (cond
	((and (<= (+ offset offset-delta) length-bytes)
	      (mmap:put-big-endian-vector addr offset vector
                                          n-bytes-per-element))
	 (incf offset offset-delta)
	 vector)
	(eof-error-p
	 (error 'mmap:end-of-mmap-error :filepath filepath
		:file-size length-bytes :offset offset :value vector))
	(t
	 eof-value)))))

(defun write-little-endian-vector (mmapped-file vector n-bytes-per-element
				   &optional (eof-error-p t) eof-value)
  "Writes VECTOR of values in Little Endian byte order to memory-mapped file,
  MMAPPED-FILE.  Each vector element spans N-BYTES-PER-ELEMENT.
  To persist this write, either use `with-mmap-file' macro or
  explicitly call `save' or `save-region' functions.  Otherwise,
  changes are ephemeral and last only until file is closed.
  Setting EOF-ERROR-P nil suppresses `end-of-mmap-error'.
  May signal `end-of-mmap-error'.
  Returns: VECTOR on success or EOF-VALUE."
  (declare (type vector vector) (type (integer 1) n-bytes-per-element))
  (with-accessors ((addr mmapped-file-addr)
		   (offset mmapped-file-offset)
                   (length-bytes mmapped-file-length-bytes)
                   (filepath mmapped-file-filepath)) mmapped-file
    (let* ((vector-length (length vector))
	   (offset-delta (* vector-length n-bytes-per-element)))
      (cond
	((and (<= (+ offset offset-delta) length-bytes)
	      (mmap:put-little-endian-vector addr offset vector
                                             n-bytes-per-element))
	 (incf offset offset-delta)
	 vector)
	(eof-error-p
	 (error 'mmap:end-of-mmap-error :filepath filepath
		:file-size length-bytes :offset offset :value vector))
	(t
	 eof-value)))))
