CL-MMAP
=======

*Common Lisp library operating on memory-mapped files like a regular file*

See also: MMAP package which handles all of the actual memory-mapping and
related POSIX calls.  If building you own storage package or database,
consider that one instead.

## Overview

On a POSIX-compliant OS including BSD Unix, Linux and macOS operate
upon memory-mapped files with behaviour similar to Common Lisp functions
such as OPEN and READ.

File operations supported include opening with direction of `:input`,
`:output` or `:io`.  Manipulate single byte or multi-byte values including
as vector elements within the mapped file.

Big endian and little endian operations are supported.

Modifications may be persisted at granularity of an entire file or
just one region.  Both modes can optionally notify other concurrent
mappings of same file to be refreshed.

See README for example code, list of dependencies, known limitations, etc.
