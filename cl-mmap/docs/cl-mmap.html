<!doctype html>

<html lang='en'>
  <head>
    <meta charset='UTF-8' />
<title>CL-MMAP Reference - High-Level API
</title>
<link rel='stylesheet' href='style.css' />
  </head>
  <body>
<article class='markdown-article' id='file-header-md'><h1>CL-MMAP</h1>

<p><em>Common Lisp library operating on memory-mapped files like a regular file</em></p>

<p>See also: MMAP package which handles all of the actual memory-mapping and
related POSIX calls.  If building you own storage package or database,
consider that one instead.</p>

<h2>Overview</h2>

<p>On a POSIX-compliant OS including BSD Unix, Linux and macOS operate
upon memory-mapped files with behaviour similar to Common Lisp functions
such as OPEN and READ.</p>

<p>File operations supported include opening with direction of <code>:input</code>,
<code>:output</code> or <code>:io</code>.  Manipulate single byte or multi-byte values including
as vector elements within the mapped file.</p>

<p>Big endian and little endian operations are supported.</p>

<p>Modifications may be persisted at granularity of an entire file or
just one region.  Both modes can optionally notify other concurrent
mappings of same file to be refreshed.</p>

<p>See README for example code, list of dependencies, known limitations, etc.</p>

</article>
<article id='reference-cl-mmap' class='apiref-article'>
  <h1>Reference: cl-mmap
  </h1>
<section id='cl-mmap-functions' class='section-functions'>
  <h2>Functions
  </h2>
<section id='apiref-close' class='section-apiref-item'>
  <div class='apiref-spec'>CLOSE
  </div>
  <div class='apiref-lambda'>(mmapped-file)
  </div>
  <div class='apiref-doc'><p>An mmap file that was opened using <code>mmap-file</code> must be closed,
  especially if it was opened with direction :output or :io to avoid
  leaking file descriptors.</p>

<p>Modifications made to mapped files that were opened for writing will
  be persisted by OS upon unmap.</p>

<p>If calling this function directly, also consider UNWIND-PROTECT.</p>

  </div>
</section>
<section id='apiref-open' class='section-apiref-item'>
  <div class='apiref-spec'>OPEN
  </div>
  <div class='apiref-lambda'>(filename offset length-bytes &amp;rest options &amp;key direction if-exists
 if-does-not-exist mode posix-open-flags posix-mmap-flags)
  </div>
  <div class='apiref-doc'><p>Open a file with mmap() semantics using parameters similar to CL:OPEN.</p>

<p>DIRECTION defaults to :input, and then OFFSET and LENGTH-BYTES may both
  be NIL for mapping whole file; otherwise, LENGTH-BYTES must be specified.</p>

<p>Unless LENGTH-BYTES is NIL, it must be a positive integer and
  ideally as a multiple of page size.  See <code>+posix-page-size+</code>.</p>

<p>When IF-DOES-NOT-EXIST is :create or :supersede, specify LENGTH-BYTES.</p>

<p>File permissions may be set with MODE; e.g., #o0644 for world-readable.</p>

<p>For advanced use cases, POSIX-OPEN-FLAGS and POSIX-MMAP-FLAGS specify
  optional bit-flags as documented in Unix manual open(2) and mmap(2),
  respectively.  When supplied, these bit-flags are applied via LOGIOR.</p>

<p>Returns: populated STRUCT of <code>mmapped-file</code> suitable as first parameter
  to functions such as <a href="#apiref-read-big-endian">read-big-endian</a> and <a href="#apiref-write-big-endian">write-big-endian</a>.</p>

<p>See also: <a href="#apiref-with-open-mmap">with-open-mmap</a> macro, and consider using that instead.</p>

  </div>
</section>
<section id='apiref-read-big-endian' class='section-apiref-item'>
  <div class='apiref-spec'>READ-BIG-ENDIAN
  </div>
  <div class='apiref-lambda'>(mmapped-file n-bytes &amp;optional (eof-error-p t) eof-value)
  </div>
  <div class='apiref-doc'><p>Reads N-BYTES sized value assuming Big Endian byte sequence
  from a memory-mapped file, MMAPPED-FILE.</p>

<p>Remaining arguments are similar to CL:READ.  Setting EOF-ERROR-P nil
  suppresses <code>end-of-mmap-error</code>.  May signal <code>posix-error</code>.</p>

<p>Big Endian is also known as Network Byte Order for IP.</p>

<p>Returns: value from file or EOF-VALUE.</p>

  </div>
</section>
<section id='apiref-read-big-endian-vector' class='section-apiref-item'>
  <div class='apiref-spec'>READ-BIG-ENDIAN-VECTOR
  </div>
  <div class='apiref-lambda'>(mmapped-file length n-bytes-per-element &amp;optional (eof-error-p t) eof-value)
  </div>
  <div class='apiref-doc'><p>Reads and returns vector of N-BYTES sized elements from
  memory-mapped file, MMAPPED-FILE assuming Big Endian byte sequence.</p>

<p>Remaining arguments are similar to CL:READ.  Setting EOF-ERROR-P nil
  suppresses <code>end-of-mmap-error</code>.  May signal <code>posix-error</code>.</p>

<p>Big Endian is also known as Network Byte Order for IP.</p>

<p>Returns: vector from file or EOF-VALUE.</p>

  </div>
</section>
<section id='apiref-read-byte' class='section-apiref-item'>
  <div class='apiref-spec'>READ-BYTE
  </div>
  <div class='apiref-lambda'>(mmapped-file &amp;optional (eof-error-p t) eof-value)
  </div>
  <div class='apiref-doc'><p>Reads and returns one byte from a memory-mapped file, MMAPPED-FILE.</p>

<p>Remaining arguments are similar to CL:READ.  Setting EOF-ERROR-P nil
  suppresses <code>end-of-mmap-error</code>.  May signal <code>posix-error</code>.</p>

<p>Returns: value from file or EOF-VALUE.</p>

  </div>
</section>
<section id='apiref-read-byte-vector' class='section-apiref-item'>
  <div class='apiref-spec'>READ-BYTE-VECTOR
  </div>
  <div class='apiref-lambda'>(mmapped-file length &amp;optional (eof-error-p t) eof-value)
  </div>
  <div class='apiref-doc'><p>Reads vector of bytes from memory-mapped file, MMAPPED-FILE.</p>

<p>Remaining arguments are similar to CL:READ.  Setting EOF-ERROR-P nil
  suppresses <code>end-of-mmap-error</code>.  May signal <code>posix-error</code>.</p>

<p>Returns: vector from file or EOF-VALUE.</p>

  </div>
</section>
<section id='apiref-read-char' class='section-apiref-item'>
  <div class='apiref-spec'>READ-CHAR
  </div>
  <div class='apiref-lambda'>(mmapped-file &amp;optional (eof-error-p t) eof-value)
  </div>
  <div class='apiref-doc'><p>Reads and returns a character from a memory-mapped file, MMAPPED-FILE.</p>

<p>Remaining arguments are similar to CL:READ.  Setting EOF-ERROR-P nil
  suppresses <code>end-of-mmap-error</code>.  May signal <code>posix-error</code>.</p>

<p>Returns: value from file or EOF-VALUE.</p>

  </div>
</section>
<section id='apiref-read-little-endian' class='section-apiref-item'>
  <div class='apiref-spec'>READ-LITTLE-ENDIAN
  </div>
  <div class='apiref-lambda'>(mmapped-file n-bytes &amp;optional (eof-error-p t) eof-value)
  </div>
  <div class='apiref-doc'><p>Reads N-BYTES sized value assuming Little Endian byte sequence
  from a memory-mapped file, MMAPPED-FILE.</p>

<p>Remaining arguments are similar to CL:READ.  Setting EOF-ERROR-P nil
  suppresses <code>end-of-mmap-error</code>.  May signal <code>posix-error</code>.</p>

<p>Returns: value from file or EOF-VALUE.</p>

  </div>
</section>
<section id='apiref-read-little-endian-vector' class='section-apiref-item'>
  <div class='apiref-spec'>READ-LITTLE-ENDIAN-VECTOR
  </div>
  <div class='apiref-lambda'>(mmapped-file length n-bytes-per-element &amp;optional (eof-error-p t) eof-value)
  </div>
  <div class='apiref-doc'><p>Reads and returns vector of N-BYTES sized elements from
  memory-mapped file, MMAPPED-FILE assuming Little Endian byte sequence.</p>

<p>Remaining arguments are similar to CL:READ.  Setting EOF-ERROR-P nil
  suppresses <code>end-of-mmap-error</code>.  May signal <code>posix-error</code>.</p>

<p>Returns: vector from file or EOF-VALUE.</p>

  </div>
</section>
<section id='apiref-save' class='section-apiref-item'>
  <div class='apiref-spec'>SAVE
  </div>
  <div class='apiref-lambda'>(mmapped-file &amp;key start n-bytes async notify)
  </div>
  <div class='apiref-doc'><p>Commit pending writes of region within mapped file
  so modifications persist for only that region.</p>

<p>Optional region specified by START spanning N-BYTES must be on
  boundary of OS page, so use multiples of <code>+posix-page-size+</code>.</p>

<p>Specifying ASYNC, return before OS has completed the request.</p>

<p>When keyword param NOTIFY is non-NIL, instructs OS kernel to
  invalidate other mappings of this file to ensure getting fresh
  values just written.</p>

<p>Attempts to save a read-only map are ignored.</p>

<p>May signal <code>posix-error</code>.</p>

<p>Returns: boolean indicating whether request was performed or not.</p>

  </div>
</section>
<section id='apiref-seek' class='section-apiref-item'>
  <div class='apiref-spec'>SEEK
  </div>
  <div class='apiref-lambda'>(mmapped-file new-offset)
  </div>
  <div class='apiref-doc'><p>Set NEW-OFFSET as FIXNUM to be new current position within MMAPPED-FILE.</p>

<p>Returns: NEW-OFFSET.</p>

  </div>
</section>
<section id='apiref-tell' class='section-apiref-item'>
  <div class='apiref-spec'>TELL
  </div>
  <div class='apiref-lambda'>(mmapped-file)
  </div>
  <div class='apiref-doc'><p>Return FIXNUM offset of current position within MMAPPED-FILE.</p>

  </div>
</section>
<section id='apiref-write-big-endian' class='section-apiref-item'>
  <div class='apiref-spec'>WRITE-BIG-ENDIAN
  </div>
  <div class='apiref-lambda'>(mmapped-file value n-bytes &amp;optional (eof-error-p t) eof-value)
  </div>
  <div class='apiref-doc'><p>Writes VALUE of N-BYTES using Big Endian byte sequence
  to memory-mapped file, MMAPPED-FILE.</p>

<p>To persist this write, either use <code>with-mmap-file</code> macro or
  explicitly call <a href="#apiref-save">save</a> or <a href="#apiref-save-region">save-region</a> functions.  Otherwise,
  changes are ephemeral and last only until file is closed.</p>

<p>Setting EOF-ERROR-P nil suppresses <code>end-of-mmap-error</code>.</p>

<p>Big Endian is also known as Network Byte Order for IP.</p>

<p>May signal <code>posix-error</code>.</p>

<p>Returns: VALUE on success or EOF-VALUE.</p>

  </div>
</section>
<section id='apiref-write-big-endian-vector' class='section-apiref-item'>
  <div class='apiref-spec'>WRITE-BIG-ENDIAN-VECTOR
  </div>
  <div class='apiref-lambda'>(mmapped-file vector n-bytes-per-element &amp;optional (eof-error-p t) eof-value)
  </div>
  <div class='apiref-doc'><p>Writes VECTOR of values in Big Endian byte order to memory-mapped file,
  MMAPPED-FILE.  Each vector element spans N-BYTES-PER-ELEMENT.</p>

<p>To persist this write, either use <code>with-mmap-file</code> macro or
  explicitly call <a href="#apiref-save">save</a> or <a href="#apiref-save-region">save-region</a> functions.  Otherwise,
  changes are ephemeral and last only until file is closed.</p>

<p>Setting EOF-ERROR-P nil suppresses <code>end-of-mmap-error</code>.</p>

<p>Big Endian is also known as Network Byte Order for IP.</p>

<p>Returns: VECTOR on success or EOF-VALUE.</p>

  </div>
</section>
<section id='apiref-write-byte' class='section-apiref-item'>
  <div class='apiref-spec'>WRITE-BYTE
  </div>
  <div class='apiref-lambda'>(mmapped-file byte &amp;optional (eof-error-p t) eof-value)
  </div>
  <div class='apiref-doc'><p>Write a single BYTE to mmap.</p>

<p>To persist this write, either use <code>with-mmap-file</code> macro or
  explicitly call <a href="#apiref-save">save</a> or <a href="#apiref-save-region">save-region</a> functions.  Otherwise,
  changes are ephemeral and last only until file is closed.</p>

<p>Setting EOF-ERROR-P nil suppresses <code>end-of-mmap-error</code>.</p>

<p>May signal <code>posix-error</code>.</p>

<p>Returns: BYTE upon success or EOF-VALUE.</p>

  </div>
</section>
<section id='apiref-write-byte-vector' class='section-apiref-item'>
  <div class='apiref-spec'>WRITE-BYTE-VECTOR
  </div>
  <div class='apiref-lambda'>(mmapped-file vector &amp;optional (eof-error-p t) eof-value)
  </div>
  <div class='apiref-doc'><p>Writes VECTOR of byte values to memory-mapped file, MMAPPED-FILE.</p>

<p>To persist this write, either use <code>with-mmap-file</code> macro or
  explicitly call <a href="#apiref-save">save</a> or <a href="#apiref-save-region">save-region</a> functions.  Otherwise,
  changes are ephemeral and last only until file is closed.</p>

<p>Setting EOF-ERROR-P nil suppresses <code>end-of-mmap-error</code>.</p>

<p>May signal <code>end-of-mmap-error</code>.</p>

<p>Returns: VECTOR on success or EOF-VALUE.</p>

  </div>
</section>
<section id='apiref-write-char' class='section-apiref-item'>
  <div class='apiref-spec'>WRITE-CHAR
  </div>
  <div class='apiref-lambda'>(mmapped-file character &amp;optional (eof-error-p t) eof-value)
  </div>
  <div class='apiref-doc'><p>Writes a single CHARACTER to memory-mapped file, MMAPPED-FILE.</p>

<p>Beware this function is only suitable for writing single byte
  ISO-8859-1 or ASCII characters or similar.  It's ill-suited for
  UTF-8 values of more than one byte.</p>

<p>To persist this write, either use <code>with-mmap-file</code> macro or
  explicitly call <a href="#apiref-save">save</a> or <a href="#apiref-save-region">save-region</a> functions.  Otherwise,
  changes are ephemeral and last only until file is closed.</p>

<p>Setting EOF-ERROR-P nil suppresses <code>end-of-mmap-error</code>.</p>

<p>May signal <code>posix-error</code>.</p>

<p>Returns: CHARACTER written or EOF-VALUE.</p>

  </div>
</section>
<section id='apiref-write-little-endian' class='section-apiref-item'>
  <div class='apiref-spec'>WRITE-LITTLE-ENDIAN
  </div>
  <div class='apiref-lambda'>(mmapped-file value n-bytes &amp;optional (eof-error-p t) eof-value)
  </div>
  <div class='apiref-doc'><p>Writes VALUE of N-BYTES using Little Endian byte sequence
  to memory-mapped file, MMAPPED-FILE.</p>

<p>To persist this write, either use <code>with-mmap-file</code> macro or
  explicitly call <a href="#apiref-save">save</a> or <a href="#apiref-save-region">save-region</a> functions.  Otherwise,
  changes are ephemeral and last only until file is closed.</p>

<p>Setting EOF-ERROR-P nil suppresses <code>end-of-mmap-error</code>.</p>

<p>May signal <code>posix-error</code>.</p>

<p>Returns: VALUE on success or EOF-VALUE.</p>

  </div>
</section>
<section id='apiref-write-little-endian-vector' class='section-apiref-item'>
  <div class='apiref-spec'>WRITE-LITTLE-ENDIAN-VECTOR
  </div>
  <div class='apiref-lambda'>(mmapped-file vector n-bytes-per-element &amp;optional (eof-error-p t) eof-value)
  </div>
  <div class='apiref-doc'><p>Writes VECTOR of values in Little Endian byte order to memory-mapped file,
  MMAPPED-FILE.  Each vector element spans N-BYTES-PER-ELEMENT.</p>

<p>To persist this write, either use <code>with-mmap-file</code> macro or
  explicitly call <a href="#apiref-save">save</a> or <a href="#apiref-save-region">save-region</a> functions.  Otherwise,
  changes are ephemeral and last only until file is closed.</p>

<p>Setting EOF-ERROR-P nil suppresses <code>end-of-mmap-error</code>.</p>

<p>May signal <code>end-of-mmap-error</code>.</p>

<p>Returns: VECTOR on success or EOF-VALUE.</p>

  </div>
</section>
</section>
<section id='cl-mmap-macros' class='section-macros'>
  <h2>Macros
  </h2>
<section id='apiref-with-open-mmap' class='section-apiref-item'>
  <div class='apiref-spec'>WITH-OPEN-MMAP
  </div>
  <div class='apiref-lambda'>((mmapped-file filename &amp;rest options &amp;key direction if-exists
  if-does-not-exist offset length-bytes mode posix-open-flags posix-mmap-flags)
 &amp;body body)
  </div>
  <div class='apiref-doc'><p>This macro ensures that a mapped file opened gets closed properly.</p>

<p>First parameter, MMAPPED-FILE, becomes a captured/anaphoric variable
  for use within BODY.</p>

<p>DIRECTION defaults to :input, and then OFFSET and LENGTH-BYTES may
  be omitted for mapping whole file; otherwise, LENGTH-BYTES is required.</p>

<p>See description of <a href="#apiref-open">open</a> function for other options and behaviour.</p>

<p>Returns: results from final expression of BODY.</p>

  </div>
</section>
</section>
</article>
<article class='markdown-article' id='file-contributing-md'><h2>Contributing</h2>

<p>During development or deeper exploration of this package, it may be useful
pushing certain keywords onto <code>*features*</code> list.</p>

<p>(Remember, of course, that a fresh compile will be necessary after updating
<code>*features*</code>.)</p>

<ul>
<li><code>(push :verbose *features*)</code>

<ul>
<li>Display logging messages to standard output.</li>
<li>Output will be mostly from <em>opening</em> memory-mapped files.</li>
</ul></li>
<li><code>(push :allow-low-level-mmap-errors *features*)</code>

<ul>
<li>Certain mmap related operations such as writing beyond file extent
  signals implementation-dependent errors.  Rather than tracking the
  multitude of specific errors (see <code>man mmap(2)</code>), they are coalesced
  into a package-specific condition.</li>
<li>This compilation flag disables the feature and may be useful while
  developing for this package or building upon it.</li>
<li>Be sure to visit code where this flag is used, and confirm granularity
  of the behaviour.</li>
</ul></li>
</ul>

<p>When updating tests:</p>

<ul>
<li><code>(setf prove:*debug-on-error* t)</code>

<ul>
<li>Use this within your development environment, so errors in test case
  code or package code may signal an error without it being masked by
  the test framework.</li>
<li>If using a test library or framework other than Prove, this variable
  of course would have no effect.</li>
</ul></li>
</ul>

</article>
<article class='markdown-article' id='file-footer-md'><h2>Links</h2>

<p>Clone <a href="https://gitlab.com/dpezely/cl-mmap" >repo</a>, which provides:</p>

<ul>
<li>Higher level <a href="https://gitlab.com/dpezely/cl-mmap/cl-mmap" >cl-mmap</a> package</li>
<li>Lower level <a href="https://gitlab.com/dpezely/cl-mmap/mmap" >mmap</a> package</li>
</ul>

</article>
<article class='markdown-article' id='file-generated-timestamps-md'><!-- GENERATED: see Makefile -->

<h2>Colophon</h2>

<ul>
<li>Documentation Generated: 2020-01-02</li>
<li>Lisp Compiler: SBCL 2.0.0</li>
<li>Quicklisp Distribution: 2019-12-27</li>
</ul>

</article>
  </body>
</html>