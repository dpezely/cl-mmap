CL-MMAP
=======

CL-MMAP is a high-level Common Lisp library for working with memory-mapped
files on POSIX-compliant operating systems, simplifying linear reads and
writes.

This is little more than a wrapper around the related lower-level
[MMAP](../mmap/) package.

CL-MMAP provides an API with options similar to
[OPEN](http://www.lispworks.com/documentation/HyperSpec/Body/f_open.htm) for
working with memory-mapped files on POSIX-compliant operating systems.

Based upon options including `:direction` and `:if-does-not-exist`,
reasonable defaults are applied for low-level `mmap()` flags and whether or
not to persist modifications upon a clean file close.

The library exports the following symbols.

For every **read-** operation, there is an equivalent **write-** function
within the top-level API within CL-MMAP package:

* `with-open-mmap` - macro that ensures that a memory-mapped
  file is safely unmapped (using `munmap-file` function) after we are
  done with it
	+ In the case of having opened the file as read/write or write-only,
	  modifications are automatically persisted upon a clean file close;
	  i.e., "clean" meaning *before* UNWIND-PROTECT's cleanup form
	+ However, your OS may perform an intermittent save at any time
	+ In the case of errors, file is *not closed* and *not explicitly saved*
	  to avoid potential corruption, so you can handle the error and retry
* `open` - with many options same as
  [`OPEN`](http://www.lispworks.com/documentation/HyperSpec/Body/f_open.htm),
   calls POSIX `open()` and `mmap()`
* `close` - remove memory-map and close file
* `read-byte`, `read-char` - read one byte or character from memory-mapped file
* `read-byte-vector` - read vector of single byte elements such as
  ISO-8859-1 or ASCII characters
	+ Further processing would be required for handling UTF-8 properly
	+ See also: UTF8-OCTETS-TO-STRING
	  in [com.google.base](http://quickdocs.org/com.google.base/api)
* `read-big-endian` and `read-little-endian` - operate on multiple bytes as
  a single value
	+ Bytes for unsigned integer #x01020304 stored using Big Endian would
	  appear as octets equivalent to vector #(#x01 #x02 #x03 #x04): from low
	  address to high
	+ Conversely, same vector stored using Little Endian would appear
	  reversed in mmap file: #(#x04 #x03 #x02 #x01)
	+ Remember: Internet Protocol's "network byte order" is Big Endian
*  `read-big-endian-vector` and `read-little-endian-vector` - for
  multi-byte elements within a vector
* `save` and `save-region` - commit pending writes of entire mapped file or
  a subset within mapped file, respectively
	+ Note: the OS may perform an intermittent save, so these commands
      simply give the means to persist on demand
* `seek` and `tell` - sets and gets current position within mapped file,
  respectively

The lower-level API uses similar names but with `get-` and `put-` (instead
of `read-` and `write-`, respectively) within MMAP-OPS package.

## Getting Started

The Common Lisp package is CL-MMAP.

**Reading**

The following code snippet shows how to use CL-MMAP's
`with-open-mmap` and `read-char` to read one character at a time.

```lisp
CL-USER> (require 'cl-mmap)
CL-USER> (cl-mmap:with-open-mmap (addr #P"/data/wikidump")
            (loop while (cl-mmap:read-char addr offset nil)))
```

**Writing**

An example write operation putting characters of alphabet plus Newline into
a text file:

```lisp
CL-USER> (cl-mmap:with-open-mmap (addr
                                  #P"/tmp/foo.txt"
                                  :direction :output
                                  :file-length 27)
           (loop for x upfrom (char-code #\A) to (char-code #\Z)
              do (cl-mmap:write-byte addr x))
           (cl-mmap:write-byte addr (char-code #\Newline)))
10
CL-USER> 
```

```bash
bash:~> cat /tmp/foo.txt
ABCDEFGHIJKLMNOPQRSTUVWXYZ
bash:~> 
```

Notice that an explicit save is omitted because the macro, `with-open-mmap`
does it for you.

You may persist writes manually using `save` function.  Your OS may persist
writes *any time* but only required to do so before unmapping per POSIX
spec.

## Dependencies

This package, CL-MMAP, is a wrapper around [../MMAP/](../MMAP/), which in
turn depends upon:

* [Alexandria](https://common-lisp.net/project/alexandria/)
* [Osicat](https://common-lisp.net/project/osicat/)
* [CFFI](https://common-lisp.net/project/cffi/)

For automated testing:

* [Prove](http://quickdocs.org/prove/)

For generating documentation:

* [cl-gendoc](http://quickdocs.org/cl-gendoc/)

## Running Tests

Tests are triggered using ASDF.  See `cl-mmap.asd` file.

```lisp
CL-USER> (require 'cl-mmap)
CL-USER> (asdf:test-system cl-mmap :force t)
```

The `:force t` flag causes a fresh compilation.

For automated testing via Makefiles or continuous build systems, consider
setting:

```lisp
CL-USER> (setf prove:*default-reporter* :tap)
```

## Automation

Tests may be run using `make clean test` but assumes SBCL for code coverage
reports.

Similarly for documentation: `make clean doc`

## License

MIT License

Essentially, do as you wish and without warranty from authors or maintainers.
