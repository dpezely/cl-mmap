;;;; cl-mmap.asd

(asdf:defsystem #:cl-mmap
  :description
  "Facade for MMAP package, offering functions similar to those in CL"
  :author "Daniel Pezely <first name at last name dot com>"
  :version "1.0"
  :license "MIT"

  :long-description
  "This offers a high-level API on top of MMAP package for convenience
  and familiarity of options similar to equivalent functions within
  the Common Lisp ANSI Spec.

  For instance, `open' offers direction of :input, :output or :io
  along with if-does-not-exist and if-exists each accommodating values
  consistent with OPEN in the CL package.

  The macro, `with-open-mmap', works similarly to WITH-OPEN-FILE in
  the CL package."

  :depends-on (#:mmap)
  :serial t
  :components ((:file "package")
               (:file "cl-mmap"))
  :in-order-to ((asdf:test-op (asdf:test-op "cl-mmap/test"))))

(asdf:defsystem #:cl-mmap/test
  :description "Tests for cl-mmap"
  :depends-on (#:cl-mmap
	       #:prove)
  :serial t
  :components
  ((:module "test"
	    :components ((:file "setup-prove")
                         (:test-file "test-open")
                         (:test-file "test-read")
			 (:test-file "test-write")
                         (:test-file "test-save"))))
  :defsystem-depends-on ("prove-asdf")
  :perform (asdf:test-op (op s)
			 (uiop:symbol-call :prove-asdf :run-test-system s)))
