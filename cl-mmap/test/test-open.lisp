;;;; test-open.lisp


;; SEE ALSO:
;; test-write.lisp for various combination open & read/write tests


(in-package #:cl-mmap/test)

(prove:plan nil)

(prove:diag "File open specifications & behaviours")

;; Be quiet while setting initial conditions-- clean slate:
(remove-all-test-files :quiet)

(prove:is (cl-mmap:open %filepath-does-not-exist% nil nil
                        :direction :input
                        :if-does-not-exist nil)
	  nil
	  "Open input, but file doesn't exist, so return NIL")

(prove:is (cl-mmap:open %filepath-does-not-exist% nil 1024
                        :direction :output
                        :if-does-not-exist nil)
	  nil
	  "Open output, but file doesn't exist, so return NIL")

(prove:is (cl-mmap:open %filepath-does-not-exist% nil 1024
                        :direction :io
                        :if-does-not-exist nil)
	  nil
	  "Open i/o, but file doesn't exist, so return NIL")

(prove:is (probe-file %test-write-filepath%)
	  nil
	  "Test file should NOT exist yet")

(prove:is (with-open-mmap (ptr
                           %test-write-filepath%
                           :direction :output
                           :length-bytes 4096)
	    (when ptr
	      :ok))
	  :ok
	  "Open output, and default to creating file when none exists")

(prove:ok (probe-file %test-write-filepath%)
	  "Test file exists")

(prove:is (test-write-file-size)
	  4096
	  "New file has storage allocated")

(prove:is (cl-mmap:open %test-write-filepath% nil 1024
                        :direction :output
                        :if-exists nil)
	  nil
	  "Open output, but file exists, so return NIL")

(ignore-errors
  (osicat-posix:unlink %test-write-filepath%))

(prove:is (with-open-mmap (ptr
                           %test-write-filepath%
                           :direction :output
                           :length-bytes 1024
                           :if-does-not-exist :create)
	    (when ptr
	      :ok))
	  :ok
	  "Open output, and create file")

(prove:is (with-open-mmap (ptr
                           %test-write-filepath%
                           :direction :output
                           :length-bytes 1024
                           :if-exists :rename-and-delete)
	    (when ptr
	      :ok))
	  :ok
	  "Open output, but file exists, so rename and delete first")


(prove:is (with-open-mmap (ptr
                           %test-write-filepath%
                           :direction :io
                           :length-bytes 1024
                           :if-exists :rename)
	    (when ptr
	      :ok))
	  :ok
	  "Open i/o, but file already exists, so rename it")

(prove:is (test-write-file-size)
	  1024
	  "New file has storaged allocated to size = 1024 bytes")

;; FIXME: could be more robust test, but really we only care that the
;; code path DOESN'T CRASH here.  Supplying additional flags for posix
;; open() is accommodated but documented as 'not rigorously tested".
(prove:is (with-open-mmap (ptr
			   %test-write-filepath%
			   :direction :output
			   :length-bytes 100
			   :posix-open-flags osicat-posix:o-sync)
	    (cl-mmap:write-byte-vector ptr #(02 04 06 08)))
	  #(02 04 06 08)
	  "Supplying :posix-open-flags"
	  :test #'equalp)

(remove-all-test-files :quiet)

(unless (prove:finalize)
  #+swank
  (break "Test suite failures")
  #-swank
  ;; Non-interactive such as running via make or CI, so avoid stack trace
  (uiop/image:shell-boolean-exit nil))
