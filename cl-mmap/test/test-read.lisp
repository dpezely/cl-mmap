;;;; test-read.lisp

(in-package #:cl-mmap/test)

(prove:plan nil)

(prove:diag "Read MMAP file")

;; Be quiet while setting initial conditions-- clean slate:
(remove-all-test-files :quiet)

(create-read-file)

(let ((*print-base* 16))
  (with-open-mmap (ptr %test-read-filepath%)
    (prove:is (cl-mmap:read-byte-vector ptr (length %header%))
	      (map 'vector #'char-code %header%)
	      "File header as string of bytes"
	      :test 'equalp)

    (prove:is (cl-mmap:read-big-endian-vector ptr (length %alphabet%) 1)
	      (map 'vector #'char-code %alphabet%)
	      "Alphabet as byte vector of char-codes"
	      :test 'equalp)

    (prove:is (cl-mmap:read-little-endian-vector ptr (length %hex-digits%) 1)
	      (map 'vector #'char-code %hex-digits%)
	      "Hex digits as vector of char-codes"
	      :test 'equalp)

    (prove:is (cl-mmap:read-char ptr)
	      #\Newline
	      "#\Newline as character")

    (prove:is (cl-mmap:read-char ptr)
	      #\Newline
	      "#\Newline as character")

    (prove:is (cl-mmap:read-big-endian ptr 2)
	      #x0A0A
	      "Two #\Newlines but as non-characters")

    (prove:is (cl-mmap:read-little-endian ptr 16)
	      #x46454443424139383736353433323130
	      "Little Endian, 16 bytes")

    (prove:is (cl-mmap:read-big-endian ptr 16)
	      #x30313233343536373839414243444546
	      "Big Endian, 16 bytes")

    (prove:is (cl-mmap:read-big-endian ptr 8)
	      #x0001020304050607
	      "Hex digits from #x00 through #x07 consecutively as single integer")

    (prove:is (cl-mmap:read-big-endian ptr 8)
	      #x08090A0B0C0D0E0F
	      "Hex digits from #x08 through #x0F consecutively as single integer")

    (prove:is (cl-mmap:read-big-endian ptr 8)
	      #x1011121314151617
	      "Hex digits from #x10 through #x17 consecutively as single integer")

    (prove:is (cl-mmap:read-byte ptr)
	      #x18
	      "Read byte")

    (prove:is (cl-mmap:read-byte ptr)
	      #x19
	      "Read byte")

    (prove:is-error (cl-mmap:read-big-endian-vector ptr 3 -1)
		    'type-error
		    "Negative byte size not allowed")

    (prove:is-error (cl-mmap:read-big-endian-vector ptr 3 0)
		    'type-error
		    "zero byte size not allowed")

    (prove:is (cl-mmap:read-big-endian-vector ptr 3 2)
	      #(#x1A1B #x1C1D #x1E1F)
	      "Three element vector, 2 bytes each Big Endian"
	      :test #'equalp)

    (prove:is (cl-mmap:read-big-endian-vector ptr 2 16)
	      #(#x202122232425262728292A2B2C2D2E2F
		#x303132333435363738393A3B3C3D3E3F)
	      "Two element vector, 16 bytes each Big Endian"
	      :test #'equalp)

    (prove:is (cl-mmap:read-little-endian-vector ptr 2 16)
	      #(#x4f4e4d4c4b4a49484746454443424140
		#x5f5e5d5c5b5a59585756555453525150)
	      "Two element vector, 16 bytes each Little Endian"
	      :test #'equalp)

    (prove:is (cl-mmap:read-little-endian-vector ptr 2 32)
	      #(#x7f7e7d7c7b7a797877767574737271706f6e6d6c6b6a69686766656463626160
		#x9f9e9d9c9b9a999897969594939291908f8e8d8c8b8a89888786858483828180)
	      "Two element vector, 32 bytes each Little Endian"
	      :test #'equalp)

    (prove:is (cl-mmap:read-little-endian-vector ptr 1 80)
	      #(#xEfEeEdEcEbEaE9E8E7E6E5E4E3E2E1E0DfDeDdDcDbDaD9D8D7D6D5D4D3D2D1D0CfCeCdCcCbCaC9C8C7C6C5C4C3C2C1C0BfBeBdBcBbBaB9B8B7B6B5B4B3B2B1B0AfAeAdAcAbAaA9A8A7A6A5A4A3A2A1A0)
	      "One element vector, 80 byte value as Little Endian"
	      :test #'equalp)

    (prove:is (cl-mmap:read-big-endian-vector ptr 16 1)
	      #(#xF0 #xF1 #xF2 #xF3 #xF4 #xF5 #xF6 #xF7 #xF8 #xF9
		#xFa #xFb #xFc #xFd #xFe #xFf)
	      "16 element vector, 1 byte values as unnecessary Big Endian"
	      :test #'equalp)

    (prove:is-error (cl-mmap:read-byte ptr)
		    'mmap:end-of-mmap-error
		    "read-byte at EOF produces error")

    (prove:is (cl-mmap:read-byte ptr nil :found-eof)
	      :found-eof
	      "read-byte at EOF returns symbol")

    (prove:is-error (cl-mmap:read-char ptr)
		    'mmap:end-of-mmap-error
		    "read-char at EOF produces error")

    (prove:is (cl-mmap:read-char ptr nil :found-eof)
	      :found-eof
	      "read-char at EOF returns symbol")

    (prove:is-error (cl-mmap:read-byte-vector ptr 1)
		    'mmap:end-of-mmap-error
		    "read-byte-vector at EOF produces error")

    (prove:is (cl-mmap:read-little-endian ptr 1 nil :found-eof)
	      :found-eof
	      "read-little-endian at EOF returns symbol")

    (prove:is-error (cl-mmap:read-little-endian ptr 1)
		    'mmap:end-of-mmap-error
		    "read-little-endian at EOF produces error")

    (prove:is (cl-mmap:read-big-endian ptr 1 nil :found-eof)
	      :found-eof
	      "read-big-endian at EOF returns symbol")

    (prove:is-error (cl-mmap:read-big-endian ptr 1)
		    'mmap:end-of-mmap-error
		    "read-big-endian at EOF produces error")

    (prove:is (cl-mmap:read-little-endian-vector ptr 1 1 nil :found-eof)
	      :found-eof
	      "read-little-endian-vector at EOF returns symbol")

    (prove:is-error (cl-mmap:read-little-endian-vector ptr 1 1)
		    'mmap:end-of-mmap-error
		    "read-little-endian-vector at EOF produces error")

    (prove:is (cl-mmap:read-big-endian-vector ptr 1 1 nil :found-eof)
	      :found-eof
	      "read-big-endian-vector at EOF returns symbol")

    (prove:is-error (cl-mmap:read-big-endian-vector ptr 1 1)
		    'mmap:end-of-mmap-error
		    "read-big-endian-vector at EOF produces error")))

(remove-all-test-files :quiet)

(unless (prove:finalize)
  #+swank
  (break "Test suite failures")
  #-swank
  ;; Non-interactive such as running via make or CI, so avoid stack trace
  (uiop/image:shell-boolean-exit nil))
