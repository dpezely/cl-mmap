;;;; test-save.lisp

(in-package #:cl-mmap/test)

(prove:plan nil)

(prove:diag "Save MMAP file")

;; Be quiet while setting initial conditions-- clean slate:
(remove-all-test-files :quiet)

(let* ((not-page-size (floor mmap:+posix-page-size+ 3)))
  (with-open-mmap (ptr
		   %test-write-filepath%
		   :direction :io
		   :length-bytes not-page-size
		   :if-exists :supersede)
    (cl-mmap:write-byte-vector ptr #(01 02 03 04))
    (cl-mmap:write-byte-vector ptr #(05 06 07 08))

    (prove:is-error (cl-mmap:save ptr :start not-page-size
                                  :n-bytes not-page-size)
		    'simple-error	;from ASSERT
		    "save requires multiples of page-size")
    (prove:is-error (cl-mmap:save ptr :start (* not-page-size 10)
                                  :n-bytes (* not-page-size 11))
		    'simple-error	;from ASSERT
		    "save requires region be within page-size")))

(prove:subtest "save persists subset of file"
  (let* ((file-size (* 4 mmap:+posix-page-size+))
	 (page-size mmap:+posix-page-size+))
    (with-open-mmap (ptr
		     %test-write-filepath%
		     :direction :io
		     :length-bytes file-size
		     :if-exists :supersede)
      (cl-mmap:write-big-endian ptr #x01020304 page-size)
      (cl-mmap:write-big-endian ptr #x05060708 page-size)
      (cl-mmap:write-big-endian ptr #x090A0B0C page-size)
      (cl-mmap:write-big-endian ptr #x0D0E0F10 page-size)

      (cl-mmap:save ptr :start page-size :n-bytes page-size)

      ;; OS may sync the entire map or just a region, but confirm only
      ;; the region here:
      (with-open-file (stream %test-write-filepath%
			      :element-type 'unsigned-byte)
	(let ((buffer (make-array page-size
				  :element-type '(unsigned-byte 8))))
	  ;; Because the OS may have performed an intermittent save,
	  ;; only test region specified to be saved:
	  (read-sequence buffer stream)	;intentionally not tested
	  (read-sequence buffer stream)
	  (prove:is (count 0 buffer)
		    (- page-size 4)
		    "Second quarter of file is mostly null")
	  (prove:is (subseq buffer (- page-size 4))
		    #(#x05 #x06 #x07 #x08)
		    "Confirmed values, so second quarter of file has been persisted"
		    :test #'equalp)))

      (prove:is (cl-mmap:close ptr)
		t
		"Calling cl-mmap:close directly succeeds")
      (prove:is (cl-mmap::mmapped-file-status ptr)
		:closed
		"File status indicates :closed")
      (prove:is (cl-mmap::mmapped-file-fd ptr)
		-1
		"File descriptor has been invalided"))))

(remove-all-test-files :quiet)

(unless (prove:finalize)
  #+swank
  (break "Test suite failures")
  #-swank
  ;; Non-interactive such as running via make or CI, so avoid stack trace
  (uiop/image:shell-boolean-exit nil))
