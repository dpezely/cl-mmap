;;;; test-write.lisp

(in-package #:cl-mmap/test)

(prove:plan nil)

(prove:diag "Write MMAP file")

;; Be quiet while setting initial conditions-- clean slate:
(remove-all-test-files :quiet)

(let ((length 4))
  (with-open-mmap (ptr %test-write-filepath%
		       :length-bytes length
		       :direction :io)
    (cl-mmap:write-byte ptr #x0))

  (with-open-file (stream %test-write-filepath%
			  :element-type '(unsigned-byte 8))
    (prove:is (read-byte stream nil)
	      #x00
	      "Open :io, write one byte: zero"
	      :test #'equalp)))

(let ((length 2))
  (with-open-mmap (ptr %test-write-filepath%
		       :length-bytes length
		       :direction :io)
    (prove:is (cl-mmap:read-byte ptr nil)
	      #x00
	      "Re-open :io, re-read byte: zero")

    (prove:is (cl-mmap:tell ptr)
	      1
	      "Get current offset via cl-mmap:TELL")

    (prove:is (cl-mmap:seek ptr 0)
	      0
	      "Set current offset via cl-mmap:SEEK")
    (cl-mmap:write-byte ptr #xFF)
    (cl-mmap:write-char ptr #\Newline))

  (with-open-file (stream %test-write-filepath%
			  :element-type '(unsigned-byte 8))
    (prove:is (read-byte stream nil)
	      #xFF
	      "Open :io, write one byte: #xFF")
    (prove:is (code-char (read-byte stream nil))
	      #\Newline
	      "Open :io, write one byte: #\Newline")))

(prove:ok (probe-file %test-write-filepath%)
	  "Test file already exists")
(prove:is (probe-file %test-renamed-filepath%)
	  nil
	  "Renamed file does NOT exist yet")

(let ((length 4))
  (with-open-mmap (ptr %test-write-filepath%
		       :length-bytes length
		       :direction :output)
    (cl-mmap:write-byte ptr #x0))

  (with-open-file (stream %test-write-filepath%
			  :element-type '(unsigned-byte 8))
    (prove:is (read-byte stream nil)
	      #x00
	      "Open :output if-exists unspecified, write one byte: zero"
	      :test #'equalp)))

(prove:ok (probe-file %test-renamed-filepath%)
	  "Renamed file exists here")

(let ((length 1))
  (with-open-mmap (ptr %test-write-filepath%
		       :length-bytes length
		       :direction :output)
    (cl-mmap:write-byte ptr #xFF))

  (with-open-file (stream %test-write-filepath%
			  :element-type '(unsigned-byte 8))
    (prove:is (read-byte stream nil)
	      #xFF
	      "Open :ouput, Write one byte: #xFF")))

(prove:is-error (with-open-mmap (ptr %test-write-filepath%
				     :length-bytes 3
				     :direction :output)
		  (cl-mmap:write-byte ptr #x100))
		type-error
		"Attempt writing more than one by as one byte: TYPE-ERROR")

(let ((length (length %alphabet%)))
  (with-open-mmap (ptr %test-write-filepath%
		       :length-bytes length
		       :direction :io)
    (loop for char upfrom 65 to 90
       do (cl-mmap:write-byte ptr char))
    (cl-mmap:write-byte ptr (char-code #\Newline)))

  (with-open-file (stream %test-write-filepath%)
    (prove:is (let ((buffer (make-string length)))
		(read-sequence buffer stream)
		buffer)
	      %alphabet%
	      "Persisted alphabet via mmap"
	      :test #'equal)))

(let ((*print-base* 16)
      (data #(#x000102030405060708090a0b0c0d0e0f
	      #x101112131415161718191a1b1c1d1e1f
	      #x202122232425262728292a2b2c2d2e2f
	      #x303132333435363738393a3b3c3d3e3f)))
  (with-open-mmap (ptr %test-write-filepath%
		       :length-bytes 128
		       :direction :output
		       :if-exists :new-version)
    (cl-mmap:write-big-endian ptr (elt data 0) 16)
    (cl-mmap:write-big-endian ptr (elt data 1) 16)
    (cl-mmap:write-big-endian ptr (elt data 2) 16)
    (cl-mmap:write-big-endian ptr (elt data 3) 16)
    (cl-mmap:write-little-endian ptr (elt data 0) 16)
    (cl-mmap:write-little-endian ptr (elt data 1) 16)
    (cl-mmap:write-little-endian ptr (elt data 2) 16)
    (cl-mmap:write-little-endian ptr (elt data 3) 16))
  (with-open-file (stream %test-write-filepath%
			  :element-type 'unsigned-byte)
    (let ((buffer (make-array 16 :element-type '(unsigned-byte 8))))
      (read-sequence buffer stream)
      (prove:is buffer (elt data 0) "Big Endian, first 16 bytes"
		:test #'compare-bytes-to-value)
      (read-sequence buffer stream)
      (prove:is buffer (elt data 1) "Big Endian, second 16 bytes"
		:test #'compare-bytes-to-value)
      (read-sequence buffer stream)
      (prove:is buffer (elt data 2) "Big Endian, third 16 bytes"
		:test #'compare-bytes-to-value)
      (read-sequence buffer stream)
      (prove:is buffer (elt data 3) "Big Endian, fourth 16 bytes"
		:test #'compare-bytes-to-value)

      (read-sequence buffer stream)
      (prove:is (reverse buffer) (elt data 0) "Little Endian, first 16 bytes"
		:test #'compare-bytes-to-value)
      (read-sequence buffer stream)
      (prove:is (reverse buffer) (elt data 1) "Little Endian, second 16 bytes"
		:test #'compare-bytes-to-value)
      (read-sequence buffer stream)
      (prove:is (reverse buffer) (elt data 2) "Little Endian, third 16 bytes"
		:test #'compare-bytes-to-value)
      (read-sequence buffer stream)
      (prove:is (reverse buffer) (elt data 3) "Little Endian, fourth 16 bytes"
		:test #'compare-bytes-to-value))))

(let ((*print-base* 16)
      (data #(#x000102030405060708090a0b0c0d0e0f
	      #x101112131415161718191a1b1c1d1e1f
	      #x202122232425262728292a2b2c2d2e2f
	      #x303132333435363738393a3b3c3d3e3f)))
  (with-open-mmap (ptr %test-write-filepath%
		       :length-bytes 128
		       :direction :output
		       :if-exists :supersede)
    (cl-mmap:write-big-endian-vector ptr data 16)
    (cl-mmap:write-little-endian-vector ptr data 16))
  (with-open-file (stream %test-write-filepath%
			  :element-type 'unsigned-byte)
    (let ((buffer (make-array 16 :element-type '(unsigned-byte 8))))
      (read-sequence buffer stream)
      (prove:is buffer (elt data 0) "Big Endian, first 16 bytes"
		:test #'compare-bytes-to-value)
      (read-sequence buffer stream)
      (prove:is buffer (elt data 1) "Big Endian, second 16 bytes"
		:test #'compare-bytes-to-value)
      (read-sequence buffer stream)
      (prove:is buffer (elt data 2) "Big Endian, third 16 bytes"
		:test #'compare-bytes-to-value)
      (read-sequence buffer stream)
      (prove:is buffer (elt data 3) "Big Endian, fourth 16 bytes"
		:test #'compare-bytes-to-value)

      (read-sequence buffer stream)
      (prove:is (reverse buffer) (elt data 0) "Little Endian, first 16 bytes"
		:test #'compare-bytes-to-value)
      (read-sequence buffer stream)
      (prove:is (reverse buffer) (elt data 1) "Little Endian, second 16 bytes"
		:test #'compare-bytes-to-value)
      (read-sequence buffer stream)
      (prove:is (reverse buffer) (elt data 2) "Little Endian, third 16 bytes"
		:test #'compare-bytes-to-value)
      (read-sequence buffer stream)
      (prove:is (reverse buffer) (elt data 3) "Little Endian, fourth 16 bytes"
		:test #'compare-bytes-to-value))))

(with-open-file (out %test-write-filepath%
		     :direction :output
		     :if-exists :supersede
		     :element-type '(unsigned-byte 8))
  (write-sequence (map 'vector #'char-code %header%) out))

(prove:subtest "Supersede"
  (let ((file-length 3)) ; some size other than %test-write-filepath%
    (prove:isnt (test-write-file-size)
		file-length
		"File exists (but not of our next target size)")
    (prove:is (with-open-mmap (ptr
			       %test-write-filepath%
			       :direction :io
			       :length-bytes file-length
			       :if-exists :supersede)
		(when ptr
		  :ok))
	      :ok
	      "Open both-directions but file already exists, so supersede")
    (prove:is (test-write-file-size)
	      file-length
	      "File has been truncated")))

(with-open-mmap (ptr
		 %test-write-filepath%
		 :direction :output
		 :length-bytes mmap:+posix-page-size+
		 :if-exists :overwrite)
  (cl-mmap:write-little-endian ptr #x01020304 mmap:+posix-page-size+)
  (prove:is (cl-mmap::mmapped-file-offset ptr)
	    mmap:+posix-page-size+
	    "Offset is at file length")
  (prove:is (cl-mmap:write-byte ptr #x01 nil)
	    nil
	    "Writing 1 byte beyond page size triggers EOF"))

(with-open-mmap (ptr
		 %test-write-filepath%
		 :direction :output
		 :length-bytes 9
		 :if-exists :supersede)
  (cl-mmap:close ptr)
  (prove:is-error (cl-mmap:write-byte ptr #xFF)
		  'mmap:end-of-mmap-error
		  "Attempted write after file close signals posix-error")
  (prove:is-error (cl-mmap:write-byte-vector ptr #(#xFF #xEE))
		  'mmap:end-of-mmap-error
		  "Attempted write after file close signals posix-error"))

(prove:subtest "Test of open with OFFEST"
  ;; Write one byte at end of middle third of file:
  (with-open-mmap (ptr
                   %test-write-filepath%
                   :direction :output
                   :length-bytes (* 3 mmap:+posix-page-size+)
                   :if-exists :supersede)
    (cl-mmap:seek ptr (1- (* 2 mmap:+posix-page-size+)))
    (cl-mmap:write-byte ptr #xFF))
  ;; Read from only the middle third of same file:
  (with-open-mmap (ptr
                   %test-write-filepath%
                   :direction :input
                   :offset mmap:+posix-page-size+
                   :length-bytes mmap:+posix-page-size+)
    (cl-mmap:seek ptr (1- mmap:+posix-page-size+))
    (prove:is (cl-mmap:read-byte ptr)
              #xFF
              "Access byte from within smaller window")
    (prove:is-error (cl-mmap:read-byte ptr)
                    'mmap:end-of-mmap-error
                    "Confirm end of smaller region")))

#-allow-low-level-mmap-errors
(prove:is-error (cl-mmap:open %test-write-filepath%
                               (1- mmap:+posix-page-size+)
                               mmap:+posix-page-size+
                               :direction :input)
                'simple-error           ;signaled by ASSERT
                "Attempted open :input with OFFEST other than page size hits ASSERT")

#-allow-low-level-mmap-errors
(prove:is-error (cl-mmap:open %test-write-filepath%
                              (1+ mmap:+posix-page-size+)
                              mmap:+posix-page-size+
                              :direction :input)
                'simple-error           ;signaled by ASSERT
                "Attempted open :input with OFFEST other than page size hits ASSERT")

#-allow-low-level-mmap-errors
(prove:is-error (cl-mmap:open %test-write-filepath%
                              (1+ mmap:+posix-page-size+)
                              mmap:+posix-page-size+
                              :direction :output)
                'simple-error           ;signaled by ASSERT
                "Attempted open :output with OFFEST other than page size hits ASSERT")

;; This next test passes intermittently due to OS behaviour.  For our
;; low-level functions to trigger an error condition seems to depend
;; upon whether the page has been place into memory with an empty page
;; following it or not.  If not, only then do we see error as expected!
#+(or)
(with-open-mmap (ptr
		 %test-write-filepath%
		 :direction :io
		 :length-bytes mmap:+posix-page-size+
		 :if-exists :supersede)
  ;; Fill to maximum file-length:
  (cl-mmap:write-little-endian ptr #x01020304 mmap:+posix-page-size+)
  ;; Forcing a write via low-level function:
  (mmap::put-big-endian (cl-mmap::mmapped-file-pointer ptr)
                            (cl-mmap::mmapped-file-offset ptr)
                            #x61626364 4)
  ;; Try reading it.  No need for SEEK, as OFFSET was never updated!
  (prove:is (mmap::get-big-endian (cl-mmap::mmapped-file-pointer ptr)
                                      (cl-mmap::mmapped-file-offset ptr) 4)
	    nil	;maybe, maybe not
	    "Low-level writing beyond page size loses data"))

(remove-all-test-files :quiet)

(unless (prove:finalize)
  #+swank
  (break "Test suite failures")
  #-swank
  ;; Non-interactive such as running via make or CI, so avoid stack trace
  (uiop/image:shell-boolean-exit nil))
