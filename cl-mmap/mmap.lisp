;;;; mmap.lisp - Common Lisp semantics for POSIX open(2), mmap(2) and msync(2)

(in-package #:mmap)

;; See also `open.lisp' file for functions called by `mmap'
;; and a table illustrating semantics of `OPEN' behaviour.

(defun mmap (pathname &rest args &key file-length mapping-type (offset 0)
                                   (direction :input) (mode *default-file-mode*)
                                   (if-exists nil e?) (if-does-not-exist nil ne?)
                                   (posix-open-flags *default-posix-open-flags*)
                                   (posix-mmap-flags *default-posix-mmap-flags*)
                                   close-without-save)
  "Opens FILENAME and maps into memory.  For new files, must specify FILE-LENGTH > 0."
  (declare (ignore close-without-save)	;Accommodate `with-mmap-file' macro
	   (type (or pathname string) pathname)
	   (type (or null symbol) mapping-type direction if-exists if-does-not-exist)
	   (type (or null (integer 0)) file-length offset mode
		 posix-open-flags posix-mmap-flags))
  #+open-mmap-without-safety (declare (ignore args))
  #-open-mmap-without-safety (apply 'validate-open-args (cons pathname args))
  (multiple-value-bind (open-flag memory-protection)
      (process-open-flags pathname direction if-exists e? if-does-not-exist ne?)
    (when open-flag
      (when posix-open-flags
	(setf open-flag (logior open-flag posix-open-flags)))
      #+verbose
      (format t "open filename=~A open-flag=#x~4,'0X/~[R~;W~;RW~] mode=#o~4,'0O~%"
	      pathname open-flag (logand open-flag #x03) mode)
      (let* ((mapping-type-flag (set-mapping-type-flag direction mapping-type))
	     (filename (if (pathnamep pathname) (namestring pathname) pathname))
	    (fd (trap (osicat-posix:open filename open-flag mode)))
	    (close-fd-on-return t))
	(when posix-mmap-flags
	  (setf mapping-type-flag (logior mapping-type-flag posix-mmap-flags)))
	(when (and file-length (find direction '(:output :io)) (> file-length 0))
	  (allocate-writable-region fd file-length))
	#+verbose
	(let ((stats (osicat-posix:fstat fd)))
	  (format t "opened fstat fd=~D mode=~5,'0O size=~A~%"
		  fd (osicat-posix:stat-mode stats) (osicat-posix:stat-size stats))
	  (format t "mmap-file length=~A protect=#x~X/~[none~;read~;write~;rw~] ~
		     mapping=~A/~[none~;shared~;private~] offset=#x~X~%"
		  file-length memory-protection (logand memory-protection #x03)
		  mapping-type mapping-type-flag offset)
	(finish-output))
	(unwind-protect
	     (let* ((fl (or file-length
			    (trap (osicat-posix:stat-size (osicat-posix:fstat fd)))))
		    (mmap-addr (trap (osicat-posix:mmap (cffi:null-pointer)
								   fl
								   memory-protection
								   mapping-type-flag
								   fd
								   offset)
						osicat-posix:map-failed)))
	       (cond
		 ((not (and (eq direction :input)
			    (or (null file-length) (= fl file-length))))
		  (setf close-fd-on-return nil)
		  #+verbose (format t "file-descriptor ~:[remains open~;CLOSED~] fd=~D~%"
				    close-fd-on-return fd)
		  (values mmap-addr fl fd))
		 (t
		  (values mmap-addr fl))))
	  (when close-fd-on-return
	    #+verbose (format t "~&closing fd=~D~%" fd)
	    (trap (osicat-posix:close fd))))))))

(defun munmap (mmap-addr file-size fd)
  "Removes a mapping at the address (with the range of FILE-SIZE) that
the MMAP-ADDR pointer points to from memory."
  (trap (osicat-posix:munmap mmap-addr file-size)
	osicat-posix:map-failed)
  #+verbose (format t "~&munmap-file pointer=~X file-size=~X fd=~A~%"
		    mmap-addr file-size fd)
  (when (and fd (> fd -1))
    #+verbose (format t "close fd=~D~%" fd)
    (trap (osicat-posix:close fd))))

(defun sync (mmap-addr offset n-bytes &key notify)
  "Finish writes so they become persisted.
  Params MMAP-ADDR is base address, OFFSET is number of
  bytes from base address to begin region to be saved, and N-BYTES is
  number of bytes from offset to be saved.
  Region specified by OFFSET spanning N-BYTES must be a
  multiple/modulo of page size.  See `+posix-page-size+'.
  Keyword param NOTIFY instructs OS kernel to invalidate other
  mappings of this mmapped file to ensure geting fresh values just
  written.
  This function should only be called when mmap was opened with
  direction :output or :io."
  (declare (type (integer 0) offset n-bytes))
  (trap (osicat-posix:msync (cffi:inc-pointer mmap-addr offset)
			    n-bytes
			    (if notify
				(logior osicat-posix:ms-invalidate
					osicat-posix:ms-sync)
				osicat-posix:ms-sync))))

(defun sync-async (mmap-addr offset n-bytes &key notify)
  "Finish writes so they become persisted asynchronously.
  Calling scope may continue without waiting operation to complete.
  Params MMAP-ADDR is base address, OFFSET is number of
  bytes from base address to begin region to be saved, and N-BYTES is
  number of bytes from offset to be saved.
  Region specified by OFFSET spanning N-BYTES must be a
  multiple/modulo of page size.  See `+posix-page-size+'.
  Keyword param NOTIFY instructs OS kernel to invalidate other
  mappings of this mmapped file to ensure geting fresh values just
  written.
  This function should only be called when mmap was opened with
  direction :output or :io.
  Returns: NIL on success or values of POSIX ERRNO integer with string
  message upon failure.  See Unix manual page `msync(2)` for details."
  (trap (osicat-posix:msync (cffi:inc-pointer mmap-addr offset)
			    n-bytes
			    (if notify
				(logior osicat-posix:ms-invalidate
					osicat-posix:ms-async)
				osicat-posix:ms-async))))
